(function(Notes) {

    var LoadMobile = function(options) {

    
	var mobileInit = function(off) {

		if (window.matchMedia("(max-width: 1024px)").matches) {
			var ideas = document.getElementsByClassName('active');
			if (ideas.length === 0) {
				return false;
			}
			var desc = document.getElementById('description-editor-container');
			if (ideas.length === 0) {
				var descIdeas = document.getElementsByClassName('wysiwyg-active');
				if (descIdeas.length === 0) {
					return false;
				}
			}
			var stage = document.getElementById('ideas-editor-container');
			var ideaEditor = stage.getElementsByTagName('textarea')[0];				
			
			// create a manager for that element
			var mc = new Hammer(stage, {domEvents: true});
			var d = new Hammer(desc, {domEvents: true});
			var turn = false;
			mc.on('swipeleft', function() {
				if(turn) {
					//stage.style.transform = 'rotate(0)';
					turn = false;
				} else {
					//stage.style.transform = 'rotate(-1024deg)';
					stage.style.transform = 'translateX(-100vw)';
					desc.style.transform = 'translateX(0vw)';
					turn = true;
				}
			});

			d.on('swiperight', function() {
				if(turn) {
					//stage.style.transform = 'rotate(0)';
					stage.style.transform = 'translateX(0vw)';
					desc.style.transform = 'translateX(100vw)';
					turn = false;
				} else {
					//stage.style.transform = 'rotate(-1024deg)';
					turn = true;
				}
			});
			return {
				mc: mc,
				d: d
			}
		}
	};
	if (window.matchMedia("(max-width: 1024px)").matches) {
		var desc = document.getElementById('description-editor-container');
		if (desc) {
			desc.style.transform = 'translateX(100vw)';
		} 
		mobileInit();
		Notes.events.addListener('idea-focus', function(id, isActive) {
			this.mobile;
				if (isActive) {
				this.mobile = mobileInit();
			} else {
			}
		}.bind(this));
	}

	Notes.events.addListener('note-loaded', function(isNew) {

		if (window.matchMedia("(max-width: 1024px)").matches) {

			$('#editor-section').on('click', function() {
				$('.description-editor').parent().hide();
				$('.comments__container').hide();
				$('.footnote').hide();
				$('.create-footnote-btn').hide();
				$('.description-empty').hide();
			}).children().click(function(e) {
				return false;
			});

			$('.note-menu').prependTo($('#note'));
		}
	});

    };

    if (Notes.init === undefined) {
        Notes.init = {};
    }

    Notes.init.LoadMobile = LoadMobile;

})(Notes);