(function(Notes, localStorage) {

	var Tour = function() {
		if (localStorage.getItem('notes-visited')) {
			return false;
		} else {
			localStorage.setItem('notes-visited', true);
			new Notes.elements.Tour({
				container: document.getElementById('tour')
			});
		}
	};

	Notes.Tour = Tour;

})(Notes, localStorage);