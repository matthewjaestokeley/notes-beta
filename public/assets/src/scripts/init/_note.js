(function(Notes, localStorage) {

    var LoadNote = function(options) {

      var e = Notes.events;
      var api = Notes.api;

      api.get({
          name: 'on-get-note',
          method: 'POST',
          endpoint: 'note/' + $('#note').data('noteId'),
          data: {
            token: localStorage.getItem('notes-token')
          }
      });

      e.addListener('on-get-note', function(data) {
        $('#note').append(data.html);
        $('.ideas-editor').each(function(editor) {
          new Notes.Editor({
              editor: document.getElementById(this.id)
          });
        });

        $('.description-editor').each(function(editor) {
          var ed = document.getElementById(this.id);
            var desc = new Notes.elements.Description({
                description: ed
            });
            desc.insertHtml(ed.innerHTML);
        });

        Notes.events.emit('note-loaded');
        Notes.events.emit('message', Notes, 'Note Loaded', 'info');
      });

    };

    if (Notes.init === undefined) {
        Notes.init = {};
    }

    Notes.init.LoadNote = LoadNote;

})(Notes, localStorage);
