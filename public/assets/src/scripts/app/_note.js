(function(Notes) {

	document.addEventListener("DOMContentLoaded", function(event) {

		var mobile = new Notes.init.LoadMobile();

		/**
		 * [loginBtn description]
		 * @type {[type]}
		 */
		var loginBtn = document.getElementById('login-btn');

		if (loginBtn) {

			var user = Notes.user = new Notes.User();

			/**
			 * [siteNav description]
			 * @type {Notes}
			 */
			var userMenu = new Notes.elements.UserMenu({
				loginBtn: document.getElementById('login-btn'),
				logoutBtn: document.getElementById('logout-btn'),
				userBtn: document.getElementById('user-btn'),
				user: user
			});

			/**
			 * [userMenu description]
			 * @type {Notes}
			 */
			var userNav = new Notes.elements.UserNav({
				button: document.getElementById('user-btn'),
				menu: document.getElementById('user-menu')
			});

			/**
			 * [form description]
			 * @type {Notes}
			 */
			var form = new Notes.elements.LoginForm({
				form: document.getElementById('login-form')
			});

			/**
			 * [signupform description]
			 * @type {Notes}
			 */
			var signupform = new Notes.elements.LoginForm({
				form: document.getElementById('signup-form')
			});

			/**
			 * [loginModal description]
			 * @type {Notes}
			 */
			var loginModal = new Notes.elements.LoginModal({
				modal: document.getElementById('login-modal')
			});

			Notes.loginModal = loginModal;
	
		}

		/**
		 * [loginPanel description]
		 * @type {[type]}
		 */
		var loginPanel = document.getElementById('login-panel');

		/**
		 * [if description]
		 * @param  {[type]} loginPanel [description]
		 * @return {[type]}            [description]
		 */
		if(loginPanel) {
			var user = Notes.user = new Notes.User();
			/**
			 * [form description]
			 * @type {Notes}
			 */
			var form = new Notes.elements.LoginForm({
				form: document.getElementById('login-form')
			});

			/**
			 * [signupform description]
			 * @type {Notes}
			 */
			var signupform = new Notes.elements.LoginForm({
				form: document.getElementById('signup-form')
			});

			/**
			 * [loginModal description]
			 * @type {Notes}
			 */
			var loginModal = new Notes.elements.LoginModal({
				modal: document.getElementById('login-panel')
			});
		}
		
		/**
		 * [description]
		 * @param  {Notes}  isNew)       {			var                                         note          [description]
		 * @param  {[type]} reminderBtn: document.getElementById('set-reminder-btn')      [description]
		 * @param  {[type]} detailsBtn:  document.getElementById('edit-details-btn')      [description]
		 * @param  {Notes}  saveBtn:     document.getElementById('save-btn')			});			var detailsPane   [description]
		 * @return {[type]}              [description]
		 */
		Notes.events.addListener('note-loaded', function(isNew) {

			/**
			 * [note description]
			 * @type {Notes}
			 */
			var note = new Notes.elements.Note({
				note: document.getElementById('note')
			});

			/**
			 * [menu description]
			 * @type {Notes}
			 */
			var menu = new Notes.elements.NoteMenu({
				newBtn: document.getElementById('new-note-btn'),
				reminderBtn: document.getElementById('set-reminder-btn'),
				detailsBtn: document.getElementById('edit-details-btn'),
				saveBtn: document.getElementById('save-btn')
			});

			/**
			 * [detailsPane description]
			 * @type {Notes}
			 */
			
			var detailsPane = new Notes.elements.DetailsPane({
				container: document.getElementById('note-details')
			});

			if (isNew) {
				 /**
				 * [description description]
				 * @type {Notes}
				 */
				var description = new Notes.elements.Description({
					description: document.getElementById('description-editor-1')
				});		
			}

			/**
			 * [editors description]
			 * @type {Notes}
			 */
			
			var editor = new Notes.Editor({
				editor: document.getElementById('ideas-editor-1')
			});

			var editors = new Notes.Editors({
				container: document.getElementById('ideas')
			});

			/**
			 * [loadComments description]
			 * @type {Notes}
			 */
			var loadComments = new Notes.elements.LoadComments();

		});

		/**
		 * Note List View
		 * @type {[type]}
		 */
		var noteListEl = document.getElementById('notes-list');

		/**
		 * [if description]
		 * @param  {[type]} noteListEl [description]
		 * @return {[type]}            [description]
		 */
		if (noteListEl) {


			var loadNotes = new Notes.elements.LoadNotes({
				notesList: noteListEl
			});

			/**
			 * [notes description]
			 * @type {Notes}
			 */
			var notes = new Notes.elements.Notes({
				notesList: noteListEl
			});



			/**
			 * [search description]
			 * @type {Notes}
			 */
			var search = new Notes.elements.SearchBar();

		}


		/**
		 * Note View
		 * @type {[type]}
		 */
		var noteEl = document.getElementById('note');

		if (noteEl) {

			if (noteEl.dataset.noteStatus === 'retreived') {
				/**
				 * [note description]
				 * @type {Notes}
				 */
				var note = new Notes.init.LoadNote({
					note: noteEl
				});
			}

			/**
			 * [if description]
			 * @param  {[type]} noteEl.dataset.noteStatus [description]
			 * @return {[type]}                           [description]
			 */
			if (noteEl.dataset.noteStatus === 'new') {
				Notes.events.emit('note-loaded', null, true);	
			}

		}

	});


})(Notes);