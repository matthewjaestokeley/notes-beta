(function(Notes) {

	var Tour = function(options) {

		this.container = options.container;
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if (w > 767) {

        	$(this.container).show();
        	$('.tour-container').show();
        }

        $('#noTour').on('click', function() {
        	$.Velocity($('.tour-container'), "slideUp", {
				easing: "easeIn",
				duration: 100
			});
			$(this.container).fadeOut();
        }.bind(this));
		
	};


	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.Tour = Tour;

})(Notes);
