(function(Notes) {

	var SearchBar = function() {
		var search = document.getElementById('notes-search');
		search.addEventListener('focus', function(event) {
			$.Velocity(event.target, {
				width: "150px"
			}, {
				duration: 150,
				easing: [0.4, 0.2, 0.3, .1]
			});
		});
	}
	
	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.SearchBar = SearchBar;

})(Notes);

