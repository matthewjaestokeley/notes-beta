(function(Notes) {

	var _ = Notes.events;
	//var $ = Notes.elemental;

	var NotesList = function() {
		this.init();
		this.listen();
	};

	NotesList.prototype.init = function() {
		this.w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		this.h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	};

	NotesList.prototype.onNoteDeletedLoadListener = function() {
		
		window.addEventListener('resize', this.init.bind(this));
	    
	    /**
	     * [deleteButton description]
	     * @type {[type]}
	     */
	    this.deleteButton = document.getElementById('delete');
	   	
	   	/**
	   	 * [deleteNoteButtons description]
	   	 * @type {[type]}
	   	 */
		this.deleteNoteButtons = document.getElementsByClassName('delete-note')
		
		/**
		 * [modalBackground description]
		 * @type {[type]}
		 */
		this.modalBackground = document.getElementById('modal-background');
		
		/**
		 * [cancelDeleteButton description]
		 * @type {[type]}
		 */
		this.cancelDeleteButton = document.getElementById('cancel-delete');

	    /**
	     * [formButton description]
	     * @type {Notes}
	     */
	    this.formButton = new Notes.elements.LoginFormButton({
            button: this.deleteButton
        });
		
		/**
		 * [description]
		 * @param  {[type]} button) 
		 * @return {[type]}
		 */
		Array.prototype.forEach.call(this.deleteNoteButtons, function(button) {
			button.addEventListener('click', this.openDeleteModal.bind(this));
		}.bind(this));

		// when the modal background is clicked
		this.modalBackground.addEventListener('click', this.closeModal.bind(this));
		
		// cancel note deletion
		this.cancelDeleteButton.addEventListener('click', this.closeModal.bind(this));
        
        // delete note
        this.deleteButton.addEventListener('click', this.onDeleteNote.bind(this));
	};

	NotesList.prototype.listen = function() {
    	_.addListener('get-notes', this.onNoteDeletedLoadListener.bind(this));
		_.addListener('note-delete', this.onNoteDeletedLoadListener.bind(this));
        _.addListener('note-delete', this.onNoteDeleted.bind(this));
	};

	NotesList.prototype.onDeleteNote = function() {

        this.formButton.changeButtonState('waiting');

        Notes.api.write({
        	name: "note-delete",
        	method: "POST",
        	endpoint: "note/delete",
        	context: this,
        	data: {
                token: localStorage.getItem('notes-token'),
                noteId: this.deleteButton.dataset.noteId,
            }
        });
	};

	NotesList.prototype.onNoteDeleted = function(data, context) {
		 if (data.error !== undefined) {
            Notes.events.emit("message", null, data.error);
            this.formButton.changeButtonState('ready');
            return false;
        }
        this.formButton.changeButtonState('success');
        Notes.events.emit('message', null, 'Note Deleted');
        $('.modal-background').fadeOut(100);
        $('.delete-modal').fadeOut(50);
        var deleteBtn = document.getElementById('delete');
        var id = this.deleteButton.dataset.noteId;
        $('.notes-list__item[data-note-id="'+id+'"]').fadeOut(100);
	};

	NotesList.prototype.openDeleteModal = function(event, value) {
		this.loadNoteDataIntoModal(event, value).openModal();
	};

	NotesList.prototype.loadNoteDataIntoModal = function(event, value) {
        var note = $(event.target);
        var title = note.data('noteTitle');
        var id = note.data('noteId');
        $('#modal-note-name').html(title);
        var deleteBtn = document.getElementById('delete');
        deleteBtn.dataset.noteId = id;
        return this;
	};

	NotesList.prototype.openModal = function() {
        $('.modal-background').fadeIn(200);
        $('.delete-modal').velocity({
            translateY: this.h
        }, {
            duration: 0
        }).show().velocity({
            translateY: 0
        }, {
            duration: 200,
            easing: "easeIn"
        });
		return this;
	};

	NotesList.prototype.closeModal = function() {
        $('.modal-background').fadeOut(100);
        $('.delete-modal').fadeOut(50);
	};

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.Notes = NotesList;
})(Notes);
