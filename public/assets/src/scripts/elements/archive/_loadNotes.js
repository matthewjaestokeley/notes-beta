(function(Notes, localStorage, setTimeout) {

    // var $ = Notes.elemental;
    var api = Notes.api;
	  var _ = Notes.events;

    var LoadNotes = function(options) {
      	this.listen();
      	this.init();
    };

    LoadNotes.prototype.init = function() {
    	api.write({
	      	name: 'get-notes',
	      	method: 'POST',
	      	endpoint: 'notes',
            context: this,
	      	data: {
	      		token: localStorage.getItem('notes-token')
      		}
	    });
    };

    LoadNotes.prototype.listen = function() {
    	_.addListener('get-notes', this.callback.bind(this));
    }

    /**
     * @todo refactor
     * @param  {[type]}   data [description]
     * @return {Function}      [description]
     */
    LoadNotes.prototype.callback = function(data) {
      if (!Array.isArray(data)) {
          _.emit('message', null, 'Error');
          return false;
      }

    	data.forEach(function(value, index) {
            if(value.note && value.note.fields) {
                $('#notes-list').append('<li class="notes-list__item" data-note-id="'+value.noteId+'" data-note-title="'+value.note.fields.title.value+'"><div class="notes-list__title"><a class="notes-list__link" href="/note/'+value.noteId+'">'+value.note.fields.title.value+'</a><ul><li><span>'+value.note.ideas[0].value+' &hellip;</span></li></ul></div><div class="notes-list__menu"><a id="delete-note-'+value.noteId+'" class="delete-note" data-note-id="'+value.noteId+'" data-note-title="'+value.note.fields.title.value+'" href="javascript:;"><span class="ion-android-cancel"></span>Delete</a></div></li>');
            }
        });
        
    }


    if (Notes.init === undefined) {
        Notes.init = {};
    }

    Notes.elements.LoadNotes = LoadNotes;

})(Notes, localStorage, window.setTimeout);
