(function(Notes) {

	var _ = Notes.events;
	var api = Notes.api;

	var Title = function(options) {
		this.title = options.title || document.getElementById('title-input');
		this.listen.call(this);
	};

	Title.prototype.listen = function() {

		this.title.addEventListener('click', function() {
			_.emit('title-edit');
		});

		_.addListener('title-edit', function() {
		
		}.bind(this)); 

	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.Title = Title;

})(Notes);
