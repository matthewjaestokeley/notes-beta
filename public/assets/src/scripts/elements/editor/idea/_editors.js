(function(Notes, setTimeout) {

	//var $ = Notes.elemental;

	var Editors = function(options) {
		this.container = options.container;
		this.editors = $(this.container).find($('.editor'));
		this.descriptions = document.getElementById('description-editor-container');
		this.isEditing = false;
		this.editorsArr = [];
		this.init();
		this.listen();
	};

	Editors.prototype.init = function() {
		//$.toClass(this.editors, this.initEditor.bind(this));
	};

	Editors.prototype.listen = function() {
		Notes.events.addListener('create-new-editor', function() {
			this.create();
		}.bind(this));

		$('.add-ideas-item').on('click', function() {
			this.create();
		}.bind(this));
	};

	Editors.prototype.create = function() {
		if (!this.isEditing) {
			var editor = this.append.call(this);
			this.initEditor.call(this, editor);
			Notes.events.emit('new-idea', null);
	        Notes.events.emit('message', Notes, 'New Idea Added');
		}
	};

	Editors.prototype.getUniqid = function() {
		return parseInt(new Date().getTime() * Math.random(100), 10);
	};

	Editors.prototype.initEditor = function(editor) {
		var e = new Notes.Editor({
		   editor: editor[0]
	    });

	    this.editorsArr.push(e);
	};

	Editors.prototype.removeEditor = function(editor) {

	};

	/**
	 * @todo refactor
	 * @return {[type]} [description]
	 */
	Editors.prototype.append = function() {
	    var uniqid = this.getUniqid();
        var editor = $(this.container).append('<li class="ideas__item"><a id="delete-idea-'+uniqid+'" href="javascript:;" class="delete-idea btn--small" data-uniqid="'+uniqid+'"><span data-uniqid="'+uniqid+'" class="ion-android-close"></span></a><textarea id="ideas-editor-'+uniqid+'" class="editor ideas-editor" data-uniqid="'+uniqid+'" placeholder="Idea"></textarea></li>');
        $(this.descriptions).append('<div id="description-editor-'+uniqid+'" class="editor description-editor" data-uniqid="'+uniqid+'" contenteditable></div><a id="create-footnote-btn-'+uniqid+'" class="create-footnote-btn ion-android-chat" data-note-id="'+uniqid+'" href="javascript:;"></a>');
        
        var desc = new Notes.elements.Description({
            description: document.getElementById('description-editor-' + uniqid)
        });

        $('.ideas-editor').parent().removeClass('active');

        var ed = $('#ideas-editor-'+uniqid);
        ed.focus();
        // needs to to be refactored
        ed.parent().addClass('active');
        desc.addActive(uniqid);
        return ed;
	};

	Notes.Editors = Editors;

})(Notes, window.setTimeout);