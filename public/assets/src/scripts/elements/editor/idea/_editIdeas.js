(function(Notes) {

	var $ = Notes.elemental;
	var _ = Notes.events;

	var EditIdeasMenu = function() {
		this.isIdeaEditing = false;
		this.init();
		this.listen();
	};

	EditIdeasMenu.prototype.init = function() {
		this.addIdeasButton = document.getElementById('add-ideas-item');
		this.ideasEditorContainer = document.getElementById('ideas-editor-container');
		this.ideasEditors = document.getElementsByClassName('ideas-editor');
		this.closeOptionsButton = document.getElementById('close-editor-options-button');
		this.openOptionsButton = document.getElementById('open-editor-options-button');
		console.log(this.openOptionsButton);
		this.deleteIdeaButton = document.getElementsByClassName('delete-idea');
	};

	EditIdeasMenu.prototype.listen = function() {

		this.openOptionsButton.addEventListener('click', function() {
			this.toggle();
		}.bind(this));

		this.closeOptionsButton.addEventListener('click', function() {
			this.toggle();
		}.bind(this));
	};

	EditIdeasMenu.prototype.toggleDeleteButton = function() {
		Array.prototype.forEach.call(this.deleteIdeaButton, function(button) {
			this.isEditing ? button.style.display = 'block' : button.style.display = 'none';
		}.bind(this));
	};

	EditIdeasMenu.prototype.toggleIdeasEditor = function() {
		Array.prototype.forEach.call(this.ideasEditors, function(editor) {
			this.isEditing ? editor.setAttribute('disabled', 'disabled') : editor.removeAttribute('disabled');
		}.bind(this));
	};

	EditIdeasMenu.prototype.toggleIdeasEditorContainer = function() {
		this.isEditing ? $.addClass(this.ideasEditorContainer, 'editing') : $.removeClass(this.ideasEditorContainer, 'editing');
	};

	EditIdeasMenu.prototype.toggleAddIdeasButton = function() {
		this.isEditing ? $.addClass(this.addIdeasButton, 'disabled') : $.removeClass(this.addIdeasButton, 'disabled');
	};

	EditIdeasMenu.prototype.toggleOptionsButton = function() {
		if (this.isEditing) {
			this.openOptionsButton.style.display = 'none';
			this.closeOptionsButton.style.display = 'inline';
		} else {
			this.openOptionsButton.style.display = 'block';
			this.closeOptionsButton.style.display = 'none';
		}
	};

	EditIdeasMenu.prototype.toggle = function() {
		this.isEditing = !this.isEditing;
		this.toggleAddIdeasButton();
		this.toggleIdeasEditorContainer();
		this.toggleIdeasEditor();
		this.toggleDeleteButton();
		this.toggleOptionsButton();

		_.emit('isIdeaEditing', null, this.isEditing);
		
		if (this.isEditing)
			_.emit('message', null, 'Editing Ideas');

	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.EditIdeasMenu = EditIdeasMenu;

})(Notes);