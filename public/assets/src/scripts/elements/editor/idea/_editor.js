(function(Notes) {

	var $ = Notes.elemental;
	var _ = Notes.events;

	var Editor = function(options) {
		this.editor = options.editor;
		this.id = this.editor.dataset.uniqid;
		this.deleteBtn = document.getElementById('delete-idea-' + this.id);
		this.isIdeaEditing = false;
		this.isDescriptionActive = false;
		this.isCommentActive = false;
		this.descriptionEditor = document.getElementById('description-editor-' + this.id);
		this.createFootnoteButton = document.getElementById('create-footnote-btn-' + this.id);
		// select all footnotes by data attribute
		this.footNotes = document.getElementById('footnote-' + this.id);
		var editMenu = new Notes.elements.EditIdeasMenu();
		var shortcuts = new Notes.utilities.EditorShortcuts({editor: this.editor});
		$.hasClass(this.editor, 'ideas-editor');
		this.listen();
	};

	Editor.prototype.init = function() {
	};

	/**
	 * Each editor is listening for focus and inputs 
	 * @return [type]} [description]
	 */
	Editor.prototype.listen = function() {

		
		//_('isIdeaEditing').listen(changeIsIdeaEditing);

		// objects listen api
		_.listen({
			isIdeaEditing: this.changeIsIdeaEditing,
			editorFocus: [this.editor, 'focus', this.onFocus],
			"description-focus": this.changeDescriptionFocus,
			"comment-focus": this.changeCommentFocus,
			editorBlur: [this.editor, 'blur', this.onBlur]
			onDelete: [this.deleteBtn, 'click', this.delete]
		});

		_.addListener('isIdeaEditing', function (bool) {
			this.isIdeaEditing = bool;
		}.bind(this));

		this.editor.addEventListener('focus', function() {
			this.addActiveClass.call(this);
			_.emit('idea-focus', null, this.id, true);
		}.bind(this));

		_.addListener('description-focus', function(id, hasFocus) {
			if (id === this.id.toString()) {
				this.isDescriptionActive = hasFocus;
			}
		}.bind(this));

		_.addListener('comment-focus', function(id, hasFocus) {
			if (id === this.id.toString()) {
				this.isCommentActive = hasFocus;
			}
		}.bind(this));

		this.editor.addEventListener('blur', function() {
			this.removeActiveClass.call(this);
			_.emit('idea-focus', null, this.id, false);
		}.bind(this));

		this.deleteBtn.addEventListener('click', function() {
			this.delete();
		}.bind(this));

	};

	Editor.prototype.addActiveClass = function() {
		if ($.hasClass(this.editor, 'ideas-editor')) {

			$.toClass('ideas-editor', function(editor) {
				$.removeClass(editor.parentNode, 'active');
			});

			$.addClass(this.editor.parentNode, 'active');
		}
	};

	Editor.prototype.removeActiveClass = function() {
		$.removeClass(this.editor.parentNode, 'active');
	};

	Editor.prototype.delete = function() {
		this.editor.parentNode.remove();
		this.descriptionEditor.parentNode.remove();
		this.createFootnoteButton.remove();
		this.footNotes.remove();
		_.emit('message', null, 'Idea Removed');
	};

	Notes.Editor = Editor;

})(Notes);