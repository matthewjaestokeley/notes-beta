(function(Notes, setTimeout) {

	var _ = Notes.events;

	var EditorShortcuts = function(options) {
		this.isIdeaEditing = false;
		this._carriageLog = [];
		this.editor = options.editor;
		this.listen();
	};

	EditorShortcuts.prototype.init = function() {

	};

	EditorShortcuts.prototype.listen = function() {

		_.addListener('isIdeaEditing', function (bool) {
			this.isIdeaEditing = bool;
		}.bind(this));

		$(this.editor).on('keydown', function(event) {
			this.carriageListener(event);
		}.bind(this));

	};

	EditorShortcuts.prototype.carriageListener = function(event) {
		if (event.keyCode === 13) {
            event.preventDefault();
            if (this._carriageLog.length === 1) {
              this.create.call(this);
              this._carriageLog = []; 
            } else {
              this._carriageLog.push(event);
            }
        } else {
            if (this._carriageLog.length > 0) {
                this._carriageLog = [];
            }
        }
	};

	EditorShortcuts.prototype.create = function() {
		if(!this.isIdeaEditing) {
			_.emit('create-new-editor');
		}
	};

	if (Notes.utilities === undefined) {
		Notes.utilities = {};
	}

	Notes.utilities.EditorShortcuts = EditorShortcuts;

})(Notes, window.setTimeout);