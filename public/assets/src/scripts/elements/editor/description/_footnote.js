/**
 * @todo refactor
 * @param  {[type]} Notes [description]
 * @return {[type]}       [description]
 */
(function(Notes) {

    var _ = Notes.events;

    var Footnote = function(options, create) {
        this.marker = options.marker;
        this.container = options.container;
        this.id = options.id;
        this.noteId = options.noteId;
        this.description = options.description;
        this.isActive = false;

        if (!this.marker) {
            return false;
        }

        if(create) {
            this.create();   
        } else {
            this.load(options);
        }
        this.listen();
    }

    Footnote.prototype.create = function() {
        var node = document.createElement('div');
        node.className = "footnote";
        node.id = "footnote-" + this.id;
        node.dataset.id = this.id;
        node.dataset.noteId = this.noteId;
        var input = document.createElement('textarea');
        input.className = 'footnote-input';
        var deleteBtn = document.createElement('button');
        var deleteIcon = document.createElement('span');
        deleteIcon.className = "ion-android-cancel";
        deleteBtn.appendChild(deleteIcon);
        node.appendChild(input);
        node.appendChild(deleteBtn);
        this.container.appendChild(node);
        this.footnote = node;
        this.deleteBtn = deleteBtn;
        this.show();

    };

    Footnote.prototype.load = function(options) {
        this.footnote = options.footnote;
        this.deleteBtn = options.deleteBtn;
    };

    Footnote.prototype.listen = function() {
        
        this.marker.addEventListener('click', function() {
            this.show();
        }.bind(this));

        this.description.addEventListener('click', function(event) {
            if(event.target !== this.marker) {
              this.hide();
            }
        }.bind(this));

        _.addListener('description-focus', function(id, isActive) {
            if(!isActive && id === this.noteId) {
                // this.container.style.display = 'none';
            }
        }.bind(this));

        _.addListener('idea-focus', function(id, isActive) {
            this.hide();
        }.bind(this));

        _.addListener('new-idea', function() {
            this.hide();
        }.bind(this));

        this.deleteBtn.addEventListener('click', function() {
            this.footnote.remove();
            this.marker.remove();
        }.bind(this));
    };

    Footnote.prototype.toggle = function() {
        if (this.isActive) {
            this.hide();
        } else {
            this.show();
        }
    };

    Footnote.prototype.show = function() {
        if (!this.isActive) {
            $(this.marker).addClass('active');
            $('#ideas-editor-'+this.noteId).parent().addClass('active');
            $('.footnote').hide();
            $('#comments-container').show();
            this.footnote.style.display = 'block';
            $(this.footnote).addClass('active');
            this.isActive = true;
            $(this.footnote).find('textarea').focus();
            _.emit('comment-focus', null, this.noteId, true);
            $('#ideas-editor-' + this.noteId).parent().addClass('active');
        }
    };

    Footnote.prototype.hide = function() {
        if(this.isActive) {
            $('#comments-container').hide();
            $(this.marker).removeClass('active');
            this.footnote.style.display = 'none';
            this.footnote.className = 'footnote';
            this.isActive = false;
            _.emit('comment-focus', null, this.id, false);
        }
    }

    Footnote.prototype.save = function() {
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.Footnote = Footnote;


})(Notes);