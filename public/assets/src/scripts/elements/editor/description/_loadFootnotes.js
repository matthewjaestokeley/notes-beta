(function(Notes) {

    var LoadFootnotes = function(options) {
        var footnotes = document.getElementsByClassName('footnote');
        for(var i = 0; i < footnotes.length; i++) {
            var comment = document.getElementById('footnote-' + footnotes[i].dataset.id);
            var footnote = new Footnote({
               marker: document.getElementById('footnote-marker-' + footnotes[i].dataset.id),
               footnote: comment,
               container: document.getElementById('comments-container'),
               id: footnotes[i].dataset.id,
               noteId: footnotes[i].dataset.noteId,
               description: document.getElementById('description-editor-' + footnotes[i].dataset.noteId),
               deleteBtn: comment.getElementsByTagName('button')[0]
           });          
        }
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.LoadComments = LoadFootnotes;

})(Notes);