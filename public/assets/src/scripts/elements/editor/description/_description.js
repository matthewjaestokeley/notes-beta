(function(Notes, getSelection) {

    var _ = Notes.events;

	var Description = function(options) {
		this.editor = options.description;
        this.id = this.editor.dataset.uniqid;
        this.active = false;
        this.commentHasFocus = false;
        this.init();
        this.listen();
	};

    Description.prototype.init = function init() {

        this.commentBtn = new Notes.elements.CommentButton({
            button: document.getElementById('create-footnote-btn-' + this.id)
        });

        this.footnoteBtn = document.getElementById('create-footnote-btn-' + this.id);

        var wysiwyg = new Notes.elements.Wysiwyg({
            editor: this.editor
        });

        this.wysiwyg = wysiwyg.wysiwyg;
    };

    Description.prototype.getFootnoteId = function() {
        return parseInt(new Date().getTime() * Math.random(100), 10);
    };

    Description.prototype.insertTextAtCursor = function insertTextAtCursor(text, id, noteId) {
        var sel, range, html;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                var div = document.createElement('input');
                div.className = "footnote-marker";
                div.readOnly = true;
                div.id = 'footnote-marker-' + id;
                div.dataset.id = id;
                div.dataset.noteId = noteId;
                range.insertNode( div );
                return div; 
            }
        } else if (document.selection && document.selection.createRange) {
            document.selection.createRange().text = text;
        }
    };

    Description.prototype.listen = function() {

        $(this.editor).focus(function() {
            _.emit('description-focus', null, this.editor.dataset.uniqid, true);
            this.hasFocus = true;
            this.addActive(this.id);
            $('.ideas-editor[data-uniqid="'+this.editor.dataset.uniqid+'"]').parent().addClass('active');
        }.bind(this));

        $(this.editor).focusout(function(event) {
            _.emit('description-focus', null, this.editor.dataset.uniqid, false, event.target);
            this.hasFocus = false;
            $('.ideas-editor[data-uniqid="'+this.editor.dataset.uniqid+'"]').parent().removeClass('active');
            if (!this.commentHasFocus) {
                this.removeActive(this.id, event.target);
            }
        }.bind(this));

        Notes.events.addListener('comment-focus', function(id, isFocused) {
            if (id === this.id && isFocused) {
                this.hasFocus = true;
                this.commentHasFocus = true;
                this.addActive(this.id);
            }
        }.bind(this));

        Notes.events.addListener('idea-focus', function(id, isActive) {
            isActive ? this.addActive.call(this, id) : this.removeActive.call(this, id);
        }.bind(this));

        $('#show-description-idea-' + this.id).on('click', function() {
            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

            $.Velocity($('.description__container'), {
                display: 'block',
                translateX: -w
            }, {
                duration: 200
            });

            $('.description__container').css({
                display: "none"
            });

            $.Velocity(document.getElementById('ideas-editor-container'), {
                translateX: 0
            }, {
                duration: 200,
                easing: "easeIn"
            });

            this.addActive.call(this, this.id);

        }.bind(this));

        this.footnoteBtn.addEventListener('click', function(event) {
            event.preventDefault();

            var id = this.getFootnoteId();
            var footnoteMarker = this.insertTextAtCursor('', id, this.id);
            if(!footnoteMarker) {
                return false;
            }

            Notes.events.emit('message', null, 'Added a Comment!');
            $('.footnote').hide();

            var footnote = new Notes.elements.Footnote({
               marker: footnoteMarker,
               container: document.getElementById('comments-container'),
               id: id,
               noteId: this.id,
               description: this.editor
            }, true);

        }.bind(this));
    };

    Description.prototype.addActive = function(id) {

        if (id.toString() === this.id) {
            $('.description-editor').parent().hide();
            $(this.editor).parent().show();
            $(this.editor).addClass('active');
            $('.create-footnote-btn').hide();
            this.commentBtn.show(); 
        }
        
        this.active = true;
    };

    Description.prototype.removeActive = function(id, eventTarget) {
        if (id.toString() === this.id) {
            $(this.editor).removeClass('active');
        }
        this.active = false;
    };

    Description.prototype.toggleFootnoteBtn = function() {

    };

    Description.prototype.hide = function() {
        $('.description-editor').parent().fadeOut(200);
        this.active = false;
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Description.prototype.insertHtml = function(html) {
    }

    Notes.elements.Description = Description;
    
})(Notes, window.getSelection);
