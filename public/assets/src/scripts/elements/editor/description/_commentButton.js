(function(Notes) {

    var CommentButton = function(options) {
        this.button = options.button;
    };

    CommentButton.prototype.show = function() {
        this.button.style.display = 'block';
    };

    CommentButton.prototype.hide = function() {
        this.button.style.display = 'none';
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.CommentButton = CommentButton;

})(Notes);
