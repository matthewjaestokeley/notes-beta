(function(Notes) {

  /**
   * Refactor this
   * @param {[type]} options [description]
   */
    var Ideas = function(options) {
        this.$container = options.container;
        this.$textarea = this.$container.find('textarea');
        this.$descriptions = $('#description-editor-container');
        this._carriageLog = [];

        this.init.call(this);
        this.listen.call(this);
    };

    Ideas.prototype.init = function() {
          Array.prototype.forEach.call(this.$textarea, this.addEditorListener, this);
    };

      // @todo find new focus for binding keydown
    Ideas.prototype.listen = function() {

      $('.add-ideas-item').on('click', function() {
          this.addBullet.call(this);
          Notes.events.emit('new-idea');
      }.bind(this));

      this.$container.on('click', function() {
          var activeTextarea = this.$container.find($(':focus'));
          var uniqid = activeTextarea.data('uniqid');
          $descriptions = $('.description-editor-container').find($('textarea'));
          Array.prototype.forEach.call($descriptions, function(value, index, array) {
              $(value).hide();
              if (parseInt(value.dataset.uniqid, 10) === uniqid) {
                  $(value).show();
              }
          });
      }.bind(this));
      return this;
   
    };


    /**
     * refresh all textareas
     * @return {[type]} [description]
     */
    Ideas.prototype.refresh = function() {
       this.$textarea = this.$container.find('textarea');
    };

    Ideas.prototype.addEditorListener = function(editor) {
        editor.addEventListener("keydown", function(event) {
          if (event.keyCode === 13) {
            event.preventDefault();
            if (this._carriageLog.length === 1) {
              this.addBullet.call(this);
              Notes.events.emit('new-idea');
              this._carriageLog = []; 
            } else {
              this._carriageLog.push(event);
            }
          } else {
              if (this._carriageLog.length > 0) {
                this._carriageLog = [];
              }
          }
          this.refresh(); 
        }.bind(this));
    };


    Ideas.prototype.addBullet = function() {
      // needs to create a new li
      // and a new description
      // with matching uniquid
      // 
      var uniqid = function() {
        return parseInt(new Date().getTime() * Math.random(100), 10);
      };
      var uniqid = uniqid();
      this.$container.append('<li class="ideas__item" data-item-count="1"><textarea class="editor ideas-editor" data-uniqid="'+uniqid+'" placeholder="Point"></textarea></li>');
      this.$descriptions.append('<textarea style="display:none" class="editor description-editor" data-uniqid="'+uniqid+'" placeholder="Description"></textarea>');
      Notes.events.emit('message', Notes, 'New Idea Added');
      this.addEditorListener($('textarea[data-uniqid="'+uniqid+'"]')[0])
      return this;
    };

    Ideas.prototype.removeBullet = function() {
      return this;
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.Ideas = Ideas;


})(Notes);