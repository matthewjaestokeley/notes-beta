(function(Notes, localStorage) {
	var _ = Notes.events;

	var UserMenu = function(options) {

		this.loginButton = options.loginBtn;
		this.logoutButton = options.logoutBtn;
		this.userButton = options.userBtn;
		this.user = options.user;
		this.listen.call(this);
		this.init.call(this);
	};

	UserMenu.prototype.init = function() {

	};

	UserMenu.prototype.listen = function() {
        this.loginButton.addEventListener('click', this.login.bind(this));
        this.logoutButton.addEventListener('click', this.logout.bind(this));
        _.addListener('user-login', function(user) { this.update.call(this, user); }.bind(this));
	};


	UserMenu.prototype.logout = function() {
		this.update.call(this);
		_.emit('message', Notes, 'Logging out ...');
		this.user.logout();
        localStorage.removeItem('notes-token');
        window.location = '/';
	};

	UserMenu.prototype.login = function() {
		_.emit('login-btn-clicked');
	};

	UserMenu.prototype.toggleUserButton = function(isUserLoggedIn) {
		this.user.isUserLoggedIn ? this.showUserButton() : this.showLoginButton();
	};

	UserMenu.prototype.showUserButton = function() {
		this.loginButton.style.display = 'none';
		this.userButton.innerHTML = this.user.username;
		this.userButton.style.display = '';
	};

	UserMenu.prototype.showLoginButton = function() {
        this.userButton.style.display = 'none';
		this.loginButton.style.display = 'block';
	};

	UserMenu.prototype.update = function() {
		this.toggleUserButton.call(this, this.user.isUserLoggedIn);
	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.UserMenu = UserMenu;

})(Notes, localStorage);
