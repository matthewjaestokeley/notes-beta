(function(Notes) {


	var UserNav = function() {

		this.isUserNavOpen = false;
		this.userBtn = document.getElementById('user-btn');
		this.listen();

	};

	UserNav.prototype.init = function() {
		
		var handle = function(event) {
			document.addEventListener('click', function(event) {
				if (event.toElement !== this.userBtn) {
					this.hide();
				}
			}.bind(this));
		}.bind(this);

		document.removeEventListener('click', handle);
		document.addEventListener('click', handle);

	};

	UserNav.prototype.listen = function() {

		this.userBtn.addEventListener('click', function() {

			!this.isUserNavOpen ? this.show.call(this) : this.hide.call(this);
			this.init(this.userBtn);
	
		}.bind(this));	
	};

	UserNav.prototype.show = function() {
		if (!this.isUserNavOpen) {
			//$('#user-menu').show();
			$.Velocity($('#user-menu'), "slideDown", {
				easing: "easeInOut",
				duration: 50
			});
			this.isUserNavOpen = !this.isUserNavOpen;
		}
	};

	UserNav.prototype.hide = function() {
		if(this.isUserNavOpen) {
			$.Velocity($('#user-menu'), "slideUp", {
				easing: "easeInOut",
				duration: 50
			});
			this.isUserNavOpen = !this.isUserNavOpen;
		}
	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.UserNav = UserNav;

})(Notes);