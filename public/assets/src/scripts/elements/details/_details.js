(function(Notes) {
    var e = Notes.events;

    function DetailsPane(options) {
        this.$container = options.container;
        this._areDetailsShown = false;
        this.listen.call(this);
        this.count = 1;
    }

    DetailsPane.prototype.listen = function() {
        e.addListener('details-btn-clicked', this.toggleDetailsPane.bind(this));

        $('#add-details').on('click', function() {
            $('#details').prepend('<li id="details-item-'+this.count+'" class="details__item" style="opacity: 0;"><form class="details-form"><div class="details__input-container"><input class="details-name details__input input" name="values[]" placeholder="name" value=""></div><div class="details__input-container"><input class="details-value details__input input" name="values[]" placeholder="value" value=""></div></form></li>');        
            $("#details-item-" + this.count).velocity({
              "opacity": 1
            }, {
              "duration": 300,
              "easing": "easeIn"
            });
            this.count = this.count + 1;
            Notes.events.emit('message', null, 'Adding Detail');
        }.bind(this));
    };

    DetailsPane.prototype.toggleDetailsPane = function() {
        !this._areDetailsShown ? this.open.call(this) : this.close.call(this);
        this._areDetailsShown = !this._areDetailsShown;
        return this;
    };

    DetailsPane.prototype.open = function() {
        var editor = document.getElementById('editor-section');
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        if (!this._areDetailsShown) {

          $('#note-details').css({
              position: 'absolute',
              display: 'block'
          });

          $.Velocity(this.$container, {
              display: 'block',
              translateX: w
          }, {
              duration: 0
          });

          $.Velocity(editor, {
              translateX: -w
          }, {
              duration: 200,
              easing: "easeIn"
          });

          $.Velocity(this.$container, {
              translateX: 0
          }, {
              duration: 200,
              easing: "easeIn"
          });
        }

        return this;

    };

    DetailsPane.prototype.close = function() {
        var editor = document.getElementById('editor-section');
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        if (this._areDetailsShown) {

            $.Velocity(this.$container, {
                translateX: w
            }, {
                duration: 200,
                easing: "easeIn"
            });

            $.Velocity(editor, {
                translateX: 0
            }, {
                duration: 200,
                easing: "easeIn"
            });
            
            $('#note-details').css({
                display: 'none'
            });

        }
        return this;
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.DetailsPane = DetailsPane;
    
  })(Notes);