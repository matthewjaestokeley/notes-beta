 (function(Notes, localStorage) {

 		var $ = Notes.elemental;
 		var _ = Notes.events;

		/**
		 * [Input description]
		 * @param {[type]} value [description]
		 */
		var LoginFormInput = function(input) {
			this.state = 'none';
			this.input = input;
			this.successClass = 'login-form__message success ion-android-done';
			this.errorClass = 'login-form__message error ion-android-warning';
		}
		
		/**
		 * [changeValidationState description]
		 * @param  {[type]} state [description]
		 * @return {[type]}       [description]
		 */
		LoginFormInput.prototype.changeValidationState = function(state, validated) {
			if (state !== this.state) {
				switch (state) {
					case 'none':
						this.removeIcon;
						break;
					case 'success':
						this.addIcon(this.successClass)
						break;
					case 'error':
			        	if (validated) return $.addClass(this.input, 'error');
						this.addIcon(this.errorClass);
						break;
				}
			}
			this.state = state;
		};

		/**
		 * [addIcon description]
		 * @param {[type]} className [description]
		 */
		LoginFormInput.prototype.addIcon = function(className) {
			this.removeIcon();
			this.input.insertAdjacentHTML('afterend', '<div class="'+className+'"></div>');
		};

		/**
		 * [removeIcon description]
		 * @return {[type]} [description]
		 */
		LoginFormInput.prototype.removeIcon = function() {
			this.input.nextSibling.remove();
		};

        if (Notes.elements === undefined) {
	        Notes.elements = {};
	    }
    
	    Notes.elements.LoginFormInput = LoginFormInput;

})(Notes, localStorage);