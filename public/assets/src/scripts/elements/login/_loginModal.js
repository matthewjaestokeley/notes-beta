(function(Notes) {
	 var e = Notes.events;

     /**
      *
      * @param {[type]} options [description]
      */
     var LoginModal = function(options) {
        this.modal = options.modal;
        this.toggleFormButton = options.toggleFormButton || document.getElementsByClassName('switch-form');
        this.loginForm = options.loginForm || document.getElementById('login-form');
        this.signupForm = options.signupForm || document.getElementById('signup-form');
        this._isModalOpen = false;
        this._isLoginPaneActive = true;

        this.hideForm(this.signupForm);

        this.listen.call(this);
  	};

    /**
     * [listen description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.listen = function() {

      e.addListener('login-btn-clicked', this.toggleLoginModal.bind(this));
      e.addListener('user-login', this.close.bind(this));

      Array.prototype.forEach.call(this.toggleFormButton, function(button) {
          button.addEventListener('click', function() {
              this.toggleSignupForm.call(this);
          }.bind(this));
      }.bind(this));

      return this;
    };

    /**
     * [toggleLoginModal description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.toggleLoginModal = function() {

        if(!this._isModalOpen) {
          this.open.call(this);
        } else {
          this.close.call(this);
        }
        return this;
    };

    /**
     * [toggleSignupForm description]
     * @todo  refactor
     * @return {[type]} [description]
     */
    LoginModal.prototype.toggleSignupForm = function() {
        this.hideForms([this.loginForm, this.signupForm]);
        !this._isLoginPaneActive ? this.showForm(this.loginForm) : this.showForm(this.signupForm);
     		this._isLoginPaneActive = !this._isLoginPaneActive;
   
    		return this;
    };

    /**
     * [hideForms description]
     * @param  {[type]} forms [description]
     * @return {[type]}       [description]
     */
    LoginModal.prototype.hideForms = function(forms) {
        return forms.forEach(function(form){
          this.hideForm(form);
        }.bind(this));
    };

    /**
     * [showForm description]
     * @param  {[type]} form [description]
     * @return {[type]}      [description]
     */
    LoginModal.prototype.showForm = function(form) {
        return form.style.display = 'block';
    };

    /**
     * [hideForm description]
     * @param  {[type]} form [description]
     * @return {[type]}      [description]
     */
    LoginModal.prototype.hideForm = function(form) {
        return form.style.display = 'none';
    };

    /**
     * [open description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.open = function() {
      if (!this._isModalOpen) {
        $.Velocity(this.modal, "slideDown", {
          duration: 500,
          easing: 'spring'
        });
      }
      this._isModalOpen = !this._isModalOpen;
      return this;
    };

    /**
     * [close description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.close = function() {
      if (this._isModalOpen) {
        $.Velocity(this.modal, "slideUp", {
          duration: 150,
          easing: "easeIn"
        });
        this._isModalOpen = !this._isModalOpen;
      }
      return this;
    };

  	if (Notes.elements === undefined) {
  		Notes.elements = {};
  	}

  	Notes.elements.LoginModal = LoginModal;

})(Notes);
