 (function(Notes, localStorage) {

 		var $ = Notes.elemental;
 		var _ = Notes.events;

		/**
		 * @todo refactor
		 * @param {[type]} form [description]
		 */
		var LoginForm = function(options) {
			this.form = options.form;
			this.inputs = this.form.getElementsByTagName('input');
			this.input = {};
			this.userField = this.inputs[0];
			this.passwordField = this.inputs[1];
			this.submitButton = this.form.getElementsByTagName('button')[0];
			this.init();
			this.listen();
		};

		/**
		 * [init description]
		 * @return {[type]} [description]
		 */
		LoginForm.prototype.init = function() {
			this.button = new Notes.elements.LoginFormButton({
				button: this.submitButton
			});
		};

		/**
		 * [changeValidationState description]
		 * @param  {[type]} value [description]
		 * @param  {[type]} input [description]
		 * @return {[type]}       [description]
		 */
		LoginForm.prototype.changeValidationState = function(value, input) {
			this.validate(value) ? input.changeValidationState('success') : input.changeValidationState('error');
		};

		/**
		 * [listen description]
		 * @return {[type]} [description]
		 */
		LoginForm.prototype.listen = function() {

			Array.prototype.forEach.call(this.inputs, function(value, index, array) {
				var input = this.input[value.dataset.field] = new Notes.elements.LoginFormInput(value);
				value.addEventListener('keyup', function(event) {
					this.changeValidationState(value.value, input);
				}.bind(this));

				value.addEventListener('focus', function(event) {
					$.removeClass(value, 'error');
					this.changeValidationState(value.value, input);
				}.bind(this));

			}.bind(this));

			this.submitButton.addEventListener('click', function(event) {
				event.preventDefault();
				this.button.changeButtonState('waiting');
				this.submit.call(this, this.submitButton.dataset.function);
			}.bind(this));

			// response listener
			_.addListener('login-response', function(response) {
				response === 'success' ? this.button.changeButtonState(response) : this.button.changeButtonState('ready');
			}.bind(this));

			_.addListener(this.submitButton.dataset.function, this.response.bind(this));

		};

		/**
		 * [validate description]
		 * @param  {[type]} value [description]
		 * @return {[type]}       [description]
		 */
		LoginForm.prototype.validate = function(value) {
			
			if (value === '') {
				_.emit('message', null, 'This field can\'t be empty');
				return false;
			}

			if (value.length > 14) {
				_.emit('message', null, 'Too long!');
				return false;
			}
			return true;

		};

		LoginForm.prototype.validateOnSubmit = function() {
			
			Array.prototype.forEach.call(this.inputs, function(input) {
 				if (input.value === '') {
					this.input[input.dataset.field].changeValidationState('error');
					_.emit('login-response', null, 'error');
					_.emit('message', this, 'The form can\'t be blank!');
				}
			}.bind(this));

		};

		LoginForm.prototype.resetInputs = function() {
			Array.prototype.forEach.call(this.inputs, function(input) {
				Notes.elemental.removeClass(input, 'error');
			});
		};

		LoginForm.prototype.getFormValues = function() {
			
			return {
				username: this.inputs[0].value,
				password: this.inputs[1].value
			};
		
		};

		LoginForm.prototype.submit = function(type) {
			this.resetInputs();
	        this.validateOnSubmit();
	        var values = this.getFormValues();
	        var doc = {
	            method: "POST",
	            endpoint: type,
	            name: type,
	            data: {
	                username: values.username,
	                password: values.password
	            }
	        };

	        Notes.api.write(doc);
	    };

    	LoginForm.prototype.response = function(data) {

	        if (data.error !== undefined) {
	        }

	        if (data.hasOwnProperty('token')) {
	            // new user
	            _.emit('user-login', this, data);
	            _.emit('message', this, 'Successfully logged in!'); 
	            _.emit('login-response', null, 'success');  
	            
	         	if (window.location.pathname === '/login') 
	         		window.location = '/';
		       

	        } else {
	            _.emit('message', Notes, data.error);
	            _.emit('login-response', null, 'error'); 
	            if (data.error === "Invalid Password") {
	            	this.input['password'].changeValidationState('error', true);
	            } else if(data.error === "Unknown User") {
	            	this.input['login'].changeValidationState('error', true);
	            } else if(data.error === 'User Exists') {
  	            	this.input['login'].changeValidationState('error', true);
	            }
	        }
	    }

        if (Notes.elements === undefined) {
	        Notes.elements = {};
	    }
    
	    Notes.elements.LoginForm = LoginForm;

})(Notes, localStorage);