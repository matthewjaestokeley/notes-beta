 (function(Notes, localStorage) {

 		var $ = Notes.elemental;
 		var _ = Notes.events;

		/**
		 * [Button description]
		 * @param {[type]} options [description]
		 */
		var LoginFormButton = function(options) {
			this.button = options.button;
			this.loaderClassName = options.loaderClassName || 'btn__loader';
			this.successClassName = options.successClassName || 'btn__success';
			this.contentClassName = options.contentClassName || 'btn__content';
			this.loader = this.button.getElementsByClassName(this.loaderClassName)[0];
			this.success = this.button.getElementsByClassName(this.successClassName)[0];
			this.content = this.button.getElementsByClassName(this.contentClassName)[0];
			this.inner = [this.loader, this.success, this.content];
		};

		/**
		 * [changeButtonState description]
		 * @param  {[type]} state [description]
		 * @return {[type]}       [description]
		 */
		LoginFormButton.prototype.changeButtonState = function(state) {
			this.setDefault();
			switch (state) {
				case 'ready':
					this.show(this.content);
					break;
				case 'waiting':
					this.show(this.loader);
					break;
				case 'success':
					this.show(this.success);
					break;
			}
		};

		/**
		 * [hide description]
		 * @param  {[type]} element [description]
		 * @return {[type]}         [description]
		 */
		LoginFormButton.prototype.hide = function(element) {
			return element.style.display = 'none';
		};

		/**
		 * [show description]
		 * @param  {[type]} element [description]
		 * @return {[type]}         [description]
		 */
		LoginFormButton.prototype.show = function(element) {
			return element.style.display = 'block';
		};

		/**
		 * [setDefault description]
		 */
		LoginFormButton.prototype.setDefault = function() {
			this.inner.forEach(function(content) {
				this.hide.call(this, content);
			}.bind(this));
		};

        if (Notes.elements === undefined) {
	        Notes.elements = {};
	    }
    
	    Notes.elements.LoginFormButton = LoginFormButton;

})(Notes, localStorage);