	(function(Notes, window) {

	var _ = Notes.events;

	var NoteMenu = function(options) {
		this.$newBtn = options.newBtn;
		this.$reminderBtn = options.reminderBtn;
		this.$detailsBtn = options.detailsBtn;
		this.$saveBtn = options.saveBtn;
		this.isDetailsActive = false;
		this.init.call(this);
		this.listen.call(this);
	};

	NoteMenu.prototype.init = function() {

	};

	NoteMenu.prototype.listen = function() {
		this.$newBtn.addEventListener('click', function() {
			window.location = '/';
		});

		this.$detailsBtn.addEventListener('click', function() {
			this.isDetailsActive = !this.isDetailsActive;
			if (this.isDetailsActive) {
				$(this.$detailsBtn).addClass('active');
			} else {
				$(this.$detailsBtn).removeClass('active');
			}
			_.emit('details-btn-clicked');
		}.bind(this));

		this.$saveBtn.addEventListener('click', function() {
			_.emit('save-btn-clicked');
		});

	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.NoteMenu = NoteMenu;

})(Notes, window);