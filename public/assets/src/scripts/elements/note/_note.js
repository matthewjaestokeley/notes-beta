(function(Notes, localStorage) {

    var Note = function(options) {

        this.note = options.note;
      
        this.listen();
    };

    Note.prototype.listen = function() {
        Notes.events.addListener('save-btn-clicked', function() {
            if (!Notes.user.isUserLoggedIn) {
                alert('Please login or create an account to save');
            } else {
                this.save.call(this);
            }
        }.bind(this));
    };

    Note.prototype.getNoteData = function() {          
        var doc = {};
        doc.fields = {};
        doc.ideas = [];
        doc.descriptions = [];
        doc.details = [];
        doc.footnotes = [];

        var serialize = function(value, index) {
            var id;
            if (value.dataset !== undefined) {
              if (value.dataset.uniqid !== undefined) {
                id = value.dataset.uniqid;
              }
            }

            if (id) {
              if(value.classList.contains('ideas-editor')) {
                  doc.ideas.push({
                    "name": value.name,
                    "value": value.value,
                    "id": id
                  });
              }
              if (value.classList.contains('description-editor')) {
                  doc.descriptions.push({
                    "name": value.name,
                    "value": value.innerHTML,
                    "id": id
                  });
              }
              
            } else {
              if (value.name === 'title') {
                if (value.value === '' || value.value === undefined) {
                  value.value = 'Untitled'
                }
              }
              doc.fields[value.name] = {
               "value": value.value
              }
            }
            
        }

        var serializeDetails = function(value) {
            var name = value.getElementsByClassName('details-name');
            var val = value.getElementsByClassName('details-value');
            doc.details.push({
                name:  name[0].value,
                value: val[0].value
            });
        };

        var serializeFootnotes = function(value) {
            var input = value.getElementsByTagName('textarea');
            doc.footnotes.push({
                name: input[0].name,
                value: input[0].value,
                id: value.dataset.id,
                noteId: value.dataset.noteId
            });
        };

        var inputs = document.getElementsByClassName('input');
        var editors = document.getElementsByClassName('editor');
        var details = document.getElementsByClassName('details-form');
        var footnotes = document.getElementsByClassName('footnote');

        Array.prototype.forEach.call(editors, serialize, this);
        Array.prototype.forEach.call(inputs, serialize, this);
        Array.prototype.forEach.call(footnotes, serializeFootnotes, this);
        Array.prototype.forEach.call(details, serializeDetails, this);
        return doc;

    };

    Note.prototype.serialize = function() {

    };

    Note.prototype.save = function() {
        var doc = {
            data: {
                note: this.getNoteData(),
                noteId: this.note.dataset.noteId,
                token: localStorage.getItem('notes-token'),
            },
            method: "POST",
            endpoint: "note/save",
            message: {
                name: "message",
                context: Notes,
                message: 'Note Saved',
                type: 'success'
            }
        };

        Notes.api.write(doc);
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.Note = Note;

})(Notes, localStorage);
