(function(Notes, localStorage) {
    var _ = Notes.events;
    var api = Notes.api;

    var LoadNote = function(options) {
        return this.init().listen().loadNote();
    };

    LoadNote.prototype.init = function() {
        this.id = '';
        this.note = document.getElementById('note');
        this.ideasEditors = document.getElementsByClassName('ideas-editor');
        this.descriptionEditors = document.getElementsByClassName('description-editor');
        return this;
    };

    LoadNote.prototype.listen = function() {
        _.addListener('on-get-note', this.onGetNote.bind(this));
    };

    LoadNote.prototype.onGetNote = function(data) {
        this.note = document.getElementById();
        $('#note').append(data.html);
        this.init();
  
        $('.ideas-editor').each(function(editor) {
          new Notes.Editor({
              editor: document.getElementById(this.id)
          });
        });

      $('.description-editor').each(function(editor) {
          var ed = document.getElementById(this.id);
          var desc = new Notes.elements.Description({
              description: ed
          });
          desc.insertHtml(ed.innerHTML);
      });

      _.emit('note-loaded');
      _.emit('message', Notes, 'Note Loaded', 'info');

      return this;
    };

    LoadNote.prototype.loadNote = function() {

      api.get({
          name: 'on-get-note',
          method: 'POST',
          endpoint: 'note/' + $('#note').data('noteId'),
          data: {
            token: localStorage.getItem('notes-token')
          }
      });

      return this;
    }

    if (Notes.init === undefined) {
        Notes.init = {};
    }

    Notes.init.LoadNote = LoadNote;

})(Notes, localStorage);
