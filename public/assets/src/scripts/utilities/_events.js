(function(Notes) {

// meant to be inherited into another objects prototype
    var events = (function() {

        var listeners = [];

        var addListener = function(name, callback) {

            listeners.push({
            	name: name,
            	callback: callback
            });
        
        };

        var emit = function(name, context) {

            if (typeof name !== 'string') {

            }

            var args = Array.prototype.splice.call(arguments, 2);

            listeners.forEach(function(listener, index, array) {
                for (var prop in listener) {
                    if(listener.hasOwnProperty(prop)) {
                        if ((prop === 'name') && (listener[prop] === name)) {
                            listener.callback.apply(context || this, args);
                        }
                    }
                }
            }.bind(this));
            return this;
        };

        return {
          addListener: addListener,
          emit: emit
        }

    })();

	Notes.events = events;

})(window.Notes);