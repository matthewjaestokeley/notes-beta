(function(Notes) {

	var elemental = {
	    addClass: function(element, className) {
	        if (element.classList && element.classList.add) {
	            element.classList.add(className);
	        } else if (element.className) {
	            element.className = element.className + ' ' + className;
	        }
	        return element;
	    },
	    removeClass: function(element, className) {
	        if (element.classList && element.classList.remove) {
	            element.classList.remove(className);
	        } else if (element.className) {
	            element.className = element.className.toString().replace(className, '');
	        }
	        return element;
	    },
	    removeClassWithPrefix: function(element, prefix) {
	        if (element.classList && element.classList.remove) {
	            element.classList.remove(prefix);
	        } else if (element.className) {
	            element.className = element.className.toString().replace(/[aA-zZ\-]*/, '');
	        }
	        return element;
        },
        hasClass: function(element, className) {
        	if (element.classList && element.classList.remove) {
        		for (var property in element.classList) {
        			if (element.classList.hasOwnProperty(property)) {
        				if (element.classList[property] === className)
        					return true;
        			}
        		}
	        } else if (element.className) {
	        	if (element.className.toString.indexOf(/[aA-zZ\-]*/)) {
	        		return true;
	        	}
	        }
	        return false;
        },
		toClass: function(className, fn) {
        	Array.prototype.forEach.call(document.getElementsByClassName(className), fn);
        },
        createElement: function(options) {
        	var element = document.createElement(options.element);        	
        	for(var prop in options.properties) {
        		if (options.hasOwnProperty(prop)) {
        			elements[prop] = options.properties[prop];
        		}
        	}
        	return element; 
        },
        // @refactor
        removeElement: function() {
        	if (!('remove' in Element.prototype)) {
			    Element.prototype.remove = function() {
			        if (this.parentNode) {
			            this.parentNode.removeChild(this);
			        }
			    };
			} else {
				element.remove();
			}
        }
	};

	Notes.elemental = elemental;

})(Notes);