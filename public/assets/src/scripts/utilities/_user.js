(function(Notes, localStorage) {

  var _ = Notes.events;

  var User = function() {
 		  this.isUserLoggedIn = false;
      this.listen.call(this);
      this.loadUser.call(this);
  };

  User.prototype.init = function(options) {
      
      if (typeof options === 'string') {
        options = JSON.parse(options);
      }

      if (options.token)
          localStorage.setItem('notes-token', options.token);

      this.username = options.username;
      
      this.login();
  };

  User.prototype.listen = function() {
      _.addListener('user-login', this.init.bind(this));
  };

  User.prototype.doesUserTokenExist = function() {
      if (localStorage.getItem('notes-token')) {
          return true;
      }
      return false;
  };

  User.prototype.loadUser = function() {
    if (this.doesUserTokenExist()) {
      Notes.api.write({
        method: "POST",
        endpoint: "user",
        name: "user-login",
        data: {
          token: localStorage.getItem('notes-token')
        }
      });
    }
  };

  User.prototype.login = function() {
  		this.isUserLoggedIn = true;
  		return this;
  };

  User.prototype.logout = function() {
  		this.isUserLoggedIn = false;
  		return this;
  };

  User.prototype.getStatus = function() {
  	return this.isUserLoggedIn;
  };

  User.prototype.getUsername = function() {
  		return this.username;
  };

  Notes.User = User;

})(Notes, localStorage)