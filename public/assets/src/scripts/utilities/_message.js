(function(Notes) {

	var message = (function(message) {

	Notes.events.addListener('message', function(message) {
	    var message = document.getElementById('console');
	    var consoleMessage = $(message).append("<div class='console-message'>"+message+"</div>");
	    $('#console').velocity("slideDown", {
          duration: 250,
          easing: "spring"
        }).delay(1000).velocity("slideUp", {
        	duration: 250,
        	easing: "spring"
        });
	});

	})();

	Notes.message = message;

})(Notes || {});