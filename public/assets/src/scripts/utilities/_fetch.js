(function(Notes, window) {

  var fetch = function(options) {

      $.ajax({
        url: options.url,
        type: options.method,
        data: options.data,
        contentType: 'application/json',
        dataType: 'json',
      }).error(function(data) {
        if (data.status === 401) {
          // if (window.location.pathname !== '/tour') {
          window.location = '/login';
          // }

      }
      }).done(function(data) {
          options.callback(data);
      });
   
    };


  Notes.fetch = fetch;


  var message = (function(options) {

    Notes.events.addListener('message', function(message) {
      var messageQueue = document.getElementById('console');
      var m = "<div class='console-message'>"+message+"</div>";
      var consoleMessage = $(m).appendTo(messageQueue).velocity("slideDown", {
          easing: "spring",
          duration: 300
      }).delay(2000).fadeOut(100, function() {
          this.remove();
      });

    });

  })();

  Notes.message = message;

})(Notes || {}, window)