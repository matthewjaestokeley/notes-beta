(function(Notes) {

    var uniqueId = function() {
        return parseInt(new Date().getTime() * Math.random(100), 10);
    };

    Notes.uniqueId = uniqueId;

})(Notes || {})