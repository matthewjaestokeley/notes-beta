/**
 * @todo an wrapper for whatever xhr client is being used
 * @param  {[type]} Notes        [description]
 * @param  {[type]} localStorage [description]
 * @return {[type]}              [description]
 */
(function(Notes, localStorage) {

  var e = Notes.events;
  var api = (function() {
    //var rootUrl = 'http://ec2-52-36-65-67.us-west-2.compute.amazonaws.com/api/';
    var rootUrl = 'http://localhost:3000/api/';
    return {
        write: function(options) {
            return Notes.fetch({
                "method": options.method,
                "url": rootUrl + options.endpoint,
                "data": JSON.stringify(options.data),
                "callback": function(response) {
                    if (options.name) {
                        e.emit(options.name, options.context || null, response);
                    } else if (options.message) {
                        e.emit(options.message.name, options.message.context, options.message.message, options.message.type);
                    }
                }
            });
        },
        get: function(options) {
            return Notes.fetch({
                "method": options.method,
                "url": rootUrl + options.endpoint,
                "data": JSON.stringify(options.data),
                "callback": function(response) {
                    e.emit(options.name, options.context || null, response);
                }
            });
        }
    }

  })();

  Notes.api = api;


})(Notes, localStorage)