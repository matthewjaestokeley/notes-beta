(function(Notes) {

	var Tours = function() {

		function getSearchParameters() {
		      var prmstr = window.location.search.substr(1);
		      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
		}

		function transformToAssocArray( prmstr ) {
		    var params = {};
		    var prmarr = prmstr.split("&");
		    for ( var i = 0; i < prmarr.length; i++) {
		        var tmparr = prmarr[i].split("=");
		        params[tmparr[0]] = tmparr[1];
		    }
		    return params;
		}

		var params = getSearchParameters();
		if (params.leg === undefined) {
			window.location = '/tour?leg=1'
		}

		var show = function(leg) {
			$('.tour-leg').hide();
			var active = $('.tour-leg[data-leg="'+leg+'"]');
			active.show();
		}

		show(params.leg);

	};	

	Notes.Tours = Tours;

})(Notes);