!function(e){"use strict";"function"==typeof define&&define.amd?define([],function(){return e(window,document)}):"undefined"!=typeof exports?module.exports=e(window,document):window.wysiwyg=e(window,document)}(function(e,t){"use strict";var n=function(e,t,n){var r;return function(){if(r){if(!n)return;clearTimeout(r)}var o=this,i=arguments;r=setTimeout(function(){r=null,e.apply(o,i)},t)}},r=function(t,n,r,o){t.addEventListener?t.addEventListener(n,r,o?!0:!1):t.attachEvent?t.attachEvent("on"+n,r):t!=e&&(t["on"+n]=r)},o=function(t,n,r,o){t.removeEventListener?t.removeEventListener(n,r,o?!0:!1):t.detachEvent?t.detachEvent("on"+n,r):t!=e&&(t["on"+n]=null)},i=function(e,n,r,o){if(t.createEvent){var i=t.createEvent("Event");i.initEvent(n,void 0!==r?r:!0,void 0!==o?o:!1),e.dispatchEvent(i)}else if(t.createEventObject){var i=t.createEventObject();e.fireEvent("on"+n,i)}else"function"==typeof e["on"+n]&&e["on"+n]()},a=function(e){return e.preventDefault?e.preventDefault():e.returnValue=!1,e.stopPropagation?e.stopPropagation():e.cancelBubble=!0,!1},l="undefined"!=typeof Node?Node.ELEMENT_NODE:1,u="undefined"!=typeof Node?Node.TEXT_NODE:3,f=function(e,t){for(var n=t;n;){if(n===e)return!0;n=n.parentNode}return!1},c=function(e,t){if(e.firstChild)return e.firstChild;for(;e;){if(e==t)return null;if(e.nextSibling)return e.nextSibling;e=e.parentNode}return null},s=function(n){if(e.getSelection){var r=e.getSelection();if(r.rangeCount>0)return r.getRangeAt(0)}else if(t.selection){var r=t.selection;return r.createRange()}return null},d=function(n,r){if(r)if(e.getSelection){var o=e.getSelection();o.removeAllRanges(),o.addRange(r)}else t.selection&&r.select()},p=function(){if(e.getSelection){var n=e.getSelection();if(!n.rangeCount)return!1;var r=n.getRangeAt(0).cloneRange();if(r.getBoundingClientRect){var o=r.getBoundingClientRect();if(o&&o.left&&o.top&&o.right&&o.bottom)return{left:parseInt(o.left),top:parseInt(o.top),width:parseInt(o.right-o.left),height:parseInt(o.bottom-o.top)};for(var i=r.getClientRects?r.getClientRects():[],a=0;a<i.length;++a){var o=i[a];if(o.left&&o.top&&o.right&&o.bottom)return{left:parseInt(o.left),top:parseInt(o.top),width:parseInt(o.right-o.left),height:parseInt(o.bottom-o.top)}}}}else if(t.selection){var n=t.selection;if("Control"!=n.type){var r=n.createRange();if(r.boundingLeft||r.boundingTop||r.boundingWidth||r.boundingHeight)return{left:r.boundingLeft,top:r.boundingTop,width:r.boundingWidth,height:r.boundingHeight}}}return!1},g=function(n){if(e.getSelection){var r=e.getSelection();return r.isCollapsed?!0:!1}if(t.selection){var r=t.selection;if("Text"==r.type){var o=t.selection.createRange(),i=t.body.createTextRange();return i.moveToElementText(n),i.setEndPoint("EndToStart",o),0==o.htmlText.length}if("Control"==r.type)return!1}return!0},v=function(n){if(e.getSelection){var r=e.getSelection();if(!r.rangeCount)return[];for(var o=[],i=0;i<r.rangeCount;++i)for(var a=r.getRangeAt(i),u=a.startContainer,s=a.endContainer;u;){if(u!=n){var d=!1;if(r.containsNode)d=r.containsNode(u,!0);else{var p=t.createRange();p.selectNodeContents(u);for(var i=0;i<r.rangeCount;++i){var a=r.getRangeAt(i);if(a.compareBoundaryPoints(a.END_TO_START,p)>=0&&a.compareBoundaryPoints(a.START_TO_END,p)<=0){d=!0;break}}}d&&o.push(u)}u=c(u,u==s?s:n)}return 0==o.length&&f(n,r.focusNode)&&r.focusNode!=n&&o.push(r.focusNode),o}if(t.selection){var r=t.selection;if("Text"==r.type){for(var o=[],g=r.createRangeCollection(),i=0;i<g.length;++i)for(var a=g[i],v=a.parentElement(),u=v;u;){var p=a.duplicate();if(p.moveToElementText(u.nodeType!=l?u.parentNode:u),p.compareEndPoints("EndToStart",a)>=0&&p.compareEndPoints("StartToEnd",a)<=0){for(var h=!1,m=0;m<o.length;++m)if(o[m]===u){h=!0;break}h||o.push(u)}u=c(u,v)}return 0==o.length&&f(n,t.activeElement)&&t.activeElement!=n&&o.push(t.activeElement),o}if("Control"==r.type){for(var o=[],a=r.createRange(),i=0;i<a.length;++i)o.push(a(i));return o}}return[]},h=function(){if(e.getSelection){var n=e.getSelection();if(!n.isCollapsed)try{n.collapseToEnd()}catch(r){}}else if(t.selection){var n=t.selection;if("Control"!=n.type){var o=n.createRange();o.collapse(!1),o.select()}}},m=function(n,r,o){if(e.getSelection){var i=e.getSelection();if(i.modify){for(var a=0;r>a;++a)i.modify("extend","backward","character");for(var a=0;o>a;++a)i.modify("extend","forward","character")}else{var l=i.getRangeAt(0);l.setStart(l.startContainer,l.startOffset-r),l.setEnd(l.endContainer,l.endOffset+o),i.removeAllRanges(),i.addRange(l)}}else if(t.selection){var i=t.selection;if("Control"!=i.type){var l=i.createRange();l.collapse(!0),l.moveStart("character",-r),l.moveEnd("character",o),l.select()}}},y=function(n){if(g(n))return null;if(e.getSelection){var r=e.getSelection();if(r.rangeCount){for(var o=t.createElement("div"),i=r.rangeCount,a=0;i>a;++a){var l=r.getRangeAt(a).cloneContents();o.appendChild(l)}return o.innerHTML}}else if(t.selection){var r=t.selection;if("Text"==r.type){var u=r.createRange();return u.htmlText}}return null},T=function(n,r){if(e.getSelection){var o=e.getSelection();if(f(n,o.anchorNode)&&f(n,o.focusNode))return!0;if(!r)return!1;var i=t.createRange();i.selectNodeContents(n),i.collapse(!1),o.removeAllRanges(),o.addRange(i)}else if(t.selection){var o=t.selection;if("Control"==o.type){var i=o.createRange();if(0!=i.length&&f(n,i(0)))return!0}else{var a=t.body.createTextRange();a.moveToElementText(n);var i=o.createRange();if(a.inRange(i))return!0}if(!r)return!1;var i=t.body.createTextRange();i.moveToElementText(n),i.setEndPoint("StartToEnd",i),i.select()}return!0},b=function(n,r){if(e.getSelection){var o=e.getSelection();if(o.getRangeAt&&o.rangeCount){var i=o.getRangeAt(0),a=t.createElement("div");a.innerHTML=r;for(var l,u,c=t.createDocumentFragment();l=a.firstChild;)u=c.appendChild(l);f(n,i.commonAncestorContainer)?(i.deleteContents(),i.insertNode(c)):n.appendChild(c),u&&(i=i.cloneRange(),i.setStartAfter(u),i.collapse(!0),o.removeAllRanges(),o.addRange(i))}}else if(t.selection){var o=t.selection;if("Control"!=o.type){var s=o.createRange();s.collapse(!0);var i=o.createRange();if(f(n,i.parentElement()))i.pasteHTML(r);else{var d=t.body.createTextRange();d.moveToElementText(n),d.collapse(!1),d.select(),d.pasteHTML(r)}i=o.createRange(),i.setEndPoint("StartToEnd",s),i.select()}}},E=function(E){E=E||{};var C=E.element||null;"string"==typeof C&&(C=t.getElementById(C));var R=E.onKeyDown||null,S=E.onKeyPress||null,L=E.onKeyUp||null,N=E.onSelection||null,A=E.onPlaceholder||null,x=E.onOpenpopup||null,M=E.onClosepopup||null,w=E.hijackContextmenu||!1,I=E.readOnly||!1,H="TEXTAREA"==C.nodeName||"INPUT"==C.nodeName;if(H){var k="contentEditable"in t.body;if(k){var O=navigator.userAgent.match(/(?:iPad|iPhone|Android).* AppleWebKit\/([^ ]+)/);O&&420<=parseInt(O[1])&&parseInt(O[1])<534&&(k=!1)}if(!k){var P=C,D=function(e){return e.replace(/<br[ \/]*>\n?/gi,"<br>\n")};P.value=D(P.value);var B=function(){return this},j=function(){return null};return{legacy:!0,getElement:function(){return P},getHTML:function(){return P.value},setHTML:function(e){return P.value=D(e),this},getSelectedHTML:j,sync:B,readOnly:function(e){return void 0===e?P.hasAttribute?P.hasAttribute("readonly"):!!P.getAttribute("readonly"):(e?P.setAttribute("readonly","readonly"):P.removeAttribute("readonly"),this)},collapseSelection:B,expandSelection:B,openPopup:j,closePopup:B,removeFormat:B,bold:B,italic:B,underline:B,strikethrough:B,forecolor:B,highlight:B,fontName:B,fontSize:B,subscript:B,superscript:B,align:B,format:B,indent:B,insertLink:B,insertImage:B,insertHTML:B,insertList:B}}}var P=null,K=null;if(H){P=C,P.style.display="none",K=t.createElement("DIV"),K.innerHTML=P.value||"";var X=P.parentNode,W=P.nextSibling;W?X.insertBefore(K,W):X.appendChild(K)}else K=C;I||K.setAttribute("contentEditable","true");var Y,_=t.all&&(!t.documentMode||t.documentMode<=8)?t:e,F=null;if(H){var V=K.innerHTML;F=function(){var e=K.innerHTML;e!=V&&(P.value=e,V=e,i(P,"change",!1))};var q=P.form;q&&r(q,"reset",function(){K.innerHTML="",F(),Y(!0)})}var z;if(A){var U=!1;z=function(){for(var e=!0,t=K;t;)if(t=c(t,K)){if(t.nodeType==l){if("IMG"==t.nodeName){e=!1;break}}else if(t.nodeType==u){var n=t.nodeValue;if(n&&-1!=n.search(/[^\s]/)){e=!1;break}}}else;U!=e&&(A(e),U=e)},z()}var G=null,J=null,Q=null;N&&(J=function(t,n,r){var o=g(K),i=v(K),a=null===t||null===n?null:{left:t,top:n,width:0,height:0},u=p();if(u&&(a=u),a){if(K.getBoundingClientRect){var f=K.getBoundingClientRect();a.left-=parseInt(f.left),a.top-=parseInt(f.top)}else{var c=K,s=0,d=0,h=!1;do s+=c.offsetLeft?parseInt(c.offsetLeft):0,d+=c.offsetTop?parseInt(c.offsetTop):0,"fixed"==c.style.position&&(h=!0);while(c=c.offsetParent);a.left-=s-(h?0:e.pageXOffset),a.top-=d-(h?0:e.pageYOffset)}a.left<0&&(a.left=0),a.top<0&&(a.top=0),a.width>K.offsetWidth&&(a.width=K.offsetWidth),a.height>K.offsetHeight&&(a.height=K.offsetHeight)}else if(i.length)for(var m=0;m<i.length;++m){var c=i[m];if(c.nodeType==l){a={left:c.offsetLeft,top:c.offsetTop,width:c.offsetWidth,height:c.offsetHeight};break}}N(o,a,i,r)},Q=n(J,1));var Z=null,$=function(t){if(!t)var t=e.event;var n=t.target||t.srcElement;n.nodeType==u&&(n=n.parentNode),f(Z,n)||te()},ee=function(){if(Z)return Z;r(_,"mousedown",$,!0),Z=t.createElement("DIV");var e=K.parentNode,n=K.nextSibling;return n?e.insertBefore(Z,n):e.appendChild(Z),x&&x(),Z},te=function(){Z&&(Z.parentNode.removeChild(Z),Z=null,o(_,"mousedown",$,!0),M&&M())};r(K,"focus",function(){P&&i(P,"focus",!1)}),r(K,"blur",function(){F&&F(),P&&i(P,"blur",!1)});var ne=null;if(z||F){var re=F?n(F,250,!0):null,oe=function(e){z&&z(),re&&re()};ne=n(oe,1),r(K,"input",ne),r(K,"DOMNodeInserted",ne),r(K,"DOMNodeRemoved",ne),r(K,"DOMSubtreeModified",ne),r(K,"DOMCharacterDataModified",ne),r(K,"propertychange",ne),r(K,"textInput",ne),r(K,"paste",ne),r(K,"cut",ne),r(K,"drop",ne)}var ie=function(t,n){if(!t)var t=e.event;var r=t.which||t.keyCode,o=String.fromCharCode(r||t.charCode),i=t.shiftKey||!1,l=t.altKey||!1,u=t.ctrlKey||!1,f=t.metaKey||!1;if(1==n){if(R&&R(r,o,i,l,u,f)===!1)return a(t)}else if(2==n){if(S&&S(r,o,i,l,u,f)===!1)return a(t)}else if(3==n&&L&&L(r,o,i,l,u,f)===!1)return a(t);if((2==n||3==n)&&(G=null,Q&&Q(null,null,!1)),2==n&&ne)switch(r){case 33:case 34:case 35:case 36:case 37:case 38:case 39:case 40:break;default:ne()}};r(K,"keydown",function(e){return ie(e,1)}),r(K,"keypress",function(e){return ie(e,2)}),r(K,"keyup",function(e){return ie(e,3)});var ae=function(t,n){if(!t)var t=e.event;var r=null,i=null;t.clientX&&t.clientY?(r=t.clientX,i=t.clientY):t.pageX&&t.pageY&&(r=t.pageX-e.pageXOffset,i=t.pageY-e.pageYOffset),t.which&&3==t.which?n=!0:t.button&&2==t.button&&(n=!0),o(_,"mouseup",ae),G=null,(w||!n)&&Q&&Q(r,i,n)};r(K,"mousedown",function(e){o(_,"mouseup",ae),r(_,"mouseup",ae)}),r(K,"mouseup",function(e){ae(e),ne&&ne()}),r(K,"dblclick",function(e){ae(e)}),r(K,"selectionchange",function(e){ae(e)}),w&&r(K,"contextmenu",function(e){return ae(e,!0),a(e)});var le=function(n,r,o){if(d(K,G),K.focus(),!T(K,o))return!1;if(e.getSelection)try{return t.queryCommandSupported&&!t.queryCommandSupported(n)?!1:t.execCommand(n,!1,r)}catch(i){}else if(t.selection){var a=t.selection;if("None"!=a.type){var l=a.createRange();try{return l.queryCommandEnabled(n)?l.execCommand(n,!1,r):!1}catch(i){}}}return!1},ue=null,fe=function(){(t.all||e.MSInputMethodContext)&&(ue=t.createElement("DIV"),K.appendChild(ue))};return Y=function(e){ue&&(K.removeChild(ue),ue=null),ne&&ne(),e?(h(),G=null):G&&(G=s(K))},{getElement:function(){return K},getHTML:function(){return K.innerHTML},setHTML:function(e){return K.innerHTML=e||"<br>",Y(!0),this},getSelectedHTML:function(){return d(K,G),T(K)?y(K):null},sync:function(){return F&&F(),this},readOnly:function(e){return void 0===e?K.hasAttribute?!K.hasAttribute("contentEditable"):!K.getAttribute("contentEditable"):(e?K.removeAttribute("contentEditable"):K.setAttribute("contentEditable","true"),this)},collapseSelection:function(){return h(),G=null,this},expandSelection:function(e,t){return d(K,G),T(K)?(m(K,e,t),G=s(K),this):this},openPopup:function(){return G||(G=s(K)),ee()},closePopup:function(){return te(),this},removeFormat:function(){return le("removeFormat"),le("unlink"),Y(),this},bold:function(){return le("bold"),Y(),this},italic:function(){return le("italic"),Y(),this},underline:function(){return le("underline"),Y(),this},strikethrough:function(){return le("strikeThrough"),Y(),this},forecolor:function(e){return le("foreColor",e),Y(),this},highlight:function(e){return le("hiliteColor",e)||le("backColor",e),Y(),this},fontName:function(e){return le("fontName",e),Y(),this},fontSize:function(e){return le("fontSize",e),Y(),this},subscript:function(){return le("subscript"),Y(),this},superscript:function(){return le("superscript"),Y(),this},align:function(e){return fe(),"left"==e?le("justifyLeft"):"center"==e?le("justifyCenter"):"right"==e?le("justifyRight"):"justify"==e&&le("justifyFull"),Y(),this},format:function(e){return fe(),le("formatBlock",e),Y(),this},indent:function(e){return fe(),le(e?"outdent":"indent"),Y(),this},insertLink:function(e){return le("createLink",e),Y(!0),this},insertImage:function(e){return le("insertImage",e,!0),Y(!0),this},insertHTML:function(e){return le("insertHTML",e,!0)||(d(K,G),T(K,!0),b(K,e)),Y(!0),this},insertList:function(e){return fe(),le(e?"insertOrderedList":"insertUnorderedList"),Y(),this}}};return E});;!function(e){"use strict";if("function"==typeof define&&define.amd)define(["jquery"],function(t){return e(window,document,t)});else{if("undefined"==typeof exports)return e(window,document,jQuery);module.exports=e(window,document,require("jquery"))}}(function(e,t,n){"use strict";var o=function(e,t,n){var o,r,a,i,s,l,p,c;switch(i=Math.floor(6*e),s=6*e-i,l=n*(1-t),p=n*(1-s*t),c=n*(1-(1-s)*t),i%6){case 0:o=n,r=c,a=l;break;case 1:o=p,r=n,a=l;break;case 2:o=l,r=n,a=c;break;case 3:o=l,r=p,a=n;break;case 4:o=c,r=l,a=n;break;case 5:o=n,r=l,a=p}var u=Math.floor(255*o).toString(16),f=Math.floor(255*r).toString(16),d=Math.floor(255*a).toString(16);return"#"+(u.length<2?"0":"")+u+(f.length<2?"0":"")+f+(d.length<2?"0":"")+d},r=function(e){return e.replace(/[&<>"]/g,function(e){var t={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;"};return t[e]||e})},a=function(a,i,s,l,p,c,u,f,d,w,h,g,y,v,m,b,C,k){var P=function(e,t){t&&(e.getSelectedHTML()?e.insertLink(t):e.insertHTML('<a href="'+r(t)+'">'+r(t)+"</a>")),e.closePopup().collapseSelection()},x=function(e,t){var o=n('<input type="text" value="">').val(t?t.attr("href"):"").addClass("wysiwyg-input").keypress(function(n){(10==n.which||13==n.which)&&(t?(t.prop("href",o.val()),e.closePopup().collapseSelection()):P(e,o.val()))});f&&o.prop("placeholder",f);var r=n();c&&(r=E(c).click(function(n){return t?(t.prop("href",o.val()),e.closePopup().collapseSelection()):P(e,o.val()),n.stopPropagation(),n.preventDefault(),!1}));var a=n("<div/>").addClass("wysiwyg-toolbar-form").prop("unselectable","on");return a.append(o).append(r),a},L=function(t){var o=function(e,o){var a='<img id="wysiwyg-insert-image" src="" alt=""'+(o?' title="'+r(o)+'"':"")+">";t.insertHTML(a).closePopup().collapseSelection();var i=n("#wysiwyg-insert-image").removeAttr("id");w&&i.css({maxWidth:w[0]+"px",maxHeight:w[1]+"px"}).load(function(){i.css({maxWidth:"",maxHeight:""});var e=i.width(),t=i.height();(e>w[0]||t>w[1])&&(e/t>w[0]/w[1]?(t=parseInt(t/e*w[0]),e=w[0]):(e=parseInt(e/t*w[1]),t=w[1]),i.prop("width",e).prop("height",t))}),i.prop("src",e)},a=n("<div/>").addClass("wysiwyg-toolbar-form").prop("unselectable","on"),i=null,s=n('<input type="file">').css({position:"absolute",left:0,top:0,width:"100%",height:"100%",opacity:0,cursor:"pointer"});if(!y&&e.File&&e.FileReader&&e.FileList){var l=function(e){if(("function"!=typeof h||h(e))&&e.type.match(h)){var t=new FileReader;t.onload=function(t){var n=t.target.result;o(n,e.name)},t.readAsDataURL(e)}};i=s.prop("draggable","true").change(function(e){for(var t=e.target.files,n=0;n<t.length;++n)l(t[n])}).on("dragover",function(e){return e.originalEvent.dataTransfer.dropEffect="copy",e.stopPropagation(),e.preventDefault(),!1}).on("drop",function(e){for(var t=e.originalEvent.dataTransfer.files,n=0;n<t.length;++n)l(t[n]);return e.stopPropagation(),e.preventDefault(),!1})}else if(g){var p=s.change(function(e){g.call(this,o)});i=n("<form/>").append(p)}i&&n("<div/>").addClass("wysiwyg-browse").html(u).append(i).appendTo(a);var d=n('<input type="text" value="">').addClass("wysiwyg-input").keypress(function(e){(10==e.which||13==e.which)&&o(d.val())});f&&d.prop("placeholder",f);var v=n();return c&&(v=E(c).click(function(e){return o(d.val()),e.stopPropagation(),e.preventDefault(),!1})),a.append(n("<div/>").append(d).append(v)),a},T=function(e){var t=function(t,o){t=n.trim(t||""),o=n.trim(o||"");var a=!1;t.length&&!o.length?a=t:-1==o.indexOf("<")&&-1==o.indexOf(">")&&o.match(/^(?:https?:\/)?\/?(?:[^:\/\s]+)(?:(?:\/\w+)*\/)(?:[\w\-\.]+[^#?\s]+)(?:.*)?(?:#[\w\-]+)?$/)&&(a=o),a&&v&&(o=v(a)||""),!o.length&&a&&(o='<video src="'+r(a)+'">'),e.insertHTML(o).closePopup().collapseSelection()},o=n("<div/>").addClass("wysiwyg-toolbar-form").prop("unselectable","on"),a=n("<textarea>").addClass("wysiwyg-input wysiwyg-inputtextarea");d&&a.prop("placeholder",d),n("<div/>").addClass("wysiwyg-embedcode").append(a).appendTo(o);var i=n('<input type="text" value="">').addClass("wysiwyg-input").keypress(function(e){(10==e.which||13==e.which)&&t(i.val())});f&&i.prop("placeholder",f);var s=n();return c&&(s=E(c).click(function(e){return t(i.val(),a.val()),e.stopPropagation(),e.preventDefault(),!1})),o.append(n("<div/>").append(i).append(s)),o},S=function(e,t){for(var r=n("<table/>").prop("cellpadding","0").prop("cellspacing","0").prop("unselectable","on"),a=1;15>a;++a){for(var i=n("<tr/>"),s=0;25>s;++s){var l;if(24==s){var p=Math.floor(255/13*(14-a)).toString(16),c=(p.length<2?"0":"")+p;l="#"+c+c+c}else{var u=s/24,f=8>=a?a/8:1,d=a>8?(16-a)/8:1;l=o(u,f,d)}n("<td/>").addClass("wysiwyg-toolbar-color").prop("title",l).prop("unselectable","on").css({backgroundColor:l}).click(function(){var n=this.title;return t?e.forecolor(n).closePopup().collapseSelection():e.highlight(n).closePopup().collapseSelection(),!1}).appendTo(i)}r.append(i)}return r},I=function(e,t){switch(e){case"insertimage":return t?function(e){t(L(K),e)}:null;case"insertvideo":return t?function(e){t(T(K),e)}:null;case"insertlink":return t?function(e){t(x(K),e)}:null;case"bold":return function(){K.bold()};case"italic":return function(){K.italic()};case"underline":return function(){K.underline()};case"strikethrough":return function(){K.strikethrough()};case"forecolor":return t?function(e){t(S(K,!0),e)}:null;case"highlight":return t?function(e){t(S(K,!1),e)}:null;case"alignleft":return function(){K.align("left")};case"aligncenter":return function(){K.align("center")};case"alignright":return function(){K.align("right")};case"alignjustify":return function(){K.align("justify")};case"subscript":return function(){K.subscript()};case"superscript":return function(){K.superscript()};case"indent":return function(){K.indent()};case"outdent":return function(){K.indent(!0)};case"orderedList":return function(){K.insertList(!0)};case"unorderedList":return function(){K.insertList()};case"removeformat":return function(){K.removeFormat().closePopup().collapseSelection()}}return null},E=function(e){var t=n("<a/>").addClass("wysiwyg-toolbar-icon").prop("href","#").prop("unselectable","on").append(e.image);return n.each(e,function(e,n){switch(e){case"class":t.addClass(n);break;case"image":case"html":case"popup":case"click":case"showstatic":case"showselection":break;default:t.attr(e,n)}}),t},M=function(e,t,o,r){n.each(p,function(a,i){if(i&&!(t===!1&&"showstatic"in i&&!i.showstatic||t===!0&&"showselection"in i&&!i.showselection)){var s;s="click"in i?function(e){i.click(n(e))}:"popup"in i?function(e){var t=o(),a=i.popup(t,n(e));r(t,e,a)}:I(a,function(e,t){var n=o();n.append(e),r(n,t),n.find("input[type=text]:first").focus()});var l;s?l=E(i).click(function(e){return s(e.currentTarget),I(a)&&K.getElement().focus(),e.stopPropagation(),e.preventDefault(),!1}):i.html&&(l=n(i.html)),l&&e.append(l)}})},D=function(o,r,a,i){for(var s=r.get(0),l=s.offsetParent,p=0,c=0,u=!1,f=0,d=0,w=!1,h=!1,g=o.width(),y=l;y;){f+=y.offsetLeft,d+=y.offsetTop;var v=n(y),m=v.css("position");"static"!=m?u=!0:u||(p+=y.offsetLeft,c+=y.offsetTop),"fixed"==m&&(w=!0),"visible"!=v.css("overflow")&&(h=!0),y=y.offsetParent}var b=n(l||t.body);b.append(o),a+=p+s.offsetLeft,i+=c+s.offsetTop,(w||h)&&(a+g>b.width()-1&&(a=b.width()-g-1),1>a&&(a=1));var C=n(e).width();f+a+g>C-1&&(a=C-f-g-1);var k=w?0:n(e).scrollLeft();k+1>f+a&&(a=k-f+1),o.css({left:parseInt(a)+"px",top:parseInt(i)+"px"})},j={},H=null,U=function(e,t,o,r){var a=function(e,t,o,r,a,i,s){if(k){var l=H||"";switch(t){case 8:l=l.substring(0,l.length-1);case 13:case 27:case 33:case 34:case 35:case 36:case 37:case 38:case 39:case 40:if(e)return;o=!1;break;default:if(!e)return;l+=o}var c=k(l,t,o,r,a,i,s);if("object"!=typeof c||!c.length)return p.closePopup(),H=null,c;var u=n(p.openPopup());u.hide().addClass("wysiwyg-popup wysiwyg-popuphover").empty().append(c),H=l}},i={element:e.get(0),onKeyDown:function(e,t,n,o,r,i){if(m&&m(e,t,n,o,r,i)===!1)return!1;if(t&&!n&&!o&&r&&!i){var s=t.toLowerCase();if(!j[s])return;return j[s](),!1}return a(!1,e,t,n,o,r,i)},onKeyPress:function(e,t,n,o,r,i){return b&&b(e,t,n,o,r,i)===!1?!1:a(!0,e,t,n,o,r,i)},onKeyUp:function(e,t,n,o,r,a){return C&&C(e,t,n,o,r,a)===!1?!1:void 0},onSelection:function(e,t,r,a){var i=!0,s=null;if(e&&n.each(r,function(e,t){var o=n(t).closest("a");return 0!=o.length?(s=x(p,o),!1):void 0}),p.readOnly()?i=!1:t?s||a||H||(-1==n.inArray("selection",l.split("-"))?i=!1:e?i=!1:1==r.length&&"IMG"==r[0].nodeName&&(i=!1)):i=!1,!i)return void p.closePopup();var c,u=function(){var e=c.outerWidth(),r=o.offset(),a=n(p.getElement()).offset(),i=t.left+parseInt(t.width/2)-parseInt(e/2)+a.left-r.left,s=t.top+t.height+a.top-r.top;D(c,o,i,s)};c=n(p.openPopup()),c.hasClass("wysiwyg-popuphover")&&!c.data("special")==!s||(c=n(p.closePopup().openPopup())),H?c.show():c.hasClass("wysiwyg-popup")||(c.addClass("wysiwyg-popup wysiwyg-popuphover"),s?c.empty().append(s).data("special",!0):M(c,!0,function(){return c.empty()},u)),u()},onOpenpopup:function(){W()},onClosepopup:function(){H=null,q()},hijackContextmenu:"selection"==l,readOnly:!!e.prop("readonly")};if(r){var s=n("<div/>").addClass("wysiwyg-placeholder").html(r).hide();o.prepend(s),i.onPlaceholder=function(e){e?s.show():s.hide()}}var p=wysiwyg(i);return p},A=n("<div/>").addClass("wysiwyg-container");i&&A.addClass(i),a.wrap(A),A=a.parent(".wysiwyg-container");var F=!1;s&&(F=n("<div/>").addClass("wysiwyg-wrapper").click(function(){K.getElement().focus()}),a.wrap(F),F=a.parent(".wysiwyg-wrapper"));var K=U(a,A,s?F:A,s);if(K.legacy){var a=n(K.getElement());a.addClass("wysiwyg-textarea"),a.is(":visible")&&a.width(A.width()-(a.outerWidth()-a.width()))}else n(K.getElement()).addClass("wysiwyg-editor");var O=null,W=function(){O&&clearTimeout(O),O=null,A.addClass("wysiwyg-active"),A.find(".wysiwyg-toolbar-focus").slideDown(200)},q=function(){O||t.activeElement==K.getElement()||(O=setTimeout(function(){O=null,A.removeClass("wysiwyg-active"),0==n.trim(K.getHTML().replace(/<br\s*[\/]?>/gi,"")).length&&A.find(".wysiwyg-toolbar-focus").slideUp(200)},100))};n(K.getElement()).focus(W).blur(q),a.closest("form").on("reset",q);var R={};if(n.each(p,function(e,t){if(t&&t.hotkey){var n=I(e);n&&(j[t.hotkey.toLowerCase()]=n,R[e]=n)}}),!n.isEmptyObject(p)&&"selection"!=l){var $=-1!=n.inArray("top",l.split("-")),z=-1!=n.inArray("focus",l.split("-")),G=n("<div/>").addClass("wysiwyg-toolbar").addClass($?"wysiwyg-toolbar-top":"wysiwyg-toolbar-bottom");z&&G.hide().addClass("wysiwyg-toolbar-focus"),M(G,!1,function(){var e=n(K.openPopup());return e.hasClass("wysiwyg-popup")&&e.hasClass("wysiwyg-popuphover")&&(e=n(K.closePopup().openPopup())),e.hasClass("wysiwyg-popup")||e.addClass("wysiwyg-popup"),e},function(e,t,o){var r=n(t),a=e.outerWidth(),i=r.offset().left-A.offset().left+parseInt(r.width()/2)-parseInt(a/2),s=r.offset().top-A.offset().top;$?s+=r.outerHeight():s-=e.outerHeight(),o&&(i=o.left,s=o.top),D(e,A,i,s)}),$?A.prepend(G):A.append(G)}return{wysiwygeditor:K,$container:A}};n.fn.wysiwyg=function(e,t){if(!e||"object"==typeof e)return e=n.extend({},e),this.each(function(){var t=n(this);if(!t.data("wysiwyg")){var o=e["class"],r=e.placeholder||t.prop("placeholder"),i=e.toolbar||"top",s=e.buttons||{},l=e.submit,p=e.selectImage,c=e.placeholderUrl||null,u=e.placeholderEmbed||null,f=e.maxImageSize||null,d=e.filterImageType||"^image/",w=e.onImageUpload||null,h=e.forceImageUpload&&w,g=e.videoFromUrl||null,y=e.onKeyDown||null,v=e.onKeyPress||null,m=e.onKeyUp||null,b=e.onAutocomplete||null,C=a(t,o,r,i,s,l,p,c,u,f,d,w,h,g,y,v,m,b);t.data("wysiwyg",C)}});if(1==this.length){var o=this.data("wysiwyg");if(!o)return this;if("container"==e)return o.$container;if("shell"==e)return o.wysiwygeditor}return this}});;/*! Hammer.JS - v2.0.6 - 2015-12-23
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2015 Jorik Tangelder;
 * Licensed under the  license */
(function(window, document, exportName, undefined) {
  'use strict';

var VENDOR_PREFIXES = ['', 'webkit', 'Moz', 'MS', 'ms', 'o'];
var TEST_ELEMENT = document.createElement('div');

var TYPE_FUNCTION = 'function';

var round = Math.round;
var abs = Math.abs;
var now = Date.now;

/**
 * set a timeout with a given scope
 * @param {Function} fn
 * @param {Number} timeout
 * @param {Object} context
 * @returns {number}
 */
function setTimeoutContext(fn, timeout, context) {
    return setTimeout(bindFn(fn, context), timeout);
}

/**
 * if the argument is an array, we want to execute the fn on each entry
 * if it aint an array we don't want to do a thing.
 * this is used by all the methods that accept a single and array argument.
 * @param {*|Array} arg
 * @param {String} fn
 * @param {Object} [context]
 * @returns {Boolean}
 */
function invokeArrayArg(arg, fn, context) {
    if (Array.isArray(arg)) {
        each(arg, context[fn], context);
        return true;
    }
    return false;
}

/**
 * walk objects and arrays
 * @param {Object} obj
 * @param {Function} iterator
 * @param {Object} context
 */
function each(obj, iterator, context) {
    var i;

    if (!obj) {
        return;
    }

    if (obj.forEach) {
        obj.forEach(iterator, context);
    } else if (obj.length !== undefined) {
        i = 0;
        while (i < obj.length) {
            iterator.call(context, obj[i], i, obj);
            i++;
        }
    } else {
        for (i in obj) {
            obj.hasOwnProperty(i) && iterator.call(context, obj[i], i, obj);
        }
    }
}

/**
 * wrap a method with a deprecation warning and stack trace
 * @param {Function} method
 * @param {String} name
 * @param {String} message
 * @returns {Function} A new function wrapping the supplied method.
 */
function deprecate(method, name, message) {
    var deprecationMessage = 'DEPRECATED METHOD: ' + name + '\n' + message + ' AT \n';
    return function() {
        var e = new Error('get-stack-trace');
        var stack = e && e.stack ? e.stack.replace(/^[^\(]+?[\n$]/gm, '')
            .replace(/^\s+at\s+/gm, '')
            .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@') : 'Unknown Stack Trace';

        var log = window.console && (window.console.warn || window.console.log);
        if (log) {
            log.call(window.console, deprecationMessage, stack);
        }
        return method.apply(this, arguments);
    };
}

/**
 * extend object.
 * means that properties in dest will be overwritten by the ones in src.
 * @param {Object} target
 * @param {...Object} objects_to_assign
 * @returns {Object} target
 */
var assign;
if (typeof Object.assign !== 'function') {
    assign = function assign(target) {
        if (target === undefined || target === null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }

        var output = Object(target);
        for (var index = 1; index < arguments.length; index++) {
            var source = arguments[index];
            if (source !== undefined && source !== null) {
                for (var nextKey in source) {
                    if (source.hasOwnProperty(nextKey)) {
                        output[nextKey] = source[nextKey];
                    }
                }
            }
        }
        return output;
    };
} else {
    assign = Object.assign;
}

/**
 * extend object.
 * means that properties in dest will be overwritten by the ones in src.
 * @param {Object} dest
 * @param {Object} src
 * @param {Boolean=false} [merge]
 * @returns {Object} dest
 */
var extend = deprecate(function extend(dest, src, merge) {
    var keys = Object.keys(src);
    var i = 0;
    while (i < keys.length) {
        if (!merge || (merge && dest[keys[i]] === undefined)) {
            dest[keys[i]] = src[keys[i]];
        }
        i++;
    }
    return dest;
}, 'extend', 'Use `assign`.');

/**
 * merge the values from src in the dest.
 * means that properties that exist in dest will not be overwritten by src
 * @param {Object} dest
 * @param {Object} src
 * @returns {Object} dest
 */
var merge = deprecate(function merge(dest, src) {
    return extend(dest, src, true);
}, 'merge', 'Use `assign`.');

/**
 * simple class inheritance
 * @param {Function} child
 * @param {Function} base
 * @param {Object} [properties]
 */
function inherit(child, base, properties) {
    var baseP = base.prototype,
        childP;

    childP = child.prototype = Object.create(baseP);
    childP.constructor = child;
    childP._super = baseP;

    if (properties) {
        assign(childP, properties);
    }
}

/**
 * simple function bind
 * @param {Function} fn
 * @param {Object} context
 * @returns {Function}
 */
function bindFn(fn, context) {
    return function boundFn() {
        return fn.apply(context, arguments);
    };
}

/**
 * let a boolean value also be a function that must return a boolean
 * this first item in args will be used as the context
 * @param {Boolean|Function} val
 * @param {Array} [args]
 * @returns {Boolean}
 */
function boolOrFn(val, args) {
    if (typeof val == TYPE_FUNCTION) {
        return val.apply(args ? args[0] || undefined : undefined, args);
    }
    return val;
}

/**
 * use the val2 when val1 is undefined
 * @param {*} val1
 * @param {*} val2
 * @returns {*}
 */
function ifUndefined(val1, val2) {
    return (val1 === undefined) ? val2 : val1;
}

/**
 * addEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function addEventListeners(target, types, handler) {
    each(splitStr(types), function(type) {
        target.addEventListener(type, handler, false);
    });
}

/**
 * removeEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function removeEventListeners(target, types, handler) {
    each(splitStr(types), function(type) {
        target.removeEventListener(type, handler, false);
    });
}

/**
 * find if a node is in the given parent
 * @method hasParent
 * @param {HTMLElement} node
 * @param {HTMLElement} parent
 * @return {Boolean} found
 */
function hasParent(node, parent) {
    while (node) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}

/**
 * small indexOf wrapper
 * @param {String} str
 * @param {String} find
 * @returns {Boolean} found
 */
function inStr(str, find) {
    return str.indexOf(find) > -1;
}

/**
 * split string on whitespace
 * @param {String} str
 * @returns {Array} words
 */
function splitStr(str) {
    return str.trim().split(/\s+/g);
}

/**
 * find if a array contains the object using indexOf or a simple polyFill
 * @param {Array} src
 * @param {String} find
 * @param {String} [findByKey]
 * @return {Boolean|Number} false when not found, or the index
 */
function inArray(src, find, findByKey) {
    if (src.indexOf && !findByKey) {
        return src.indexOf(find);
    } else {
        var i = 0;
        while (i < src.length) {
            if ((findByKey && src[i][findByKey] == find) || (!findByKey && src[i] === find)) {
                return i;
            }
            i++;
        }
        return -1;
    }
}

/**
 * convert array-like objects to real arrays
 * @param {Object} obj
 * @returns {Array}
 */
function toArray(obj) {
    return Array.prototype.slice.call(obj, 0);
}

/**
 * unique array with objects based on a key (like 'id') or just by the array's value
 * @param {Array} src [{id:1},{id:2},{id:1}]
 * @param {String} [key]
 * @param {Boolean} [sort=False]
 * @returns {Array} [{id:1},{id:2}]
 */
function uniqueArray(src, key, sort) {
    var results = [];
    var values = [];
    var i = 0;

    while (i < src.length) {
        var val = key ? src[i][key] : src[i];
        if (inArray(values, val) < 0) {
            results.push(src[i]);
        }
        values[i] = val;
        i++;
    }

    if (sort) {
        if (!key) {
            results = results.sort();
        } else {
            results = results.sort(function sortUniqueArray(a, b) {
                return a[key] > b[key];
            });
        }
    }

    return results;
}

/**
 * get the prefixed property
 * @param {Object} obj
 * @param {String} property
 * @returns {String|Undefined} prefixed
 */
function prefixed(obj, property) {
    var prefix, prop;
    var camelProp = property[0].toUpperCase() + property.slice(1);

    var i = 0;
    while (i < VENDOR_PREFIXES.length) {
        prefix = VENDOR_PREFIXES[i];
        prop = (prefix) ? prefix + camelProp : property;

        if (prop in obj) {
            return prop;
        }
        i++;
    }
    return undefined;
}

/**
 * get a unique id
 * @returns {number} uniqueId
 */
var _uniqueId = 1;
function uniqueId() {
    return _uniqueId++;
}

/**
 * get the window object of an element
 * @param {HTMLElement} element
 * @returns {DocumentView|Window}
 */
function getWindowForElement(element) {
    var doc = element.ownerDocument || element;
    return (doc.defaultView || doc.parentWindow || window);
}

var MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i;

var SUPPORT_TOUCH = ('ontouchstart' in window);
var SUPPORT_POINTER_EVENTS = prefixed(window, 'PointerEvent') !== undefined;
var SUPPORT_ONLY_TOUCH = SUPPORT_TOUCH && MOBILE_REGEX.test(navigator.userAgent);

var INPUT_TYPE_TOUCH = 'touch';
var INPUT_TYPE_PEN = 'pen';
var INPUT_TYPE_MOUSE = 'mouse';
var INPUT_TYPE_KINECT = 'kinect';

var COMPUTE_INTERVAL = 25;

var INPUT_START = 1;
var INPUT_MOVE = 2;
var INPUT_END = 4;
var INPUT_CANCEL = 8;

var DIRECTION_NONE = 1;
var DIRECTION_LEFT = 2;
var DIRECTION_RIGHT = 4;
var DIRECTION_UP = 8;
var DIRECTION_DOWN = 16;

var DIRECTION_HORIZONTAL = DIRECTION_LEFT | DIRECTION_RIGHT;
var DIRECTION_VERTICAL = DIRECTION_UP | DIRECTION_DOWN;
var DIRECTION_ALL = DIRECTION_HORIZONTAL | DIRECTION_VERTICAL;

var PROPS_XY = ['x', 'y'];
var PROPS_CLIENT_XY = ['clientX', 'clientY'];

/**
 * create new input type manager
 * @param {Manager} manager
 * @param {Function} callback
 * @returns {Input}
 * @constructor
 */
function Input(manager, callback) {
    var self = this;
    this.manager = manager;
    this.callback = callback;
    this.element = manager.element;
    this.target = manager.options.inputTarget;

    // smaller wrapper around the handler, for the scope and the enabled state of the manager,
    // so when disabled the input events are completely bypassed.
    this.domHandler = function(ev) {
        if (boolOrFn(manager.options.enable, [manager])) {
            self.handler(ev);
        }
    };

    this.init();

}

Input.prototype = {
    /**
     * should handle the inputEvent data and trigger the callback
     * @virtual
     */
    handler: function() { },

    /**
     * bind the events
     */
    init: function() {
        this.evEl && addEventListeners(this.element, this.evEl, this.domHandler);
        this.evTarget && addEventListeners(this.target, this.evTarget, this.domHandler);
        this.evWin && addEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
    },

    /**
     * unbind the events
     */
    destroy: function() {
        this.evEl && removeEventListeners(this.element, this.evEl, this.domHandler);
        this.evTarget && removeEventListeners(this.target, this.evTarget, this.domHandler);
        this.evWin && removeEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
    }
};

/**
 * create new input type manager
 * called by the Manager constructor
 * @param {Hammer} manager
 * @returns {Input}
 */
function createInputInstance(manager) {
    var Type;
    var inputClass = manager.options.inputClass;

    if (inputClass) {
        Type = inputClass;
    } else if (SUPPORT_POINTER_EVENTS) {
        Type = PointerEventInput;
    } else if (SUPPORT_ONLY_TOUCH) {
        Type = TouchInput;
    } else if (!SUPPORT_TOUCH) {
        Type = MouseInput;
    } else {
        Type = TouchMouseInput;
    }
    return new (Type)(manager, inputHandler);
}

/**
 * handle input events
 * @param {Manager} manager
 * @param {String} eventType
 * @param {Object} input
 */
function inputHandler(manager, eventType, input) {
    var pointersLen = input.pointers.length;
    var changedPointersLen = input.changedPointers.length;
    var isFirst = (eventType & INPUT_START && (pointersLen - changedPointersLen === 0));
    var isFinal = (eventType & (INPUT_END | INPUT_CANCEL) && (pointersLen - changedPointersLen === 0));

    input.isFirst = !!isFirst;
    input.isFinal = !!isFinal;

    if (isFirst) {
        manager.session = {};
    }

    // source event is the normalized value of the domEvents
    // like 'touchstart, mouseup, pointerdown'
    input.eventType = eventType;

    // compute scale, rotation etc
    computeInputData(manager, input);

    // emit secret event
    manager.emit('hammer.input', input);

    manager.recognize(input);
    manager.session.prevInput = input;
}

/**
 * extend the data with some usable properties like scale, rotate, velocity etc
 * @param {Object} manager
 * @param {Object} input
 */
function computeInputData(manager, input) {
    var session = manager.session;
    var pointers = input.pointers;
    var pointersLength = pointers.length;

    // store the first input to calculate the distance and direction
    if (!session.firstInput) {
        session.firstInput = simpleCloneInputData(input);
    }

    // to compute scale and rotation we need to store the multiple touches
    if (pointersLength > 1 && !session.firstMultiple) {
        session.firstMultiple = simpleCloneInputData(input);
    } else if (pointersLength === 1) {
        session.firstMultiple = false;
    }

    var firstInput = session.firstInput;
    var firstMultiple = session.firstMultiple;
    var offsetCenter = firstMultiple ? firstMultiple.center : firstInput.center;

    var center = input.center = getCenter(pointers);
    input.timeStamp = now();
    input.deltaTime = input.timeStamp - firstInput.timeStamp;

    input.angle = getAngle(offsetCenter, center);
    input.distance = getDistance(offsetCenter, center);

    computeDeltaXY(session, input);
    input.offsetDirection = getDirection(input.deltaX, input.deltaY);

    var overallVelocity = getVelocity(input.deltaTime, input.deltaX, input.deltaY);
    input.overallVelocityX = overallVelocity.x;
    input.overallVelocityY = overallVelocity.y;
    input.overallVelocity = (abs(overallVelocity.x) > abs(overallVelocity.y)) ? overallVelocity.x : overallVelocity.y;

    input.scale = firstMultiple ? getScale(firstMultiple.pointers, pointers) : 1;
    input.rotation = firstMultiple ? getRotation(firstMultiple.pointers, pointers) : 0;

    input.maxPointers = !session.prevInput ? input.pointers.length : ((input.pointers.length >
        session.prevInput.maxPointers) ? input.pointers.length : session.prevInput.maxPointers);

    computeIntervalInputData(session, input);

    // find the correct target
    var target = manager.element;
    if (hasParent(input.srcEvent.target, target)) {
        target = input.srcEvent.target;
    }
    input.target = target;
}

function computeDeltaXY(session, input) {
    var center = input.center;
    var offset = session.offsetDelta || {};
    var prevDelta = session.prevDelta || {};
    var prevInput = session.prevInput || {};

    if (input.eventType === INPUT_START || prevInput.eventType === INPUT_END) {
        prevDelta = session.prevDelta = {
            x: prevInput.deltaX || 0,
            y: prevInput.deltaY || 0
        };

        offset = session.offsetDelta = {
            x: center.x,
            y: center.y
        };
    }

    input.deltaX = prevDelta.x + (center.x - offset.x);
    input.deltaY = prevDelta.y + (center.y - offset.y);
}

/**
 * velocity is calculated every x ms
 * @param {Object} session
 * @param {Object} input
 */
function computeIntervalInputData(session, input) {
    var last = session.lastInterval || input,
        deltaTime = input.timeStamp - last.timeStamp,
        velocity, velocityX, velocityY, direction;

    if (input.eventType != INPUT_CANCEL && (deltaTime > COMPUTE_INTERVAL || last.velocity === undefined)) {
        var deltaX = input.deltaX - last.deltaX;
        var deltaY = input.deltaY - last.deltaY;

        var v = getVelocity(deltaTime, deltaX, deltaY);
        velocityX = v.x;
        velocityY = v.y;
        velocity = (abs(v.x) > abs(v.y)) ? v.x : v.y;
        direction = getDirection(deltaX, deltaY);

        session.lastInterval = input;
    } else {
        // use latest velocity info if it doesn't overtake a minimum period
        velocity = last.velocity;
        velocityX = last.velocityX;
        velocityY = last.velocityY;
        direction = last.direction;
    }

    input.velocity = velocity;
    input.velocityX = velocityX;
    input.velocityY = velocityY;
    input.direction = direction;
}

/**
 * create a simple clone from the input used for storage of firstInput and firstMultiple
 * @param {Object} input
 * @returns {Object} clonedInputData
 */
function simpleCloneInputData(input) {
    // make a simple copy of the pointers because we will get a reference if we don't
    // we only need clientXY for the calculations
    var pointers = [];
    var i = 0;
    while (i < input.pointers.length) {
        pointers[i] = {
            clientX: round(input.pointers[i].clientX),
            clientY: round(input.pointers[i].clientY)
        };
        i++;
    }

    return {
        timeStamp: now(),
        pointers: pointers,
        center: getCenter(pointers),
        deltaX: input.deltaX,
        deltaY: input.deltaY
    };
}

/**
 * get the center of all the pointers
 * @param {Array} pointers
 * @return {Object} center contains `x` and `y` properties
 */
function getCenter(pointers) {
    var pointersLength = pointers.length;

    // no need to loop when only one touch
    if (pointersLength === 1) {
        return {
            x: round(pointers[0].clientX),
            y: round(pointers[0].clientY)
        };
    }

    var x = 0, y = 0, i = 0;
    while (i < pointersLength) {
        x += pointers[i].clientX;
        y += pointers[i].clientY;
        i++;
    }

    return {
        x: round(x / pointersLength),
        y: round(y / pointersLength)
    };
}

/**
 * calculate the velocity between two points. unit is in px per ms.
 * @param {Number} deltaTime
 * @param {Number} x
 * @param {Number} y
 * @return {Object} velocity `x` and `y`
 */
function getVelocity(deltaTime, x, y) {
    return {
        x: x / deltaTime || 0,
        y: y / deltaTime || 0
    };
}

/**
 * get the direction between two points
 * @param {Number} x
 * @param {Number} y
 * @return {Number} direction
 */
function getDirection(x, y) {
    if (x === y) {
        return DIRECTION_NONE;
    }

    if (abs(x) >= abs(y)) {
        return x < 0 ? DIRECTION_LEFT : DIRECTION_RIGHT;
    }
    return y < 0 ? DIRECTION_UP : DIRECTION_DOWN;
}

/**
 * calculate the absolute distance between two points
 * @param {Object} p1 {x, y}
 * @param {Object} p2 {x, y}
 * @param {Array} [props] containing x and y keys
 * @return {Number} distance
 */
function getDistance(p1, p2, props) {
    if (!props) {
        props = PROPS_XY;
    }
    var x = p2[props[0]] - p1[props[0]],
        y = p2[props[1]] - p1[props[1]];

    return Math.sqrt((x * x) + (y * y));
}

/**
 * calculate the angle between two coordinates
 * @param {Object} p1
 * @param {Object} p2
 * @param {Array} [props] containing x and y keys
 * @return {Number} angle
 */
function getAngle(p1, p2, props) {
    if (!props) {
        props = PROPS_XY;
    }
    var x = p2[props[0]] - p1[props[0]],
        y = p2[props[1]] - p1[props[1]];
    return Math.atan2(y, x) * 180 / Math.PI;
}

/**
 * calculate the rotation degrees between two pointersets
 * @param {Array} start array of pointers
 * @param {Array} end array of pointers
 * @return {Number} rotation
 */
function getRotation(start, end) {
    return getAngle(end[1], end[0], PROPS_CLIENT_XY) + getAngle(start[1], start[0], PROPS_CLIENT_XY);
}

/**
 * calculate the scale factor between two pointersets
 * no scale is 1, and goes down to 0 when pinched together, and bigger when pinched out
 * @param {Array} start array of pointers
 * @param {Array} end array of pointers
 * @return {Number} scale
 */
function getScale(start, end) {
    return getDistance(end[0], end[1], PROPS_CLIENT_XY) / getDistance(start[0], start[1], PROPS_CLIENT_XY);
}

var MOUSE_INPUT_MAP = {
    mousedown: INPUT_START,
    mousemove: INPUT_MOVE,
    mouseup: INPUT_END
};

var MOUSE_ELEMENT_EVENTS = 'mousedown';
var MOUSE_WINDOW_EVENTS = 'mousemove mouseup';

/**
 * Mouse events input
 * @constructor
 * @extends Input
 */
function MouseInput() {
    this.evEl = MOUSE_ELEMENT_EVENTS;
    this.evWin = MOUSE_WINDOW_EVENTS;

    this.allow = true; // used by Input.TouchMouse to disable mouse events
    this.pressed = false; // mousedown state

    Input.apply(this, arguments);
}

inherit(MouseInput, Input, {
    /**
     * handle mouse events
     * @param {Object} ev
     */
    handler: function MEhandler(ev) {
        var eventType = MOUSE_INPUT_MAP[ev.type];

        // on start we want to have the left mouse button down
        if (eventType & INPUT_START && ev.button === 0) {
            this.pressed = true;
        }

        if (eventType & INPUT_MOVE && ev.which !== 1) {
            eventType = INPUT_END;
        }

        // mouse must be down, and mouse events are allowed (see the TouchMouse input)
        if (!this.pressed || !this.allow) {
            return;
        }

        if (eventType & INPUT_END) {
            this.pressed = false;
        }

        this.callback(this.manager, eventType, {
            pointers: [ev],
            changedPointers: [ev],
            pointerType: INPUT_TYPE_MOUSE,
            srcEvent: ev
        });
    }
});

var POINTER_INPUT_MAP = {
    pointerdown: INPUT_START,
    pointermove: INPUT_MOVE,
    pointerup: INPUT_END,
    pointercancel: INPUT_CANCEL,
    pointerout: INPUT_CANCEL
};

// in IE10 the pointer types is defined as an enum
var IE10_POINTER_TYPE_ENUM = {
    2: INPUT_TYPE_TOUCH,
    3: INPUT_TYPE_PEN,
    4: INPUT_TYPE_MOUSE,
    5: INPUT_TYPE_KINECT // see https://twitter.com/jacobrossi/status/480596438489890816
};

var POINTER_ELEMENT_EVENTS = 'pointerdown';
var POINTER_WINDOW_EVENTS = 'pointermove pointerup pointercancel';

// IE10 has prefixed support, and case-sensitive
if (window.MSPointerEvent && !window.PointerEvent) {
    POINTER_ELEMENT_EVENTS = 'MSPointerDown';
    POINTER_WINDOW_EVENTS = 'MSPointerMove MSPointerUp MSPointerCancel';
}

/**
 * Pointer events input
 * @constructor
 * @extends Input
 */
function PointerEventInput() {
    this.evEl = POINTER_ELEMENT_EVENTS;
    this.evWin = POINTER_WINDOW_EVENTS;

    Input.apply(this, arguments);

    this.store = (this.manager.session.pointerEvents = []);
}

inherit(PointerEventInput, Input, {
    /**
     * handle mouse events
     * @param {Object} ev
     */
    handler: function PEhandler(ev) {
        var store = this.store;
        var removePointer = false;

        var eventTypeNormalized = ev.type.toLowerCase().replace('ms', '');
        var eventType = POINTER_INPUT_MAP[eventTypeNormalized];
        var pointerType = IE10_POINTER_TYPE_ENUM[ev.pointerType] || ev.pointerType;

        var isTouch = (pointerType == INPUT_TYPE_TOUCH);

        // get index of the event in the store
        var storeIndex = inArray(store, ev.pointerId, 'pointerId');

        // start and mouse must be down
        if (eventType & INPUT_START && (ev.button === 0 || isTouch)) {
            if (storeIndex < 0) {
                store.push(ev);
                storeIndex = store.length - 1;
            }
        } else if (eventType & (INPUT_END | INPUT_CANCEL)) {
            removePointer = true;
        }

        // it not found, so the pointer hasn't been down (so it's probably a hover)
        if (storeIndex < 0) {
            return;
        }

        // update the event in the store
        store[storeIndex] = ev;

        this.callback(this.manager, eventType, {
            pointers: store,
            changedPointers: [ev],
            pointerType: pointerType,
            srcEvent: ev
        });

        if (removePointer) {
            // remove from the store
            store.splice(storeIndex, 1);
        }
    }
});

var SINGLE_TOUCH_INPUT_MAP = {
    touchstart: INPUT_START,
    touchmove: INPUT_MOVE,
    touchend: INPUT_END,
    touchcancel: INPUT_CANCEL
};

var SINGLE_TOUCH_TARGET_EVENTS = 'touchstart';
var SINGLE_TOUCH_WINDOW_EVENTS = 'touchstart touchmove touchend touchcancel';

/**
 * Touch events input
 * @constructor
 * @extends Input
 */
function SingleTouchInput() {
    this.evTarget = SINGLE_TOUCH_TARGET_EVENTS;
    this.evWin = SINGLE_TOUCH_WINDOW_EVENTS;
    this.started = false;

    Input.apply(this, arguments);
}

inherit(SingleTouchInput, Input, {
    handler: function TEhandler(ev) {
        var type = SINGLE_TOUCH_INPUT_MAP[ev.type];

        // should we handle the touch events?
        if (type === INPUT_START) {
            this.started = true;
        }

        if (!this.started) {
            return;
        }

        var touches = normalizeSingleTouches.call(this, ev, type);

        // when done, reset the started state
        if (type & (INPUT_END | INPUT_CANCEL) && touches[0].length - touches[1].length === 0) {
            this.started = false;
        }

        this.callback(this.manager, type, {
            pointers: touches[0],
            changedPointers: touches[1],
            pointerType: INPUT_TYPE_TOUCH,
            srcEvent: ev
        });
    }
});

/**
 * @this {TouchInput}
 * @param {Object} ev
 * @param {Number} type flag
 * @returns {undefined|Array} [all, changed]
 */
function normalizeSingleTouches(ev, type) {
    var all = toArray(ev.touches);
    var changed = toArray(ev.changedTouches);

    if (type & (INPUT_END | INPUT_CANCEL)) {
        all = uniqueArray(all.concat(changed), 'identifier', true);
    }

    return [all, changed];
}

var TOUCH_INPUT_MAP = {
    touchstart: INPUT_START,
    touchmove: INPUT_MOVE,
    touchend: INPUT_END,
    touchcancel: INPUT_CANCEL
};

var TOUCH_TARGET_EVENTS = 'touchstart touchmove touchend touchcancel';

/**
 * Multi-user touch events input
 * @constructor
 * @extends Input
 */
function TouchInput() {
    this.evTarget = TOUCH_TARGET_EVENTS;
    this.targetIds = {};

    Input.apply(this, arguments);
}

inherit(TouchInput, Input, {
    handler: function MTEhandler(ev) {
        var type = TOUCH_INPUT_MAP[ev.type];
        var touches = getTouches.call(this, ev, type);
        if (!touches) {
            return;
        }

        this.callback(this.manager, type, {
            pointers: touches[0],
            changedPointers: touches[1],
            pointerType: INPUT_TYPE_TOUCH,
            srcEvent: ev
        });
    }
});

/**
 * @this {TouchInput}
 * @param {Object} ev
 * @param {Number} type flag
 * @returns {undefined|Array} [all, changed]
 */
function getTouches(ev, type) {
    var allTouches = toArray(ev.touches);
    var targetIds = this.targetIds;

    // when there is only one touch, the process can be simplified
    if (type & (INPUT_START | INPUT_MOVE) && allTouches.length === 1) {
        targetIds[allTouches[0].identifier] = true;
        return [allTouches, allTouches];
    }

    var i,
        targetTouches,
        changedTouches = toArray(ev.changedTouches),
        changedTargetTouches = [],
        target = this.target;

    // get target touches from touches
    targetTouches = allTouches.filter(function(touch) {
        return hasParent(touch.target, target);
    });

    // collect touches
    if (type === INPUT_START) {
        i = 0;
        while (i < targetTouches.length) {
            targetIds[targetTouches[i].identifier] = true;
            i++;
        }
    }

    // filter changed touches to only contain touches that exist in the collected target ids
    i = 0;
    while (i < changedTouches.length) {
        if (targetIds[changedTouches[i].identifier]) {
            changedTargetTouches.push(changedTouches[i]);
        }

        // cleanup removed touches
        if (type & (INPUT_END | INPUT_CANCEL)) {
            delete targetIds[changedTouches[i].identifier];
        }
        i++;
    }

    if (!changedTargetTouches.length) {
        return;
    }

    return [
        // merge targetTouches with changedTargetTouches so it contains ALL touches, including 'end' and 'cancel'
        uniqueArray(targetTouches.concat(changedTargetTouches), 'identifier', true),
        changedTargetTouches
    ];
}

/**
 * Combined touch and mouse input
 *
 * Touch has a higher priority then mouse, and while touching no mouse events are allowed.
 * This because touch devices also emit mouse events while doing a touch.
 *
 * @constructor
 * @extends Input
 */
function TouchMouseInput() {
    Input.apply(this, arguments);

    var handler = bindFn(this.handler, this);
    this.touch = new TouchInput(this.manager, handler);
    this.mouse = new MouseInput(this.manager, handler);
}

inherit(TouchMouseInput, Input, {
    /**
     * handle mouse and touch events
     * @param {Hammer} manager
     * @param {String} inputEvent
     * @param {Object} inputData
     */
    handler: function TMEhandler(manager, inputEvent, inputData) {
        var isTouch = (inputData.pointerType == INPUT_TYPE_TOUCH),
            isMouse = (inputData.pointerType == INPUT_TYPE_MOUSE);

        // when we're in a touch event, so  block all upcoming mouse events
        // most mobile browser also emit mouseevents, right after touchstart
        if (isTouch) {
            this.mouse.allow = false;
        } else if (isMouse && !this.mouse.allow) {
            return;
        }

        // reset the allowMouse when we're done
        if (inputEvent & (INPUT_END | INPUT_CANCEL)) {
            this.mouse.allow = true;
        }

        this.callback(manager, inputEvent, inputData);
    },

    /**
     * remove the event listeners
     */
    destroy: function destroy() {
        this.touch.destroy();
        this.mouse.destroy();
    }
});

var PREFIXED_TOUCH_ACTION = prefixed(TEST_ELEMENT.style, 'touchAction');
var NATIVE_TOUCH_ACTION = PREFIXED_TOUCH_ACTION !== undefined;

// magical touchAction value
var TOUCH_ACTION_COMPUTE = 'compute';
var TOUCH_ACTION_AUTO = 'auto';
var TOUCH_ACTION_MANIPULATION = 'manipulation'; // not implemented
var TOUCH_ACTION_NONE = 'none';
var TOUCH_ACTION_PAN_X = 'pan-x';
var TOUCH_ACTION_PAN_Y = 'pan-y';

/**
 * Touch Action
 * sets the touchAction property or uses the js alternative
 * @param {Manager} manager
 * @param {String} value
 * @constructor
 */
function TouchAction(manager, value) {
    this.manager = manager;
    this.set(value);
}

TouchAction.prototype = {
    /**
     * set the touchAction value on the element or enable the polyfill
     * @param {String} value
     */
    set: function(value) {
        // find out the touch-action by the event handlers
        if (value == TOUCH_ACTION_COMPUTE) {
            value = this.compute();
        }

        if (NATIVE_TOUCH_ACTION && this.manager.element.style) {
            this.manager.element.style[PREFIXED_TOUCH_ACTION] = value;
        }
        this.actions = value.toLowerCase().trim();
    },

    /**
     * just re-set the touchAction value
     */
    update: function() {
        this.set(this.manager.options.touchAction);
    },

    /**
     * compute the value for the touchAction property based on the recognizer's settings
     * @returns {String} value
     */
    compute: function() {
        var actions = [];
        each(this.manager.recognizers, function(recognizer) {
            if (boolOrFn(recognizer.options.enable, [recognizer])) {
                actions = actions.concat(recognizer.getTouchAction());
            }
        });
        return cleanTouchActions(actions.join(' '));
    },

    /**
     * this method is called on each input cycle and provides the preventing of the browser behavior
     * @param {Object} input
     */
    preventDefaults: function(input) {
        // not needed with native support for the touchAction property
        if (NATIVE_TOUCH_ACTION) {
            return;
        }

        var srcEvent = input.srcEvent;
        var direction = input.offsetDirection;

        // if the touch action did prevented once this session
        if (this.manager.session.prevented) {
            srcEvent.preventDefault();
            return;
        }

        var actions = this.actions;
        var hasNone = inStr(actions, TOUCH_ACTION_NONE);
        var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y);
        var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X);

        if (hasNone) {
            //do not prevent defaults if this is a tap gesture

            var isTapPointer = input.pointers.length === 1;
            var isTapMovement = input.distance < 2;
            var isTapTouchTime = input.deltaTime < 250;

            if (isTapPointer && isTapMovement && isTapTouchTime) {
                return;
            }
        }

        if (hasPanX && hasPanY) {
            // `pan-x pan-y` means browser handles all scrolling/panning, do not prevent
            return;
        }

        if (hasNone ||
            (hasPanY && direction & DIRECTION_HORIZONTAL) ||
            (hasPanX && direction & DIRECTION_VERTICAL)) {
            return this.preventSrc(srcEvent);
        }
    },

    /**
     * call preventDefault to prevent the browser's default behavior (scrolling in most cases)
     * @param {Object} srcEvent
     */
    preventSrc: function(srcEvent) {
        this.manager.session.prevented = true;
        srcEvent.preventDefault();
    }
};

/**
 * when the touchActions are collected they are not a valid value, so we need to clean things up. *
 * @param {String} actions
 * @returns {*}
 */
function cleanTouchActions(actions) {
    // none
    if (inStr(actions, TOUCH_ACTION_NONE)) {
        return TOUCH_ACTION_NONE;
    }

    var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X);
    var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y);

    // if both pan-x and pan-y are set (different recognizers
    // for different directions, e.g. horizontal pan but vertical swipe?)
    // we need none (as otherwise with pan-x pan-y combined none of these
    // recognizers will work, since the browser would handle all panning
    if (hasPanX && hasPanY) {
        return TOUCH_ACTION_NONE;
    }

    // pan-x OR pan-y
    if (hasPanX || hasPanY) {
        return hasPanX ? TOUCH_ACTION_PAN_X : TOUCH_ACTION_PAN_Y;
    }

    // manipulation
    if (inStr(actions, TOUCH_ACTION_MANIPULATION)) {
        return TOUCH_ACTION_MANIPULATION;
    }

    return TOUCH_ACTION_AUTO;
}

/**
 * Recognizer flow explained; *
 * All recognizers have the initial state of POSSIBLE when a input session starts.
 * The definition of a input session is from the first input until the last input, with all it's movement in it. *
 * Example session for mouse-input: mousedown -> mousemove -> mouseup
 *
 * On each recognizing cycle (see Manager.recognize) the .recognize() method is executed
 * which determines with state it should be.
 *
 * If the recognizer has the state FAILED, CANCELLED or RECOGNIZED (equals ENDED), it is reset to
 * POSSIBLE to give it another change on the next cycle.
 *
 *               Possible
 *                  |
 *            +-----+---------------+
 *            |                     |
 *      +-----+-----+               |
 *      |           |               |
 *   Failed      Cancelled          |
 *                          +-------+------+
 *                          |              |
 *                      Recognized       Began
 *                                         |
 *                                      Changed
 *                                         |
 *                                  Ended/Recognized
 */
var STATE_POSSIBLE = 1;
var STATE_BEGAN = 2;
var STATE_CHANGED = 4;
var STATE_ENDED = 8;
var STATE_RECOGNIZED = STATE_ENDED;
var STATE_CANCELLED = 16;
var STATE_FAILED = 32;

/**
 * Recognizer
 * Every recognizer needs to extend from this class.
 * @constructor
 * @param {Object} options
 */
function Recognizer(options) {
    this.options = assign({}, this.defaults, options || {});

    this.id = uniqueId();

    this.manager = null;

    // default is enable true
    this.options.enable = ifUndefined(this.options.enable, true);

    this.state = STATE_POSSIBLE;

    this.simultaneous = {};
    this.requireFail = [];
}

Recognizer.prototype = {
    /**
     * @virtual
     * @type {Object}
     */
    defaults: {},

    /**
     * set options
     * @param {Object} options
     * @return {Recognizer}
     */
    set: function(options) {
        assign(this.options, options);

        // also update the touchAction, in case something changed about the directions/enabled state
        this.manager && this.manager.touchAction.update();
        return this;
    },

    /**
     * recognize simultaneous with an other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    recognizeWith: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'recognizeWith', this)) {
            return this;
        }

        var simultaneous = this.simultaneous;
        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        if (!simultaneous[otherRecognizer.id]) {
            simultaneous[otherRecognizer.id] = otherRecognizer;
            otherRecognizer.recognizeWith(this);
        }
        return this;
    },

    /**
     * drop the simultaneous link. it doesnt remove the link on the other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    dropRecognizeWith: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'dropRecognizeWith', this)) {
            return this;
        }

        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        delete this.simultaneous[otherRecognizer.id];
        return this;
    },

    /**
     * recognizer can only run when an other is failing
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    requireFailure: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'requireFailure', this)) {
            return this;
        }

        var requireFail = this.requireFail;
        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        if (inArray(requireFail, otherRecognizer) === -1) {
            requireFail.push(otherRecognizer);
            otherRecognizer.requireFailure(this);
        }
        return this;
    },

    /**
     * drop the requireFailure link. it does not remove the link on the other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    dropRequireFailure: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'dropRequireFailure', this)) {
            return this;
        }

        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        var index = inArray(this.requireFail, otherRecognizer);
        if (index > -1) {
            this.requireFail.splice(index, 1);
        }
        return this;
    },

    /**
     * has require failures boolean
     * @returns {boolean}
     */
    hasRequireFailures: function() {
        return this.requireFail.length > 0;
    },

    /**
     * if the recognizer can recognize simultaneous with an other recognizer
     * @param {Recognizer} otherRecognizer
     * @returns {Boolean}
     */
    canRecognizeWith: function(otherRecognizer) {
        return !!this.simultaneous[otherRecognizer.id];
    },

    /**
     * You should use `tryEmit` instead of `emit` directly to check
     * that all the needed recognizers has failed before emitting.
     * @param {Object} input
     */
    emit: function(input) {
        var self = this;
        var state = this.state;

        function emit(event) {
            self.manager.emit(event, input);
        }

        // 'panstart' and 'panmove'
        if (state < STATE_ENDED) {
            emit(self.options.event + stateStr(state));
        }

        emit(self.options.event); // simple 'eventName' events

        if (input.additionalEvent) { // additional event(panleft, panright, pinchin, pinchout...)
            emit(input.additionalEvent);
        }

        // panend and pancancel
        if (state >= STATE_ENDED) {
            emit(self.options.event + stateStr(state));
        }
    },

    /**
     * Check that all the require failure recognizers has failed,
     * if true, it emits a gesture event,
     * otherwise, setup the state to FAILED.
     * @param {Object} input
     */
    tryEmit: function(input) {
        if (this.canEmit()) {
            return this.emit(input);
        }
        // it's failing anyway
        this.state = STATE_FAILED;
    },

    /**
     * can we emit?
     * @returns {boolean}
     */
    canEmit: function() {
        var i = 0;
        while (i < this.requireFail.length) {
            if (!(this.requireFail[i].state & (STATE_FAILED | STATE_POSSIBLE))) {
                return false;
            }
            i++;
        }
        return true;
    },

    /**
     * update the recognizer
     * @param {Object} inputData
     */
    recognize: function(inputData) {
        // make a new copy of the inputData
        // so we can change the inputData without messing up the other recognizers
        var inputDataClone = assign({}, inputData);

        // is is enabled and allow recognizing?
        if (!boolOrFn(this.options.enable, [this, inputDataClone])) {
            this.reset();
            this.state = STATE_FAILED;
            return;
        }

        // reset when we've reached the end
        if (this.state & (STATE_RECOGNIZED | STATE_CANCELLED | STATE_FAILED)) {
            this.state = STATE_POSSIBLE;
        }

        this.state = this.process(inputDataClone);

        // the recognizer has recognized a gesture
        // so trigger an event
        if (this.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED | STATE_CANCELLED)) {
            this.tryEmit(inputDataClone);
        }
    },

    /**
     * return the state of the recognizer
     * the actual recognizing happens in this method
     * @virtual
     * @param {Object} inputData
     * @returns {Const} STATE
     */
    process: function(inputData) { }, // jshint ignore:line

    /**
     * return the preferred touch-action
     * @virtual
     * @returns {Array}
     */
    getTouchAction: function() { },

    /**
     * called when the gesture isn't allowed to recognize
     * like when another is being recognized or it is disabled
     * @virtual
     */
    reset: function() { }
};

/**
 * get a usable string, used as event postfix
 * @param {Const} state
 * @returns {String} state
 */
function stateStr(state) {
    if (state & STATE_CANCELLED) {
        return 'cancel';
    } else if (state & STATE_ENDED) {
        return 'end';
    } else if (state & STATE_CHANGED) {
        return 'move';
    } else if (state & STATE_BEGAN) {
        return 'start';
    }
    return '';
}

/**
 * direction cons to string
 * @param {Const} direction
 * @returns {String}
 */
function directionStr(direction) {
    if (direction == DIRECTION_DOWN) {
        return 'down';
    } else if (direction == DIRECTION_UP) {
        return 'up';
    } else if (direction == DIRECTION_LEFT) {
        return 'left';
    } else if (direction == DIRECTION_RIGHT) {
        return 'right';
    }
    return '';
}

/**
 * get a recognizer by name if it is bound to a manager
 * @param {Recognizer|String} otherRecognizer
 * @param {Recognizer} recognizer
 * @returns {Recognizer}
 */
function getRecognizerByNameIfManager(otherRecognizer, recognizer) {
    var manager = recognizer.manager;
    if (manager) {
        return manager.get(otherRecognizer);
    }
    return otherRecognizer;
}

/**
 * This recognizer is just used as a base for the simple attribute recognizers.
 * @constructor
 * @extends Recognizer
 */
function AttrRecognizer() {
    Recognizer.apply(this, arguments);
}

inherit(AttrRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof AttrRecognizer
     */
    defaults: {
        /**
         * @type {Number}
         * @default 1
         */
        pointers: 1
    },

    /**
     * Used to check if it the recognizer receives valid input, like input.distance > 10.
     * @memberof AttrRecognizer
     * @param {Object} input
     * @returns {Boolean} recognized
     */
    attrTest: function(input) {
        var optionPointers = this.options.pointers;
        return optionPointers === 0 || input.pointers.length === optionPointers;
    },

    /**
     * Process the input and return the state for the recognizer
     * @memberof AttrRecognizer
     * @param {Object} input
     * @returns {*} State
     */
    process: function(input) {
        var state = this.state;
        var eventType = input.eventType;

        var isRecognized = state & (STATE_BEGAN | STATE_CHANGED);
        var isValid = this.attrTest(input);

        // on cancel input and we've recognized before, return STATE_CANCELLED
        if (isRecognized && (eventType & INPUT_CANCEL || !isValid)) {
            return state | STATE_CANCELLED;
        } else if (isRecognized || isValid) {
            if (eventType & INPUT_END) {
                return state | STATE_ENDED;
            } else if (!(state & STATE_BEGAN)) {
                return STATE_BEGAN;
            }
            return state | STATE_CHANGED;
        }
        return STATE_FAILED;
    }
});

/**
 * Pan
 * Recognized when the pointer is down and moved in the allowed direction.
 * @constructor
 * @extends AttrRecognizer
 */
function PanRecognizer() {
    AttrRecognizer.apply(this, arguments);

    this.pX = null;
    this.pY = null;
}

inherit(PanRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof PanRecognizer
     */
    defaults: {
        event: 'pan',
        threshold: 10,
        pointers: 1,
        direction: DIRECTION_ALL
    },

    getTouchAction: function() {
        var direction = this.options.direction;
        var actions = [];
        if (direction & DIRECTION_HORIZONTAL) {
            actions.push(TOUCH_ACTION_PAN_Y);
        }
        if (direction & DIRECTION_VERTICAL) {
            actions.push(TOUCH_ACTION_PAN_X);
        }
        return actions;
    },

    directionTest: function(input) {
        var options = this.options;
        var hasMoved = true;
        var distance = input.distance;
        var direction = input.direction;
        var x = input.deltaX;
        var y = input.deltaY;

        // lock to axis?
        if (!(direction & options.direction)) {
            if (options.direction & DIRECTION_HORIZONTAL) {
                direction = (x === 0) ? DIRECTION_NONE : (x < 0) ? DIRECTION_LEFT : DIRECTION_RIGHT;
                hasMoved = x != this.pX;
                distance = Math.abs(input.deltaX);
            } else {
                direction = (y === 0) ? DIRECTION_NONE : (y < 0) ? DIRECTION_UP : DIRECTION_DOWN;
                hasMoved = y != this.pY;
                distance = Math.abs(input.deltaY);
            }
        }
        input.direction = direction;
        return hasMoved && distance > options.threshold && direction & options.direction;
    },

    attrTest: function(input) {
        return AttrRecognizer.prototype.attrTest.call(this, input) &&
            (this.state & STATE_BEGAN || (!(this.state & STATE_BEGAN) && this.directionTest(input)));
    },

    emit: function(input) {

        this.pX = input.deltaX;
        this.pY = input.deltaY;

        var direction = directionStr(input.direction);

        if (direction) {
            input.additionalEvent = this.options.event + direction;
        }
        this._super.emit.call(this, input);
    }
});

/**
 * Pinch
 * Recognized when two or more pointers are moving toward (zoom-in) or away from each other (zoom-out).
 * @constructor
 * @extends AttrRecognizer
 */
function PinchRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(PinchRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof PinchRecognizer
     */
    defaults: {
        event: 'pinch',
        threshold: 0,
        pointers: 2
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_NONE];
    },

    attrTest: function(input) {
        return this._super.attrTest.call(this, input) &&
            (Math.abs(input.scale - 1) > this.options.threshold || this.state & STATE_BEGAN);
    },

    emit: function(input) {
        if (input.scale !== 1) {
            var inOut = input.scale < 1 ? 'in' : 'out';
            input.additionalEvent = this.options.event + inOut;
        }
        this._super.emit.call(this, input);
    }
});

/**
 * Press
 * Recognized when the pointer is down for x ms without any movement.
 * @constructor
 * @extends Recognizer
 */
function PressRecognizer() {
    Recognizer.apply(this, arguments);

    this._timer = null;
    this._input = null;
}

inherit(PressRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof PressRecognizer
     */
    defaults: {
        event: 'press',
        pointers: 1,
        time: 251, // minimal time of the pointer to be pressed
        threshold: 9 // a minimal movement is ok, but keep it low
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_AUTO];
    },

    process: function(input) {
        var options = this.options;
        var validPointers = input.pointers.length === options.pointers;
        var validMovement = input.distance < options.threshold;
        var validTime = input.deltaTime > options.time;

        this._input = input;

        // we only allow little movement
        // and we've reached an end event, so a tap is possible
        if (!validMovement || !validPointers || (input.eventType & (INPUT_END | INPUT_CANCEL) && !validTime)) {
            this.reset();
        } else if (input.eventType & INPUT_START) {
            this.reset();
            this._timer = setTimeoutContext(function() {
                this.state = STATE_RECOGNIZED;
                this.tryEmit();
            }, options.time, this);
        } else if (input.eventType & INPUT_END) {
            return STATE_RECOGNIZED;
        }
        return STATE_FAILED;
    },

    reset: function() {
        clearTimeout(this._timer);
    },

    emit: function(input) {
        if (this.state !== STATE_RECOGNIZED) {
            return;
        }

        if (input && (input.eventType & INPUT_END)) {
            this.manager.emit(this.options.event + 'up', input);
        } else {
            this._input.timeStamp = now();
            this.manager.emit(this.options.event, this._input);
        }
    }
});

/**
 * Rotate
 * Recognized when two or more pointer are moving in a circular motion.
 * @constructor
 * @extends AttrRecognizer
 */
function RotateRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(RotateRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof RotateRecognizer
     */
    defaults: {
        event: 'rotate',
        threshold: 0,
        pointers: 2
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_NONE];
    },

    attrTest: function(input) {
        return this._super.attrTest.call(this, input) &&
            (Math.abs(input.rotation) > this.options.threshold || this.state & STATE_BEGAN);
    }
});

/**
 * Swipe
 * Recognized when the pointer is moving fast (velocity), with enough distance in the allowed direction.
 * @constructor
 * @extends AttrRecognizer
 */
function SwipeRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(SwipeRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof SwipeRecognizer
     */
    defaults: {
        event: 'swipe',
        threshold: 10,
        velocity: 0.3,
        direction: DIRECTION_HORIZONTAL | DIRECTION_VERTICAL,
        pointers: 1
    },

    getTouchAction: function() {
        return PanRecognizer.prototype.getTouchAction.call(this);
    },

    attrTest: function(input) {
        var direction = this.options.direction;
        var velocity;

        if (direction & (DIRECTION_HORIZONTAL | DIRECTION_VERTICAL)) {
            velocity = input.overallVelocity;
        } else if (direction & DIRECTION_HORIZONTAL) {
            velocity = input.overallVelocityX;
        } else if (direction & DIRECTION_VERTICAL) {
            velocity = input.overallVelocityY;
        }

        return this._super.attrTest.call(this, input) &&
            direction & input.offsetDirection &&
            input.distance > this.options.threshold &&
            input.maxPointers == this.options.pointers &&
            abs(velocity) > this.options.velocity && input.eventType & INPUT_END;
    },

    emit: function(input) {
        var direction = directionStr(input.offsetDirection);
        if (direction) {
            this.manager.emit(this.options.event + direction, input);
        }

        this.manager.emit(this.options.event, input);
    }
});

/**
 * A tap is ecognized when the pointer is doing a small tap/click. Multiple taps are recognized if they occur
 * between the given interval and position. The delay option can be used to recognize multi-taps without firing
 * a single tap.
 *
 * The eventData from the emitted event contains the property `tapCount`, which contains the amount of
 * multi-taps being recognized.
 * @constructor
 * @extends Recognizer
 */
function TapRecognizer() {
    Recognizer.apply(this, arguments);

    // previous time and center,
    // used for tap counting
    this.pTime = false;
    this.pCenter = false;

    this._timer = null;
    this._input = null;
    this.count = 0;
}

inherit(TapRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof PinchRecognizer
     */
    defaults: {
        event: 'tap',
        pointers: 1,
        taps: 1,
        interval: 300, // max time between the multi-tap taps
        time: 250, // max time of the pointer to be down (like finger on the screen)
        threshold: 9, // a minimal movement is ok, but keep it low
        posThreshold: 10 // a multi-tap can be a bit off the initial position
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_MANIPULATION];
    },

    process: function(input) {
        var options = this.options;

        var validPointers = input.pointers.length === options.pointers;
        var validMovement = input.distance < options.threshold;
        var validTouchTime = input.deltaTime < options.time;

        this.reset();

        if ((input.eventType & INPUT_START) && (this.count === 0)) {
            return this.failTimeout();
        }

        // we only allow little movement
        // and we've reached an end event, so a tap is possible
        if (validMovement && validTouchTime && validPointers) {
            if (input.eventType != INPUT_END) {
                return this.failTimeout();
            }

            var validInterval = this.pTime ? (input.timeStamp - this.pTime < options.interval) : true;
            var validMultiTap = !this.pCenter || getDistance(this.pCenter, input.center) < options.posThreshold;

            this.pTime = input.timeStamp;
            this.pCenter = input.center;

            if (!validMultiTap || !validInterval) {
                this.count = 1;
            } else {
                this.count += 1;
            }

            this._input = input;

            // if tap count matches we have recognized it,
            // else it has began recognizing...
            var tapCount = this.count % options.taps;
            if (tapCount === 0) {
                // no failing requirements, immediately trigger the tap event
                // or wait as long as the multitap interval to trigger
                if (!this.hasRequireFailures()) {
                    return STATE_RECOGNIZED;
                } else {
                    this._timer = setTimeoutContext(function() {
                        this.state = STATE_RECOGNIZED;
                        this.tryEmit();
                    }, options.interval, this);
                    return STATE_BEGAN;
                }
            }
        }
        return STATE_FAILED;
    },

    failTimeout: function() {
        this._timer = setTimeoutContext(function() {
            this.state = STATE_FAILED;
        }, this.options.interval, this);
        return STATE_FAILED;
    },

    reset: function() {
        clearTimeout(this._timer);
    },

    emit: function() {
        if (this.state == STATE_RECOGNIZED) {
            this._input.tapCount = this.count;
            this.manager.emit(this.options.event, this._input);
        }
    }
});

/**
 * Simple way to create a manager with a default set of recognizers.
 * @param {HTMLElement} element
 * @param {Object} [options]
 * @constructor
 */
function Hammer(element, options) {
    options = options || {};
    options.recognizers = ifUndefined(options.recognizers, Hammer.defaults.preset);
    return new Manager(element, options);
}

/**
 * @const {string}
 */
Hammer.VERSION = '2.0.6';

/**
 * default settings
 * @namespace
 */
Hammer.defaults = {
    /**
     * set if DOM events are being triggered.
     * But this is slower and unused by simple implementations, so disabled by default.
     * @type {Boolean}
     * @default false
     */
    domEvents: false,

    /**
     * The value for the touchAction property/fallback.
     * When set to `compute` it will magically set the correct value based on the added recognizers.
     * @type {String}
     * @default compute
     */
    touchAction: TOUCH_ACTION_COMPUTE,

    /**
     * @type {Boolean}
     * @default true
     */
    enable: true,

    /**
     * EXPERIMENTAL FEATURE -- can be removed/changed
     * Change the parent input target element.
     * If Null, then it is being set the to main element.
     * @type {Null|EventTarget}
     * @default null
     */
    inputTarget: null,

    /**
     * force an input class
     * @type {Null|Function}
     * @default null
     */
    inputClass: null,

    /**
     * Default recognizer setup when calling `Hammer()`
     * When creating a new Manager these will be skipped.
     * @type {Array}
     */
    preset: [
        // RecognizerClass, options, [recognizeWith, ...], [requireFailure, ...]
        [RotateRecognizer, {enable: false}],
        [PinchRecognizer, {enable: false}, ['rotate']],
        [SwipeRecognizer, {direction: DIRECTION_HORIZONTAL}],
        [PanRecognizer, {direction: DIRECTION_HORIZONTAL}, ['swipe']],
        [TapRecognizer],
        [TapRecognizer, {event: 'doubletap', taps: 2}, ['tap']],
        [PressRecognizer]
    ],

    /**
     * Some CSS properties can be used to improve the working of Hammer.
     * Add them to this method and they will be set when creating a new Manager.
     * @namespace
     */
    cssProps: {
        /**
         * Disables text selection to improve the dragging gesture. Mainly for desktop browsers.
         * @type {String}
         * @default 'none'
         */
        userSelect: 'none',

        /**
         * Disable the Windows Phone grippers when pressing an element.
         * @type {String}
         * @default 'none'
         */
        touchSelect: 'none',

        /**
         * Disables the default callout shown when you touch and hold a touch target.
         * On iOS, when you touch and hold a touch target such as a link, Safari displays
         * a callout containing information about the link. This property allows you to disable that callout.
         * @type {String}
         * @default 'none'
         */
        touchCallout: 'none',

        /**
         * Specifies whether zooming is enabled. Used by IE10>
         * @type {String}
         * @default 'none'
         */
        contentZooming: 'none',

        /**
         * Specifies that an entire element should be draggable instead of its contents. Mainly for desktop browsers.
         * @type {String}
         * @default 'none'
         */
        userDrag: 'none',

        /**
         * Overrides the highlight color shown when the user taps a link or a JavaScript
         * clickable element in iOS. This property obeys the alpha value, if specified.
         * @type {String}
         * @default 'rgba(0,0,0,0)'
         */
        tapHighlightColor: 'rgba(0,0,0,0)'
    }
};

var STOP = 1;
var FORCED_STOP = 2;

/**
 * Manager
 * @param {HTMLElement} element
 * @param {Object} [options]
 * @constructor
 */
function Manager(element, options) {
    this.options = assign({}, Hammer.defaults, options || {});

    this.options.inputTarget = this.options.inputTarget || element;

    this.handlers = {};
    this.session = {};
    this.recognizers = [];

    this.element = element;
    this.input = createInputInstance(this);
    this.touchAction = new TouchAction(this, this.options.touchAction);

    toggleCssProps(this, true);

    each(this.options.recognizers, function(item) {
        var recognizer = this.add(new (item[0])(item[1]));
        item[2] && recognizer.recognizeWith(item[2]);
        item[3] && recognizer.requireFailure(item[3]);
    }, this);
}

Manager.prototype = {
    /**
     * set options
     * @param {Object} options
     * @returns {Manager}
     */
    set: function(options) {
        assign(this.options, options);

        // Options that need a little more setup
        if (options.touchAction) {
            this.touchAction.update();
        }
        if (options.inputTarget) {
            // Clean up existing event listeners and reinitialize
            this.input.destroy();
            this.input.target = options.inputTarget;
            this.input.init();
        }
        return this;
    },

    /**
     * stop recognizing for this session.
     * This session will be discarded, when a new [input]start event is fired.
     * When forced, the recognizer cycle is stopped immediately.
     * @param {Boolean} [force]
     */
    stop: function(force) {
        this.session.stopped = force ? FORCED_STOP : STOP;
    },

    /**
     * run the recognizers!
     * called by the inputHandler function on every movement of the pointers (touches)
     * it walks through all the recognizers and tries to detect the gesture that is being made
     * @param {Object} inputData
     */
    recognize: function(inputData) {
        var session = this.session;
        if (session.stopped) {
            return;
        }

        // run the touch-action polyfill
        this.touchAction.preventDefaults(inputData);

        var recognizer;
        var recognizers = this.recognizers;

        // this holds the recognizer that is being recognized.
        // so the recognizer's state needs to be BEGAN, CHANGED, ENDED or RECOGNIZED
        // if no recognizer is detecting a thing, it is set to `null`
        var curRecognizer = session.curRecognizer;

        // reset when the last recognizer is recognized
        // or when we're in a new session
        if (!curRecognizer || (curRecognizer && curRecognizer.state & STATE_RECOGNIZED)) {
            curRecognizer = session.curRecognizer = null;
        }

        var i = 0;
        while (i < recognizers.length) {
            recognizer = recognizers[i];

            // find out if we are allowed try to recognize the input for this one.
            // 1.   allow if the session is NOT forced stopped (see the .stop() method)
            // 2.   allow if we still haven't recognized a gesture in this session, or the this recognizer is the one
            //      that is being recognized.
            // 3.   allow if the recognizer is allowed to run simultaneous with the current recognized recognizer.
            //      this can be setup with the `recognizeWith()` method on the recognizer.
            if (session.stopped !== FORCED_STOP && ( // 1
                    !curRecognizer || recognizer == curRecognizer || // 2
                    recognizer.canRecognizeWith(curRecognizer))) { // 3
                recognizer.recognize(inputData);
            } else {
                recognizer.reset();
            }

            // if the recognizer has been recognizing the input as a valid gesture, we want to store this one as the
            // current active recognizer. but only if we don't already have an active recognizer
            if (!curRecognizer && recognizer.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED)) {
                curRecognizer = session.curRecognizer = recognizer;
            }
            i++;
        }
    },

    /**
     * get a recognizer by its event name.
     * @param {Recognizer|String} recognizer
     * @returns {Recognizer|Null}
     */
    get: function(recognizer) {
        if (recognizer instanceof Recognizer) {
            return recognizer;
        }

        var recognizers = this.recognizers;
        for (var i = 0; i < recognizers.length; i++) {
            if (recognizers[i].options.event == recognizer) {
                return recognizers[i];
            }
        }
        return null;
    },

    /**
     * add a recognizer to the manager
     * existing recognizers with the same event name will be removed
     * @param {Recognizer} recognizer
     * @returns {Recognizer|Manager}
     */
    add: function(recognizer) {
        if (invokeArrayArg(recognizer, 'add', this)) {
            return this;
        }

        // remove existing
        var existing = this.get(recognizer.options.event);
        if (existing) {
            this.remove(existing);
        }

        this.recognizers.push(recognizer);
        recognizer.manager = this;

        this.touchAction.update();
        return recognizer;
    },

    /**
     * remove a recognizer by name or instance
     * @param {Recognizer|String} recognizer
     * @returns {Manager}
     */
    remove: function(recognizer) {
        if (invokeArrayArg(recognizer, 'remove', this)) {
            return this;
        }

        recognizer = this.get(recognizer);

        // let's make sure this recognizer exists
        if (recognizer) {
            var recognizers = this.recognizers;
            var index = inArray(recognizers, recognizer);

            if (index !== -1) {
                recognizers.splice(index, 1);
                this.touchAction.update();
            }
        }

        return this;
    },

    /**
     * bind event
     * @param {String} events
     * @param {Function} handler
     * @returns {EventEmitter} this
     */
    on: function(events, handler) {
        var handlers = this.handlers;
        each(splitStr(events), function(event) {
            handlers[event] = handlers[event] || [];
            handlers[event].push(handler);
        });
        return this;
    },

    /**
     * unbind event, leave emit blank to remove all handlers
     * @param {String} events
     * @param {Function} [handler]
     * @returns {EventEmitter} this
     */
    off: function(events, handler) {
        var handlers = this.handlers;
        each(splitStr(events), function(event) {
            if (!handler) {
                delete handlers[event];
            } else {
                handlers[event] && handlers[event].splice(inArray(handlers[event], handler), 1);
            }
        });
        return this;
    },

    /**
     * emit event to the listeners
     * @param {String} event
     * @param {Object} data
     */
    emit: function(event, data) {
        // we also want to trigger dom events
        if (this.options.domEvents) {
            triggerDomEvent(event, data);
        }

        // no handlers, so skip it all
        var handlers = this.handlers[event] && this.handlers[event].slice();
        if (!handlers || !handlers.length) {
            return;
        }

        data.type = event;
        data.preventDefault = function() {
            data.srcEvent.preventDefault();
        };

        var i = 0;
        while (i < handlers.length) {
            handlers[i](data);
            i++;
        }
    },

    /**
     * destroy the manager and unbinds all events
     * it doesn't unbind dom events, that is the user own responsibility
     */
    destroy: function() {
        this.element && toggleCssProps(this, false);

        this.handlers = {};
        this.session = {};
        this.input.destroy();
        this.element = null;
    }
};

/**
 * add/remove the css properties as defined in manager.options.cssProps
 * @param {Manager} manager
 * @param {Boolean} add
 */
function toggleCssProps(manager, add) {
    var element = manager.element;
    if (!element.style) {
        return;
    }
    each(manager.options.cssProps, function(value, name) {
        element.style[prefixed(element.style, name)] = add ? value : '';
    });
}

/**
 * trigger dom event
 * @param {String} event
 * @param {Object} data
 */
function triggerDomEvent(event, data) {
    var gestureEvent = document.createEvent('Event');
    gestureEvent.initEvent(event, true, true);
    gestureEvent.gesture = data;
    data.target.dispatchEvent(gestureEvent);
}

assign(Hammer, {
    INPUT_START: INPUT_START,
    INPUT_MOVE: INPUT_MOVE,
    INPUT_END: INPUT_END,
    INPUT_CANCEL: INPUT_CANCEL,

    STATE_POSSIBLE: STATE_POSSIBLE,
    STATE_BEGAN: STATE_BEGAN,
    STATE_CHANGED: STATE_CHANGED,
    STATE_ENDED: STATE_ENDED,
    STATE_RECOGNIZED: STATE_RECOGNIZED,
    STATE_CANCELLED: STATE_CANCELLED,
    STATE_FAILED: STATE_FAILED,

    DIRECTION_NONE: DIRECTION_NONE,
    DIRECTION_LEFT: DIRECTION_LEFT,
    DIRECTION_RIGHT: DIRECTION_RIGHT,
    DIRECTION_UP: DIRECTION_UP,
    DIRECTION_DOWN: DIRECTION_DOWN,
    DIRECTION_HORIZONTAL: DIRECTION_HORIZONTAL,
    DIRECTION_VERTICAL: DIRECTION_VERTICAL,
    DIRECTION_ALL: DIRECTION_ALL,

    Manager: Manager,
    Input: Input,
    TouchAction: TouchAction,

    TouchInput: TouchInput,
    MouseInput: MouseInput,
    PointerEventInput: PointerEventInput,
    TouchMouseInput: TouchMouseInput,
    SingleTouchInput: SingleTouchInput,

    Recognizer: Recognizer,
    AttrRecognizer: AttrRecognizer,
    Tap: TapRecognizer,
    Pan: PanRecognizer,
    Swipe: SwipeRecognizer,
    Pinch: PinchRecognizer,
    Rotate: RotateRecognizer,
    Press: PressRecognizer,

    on: addEventListeners,
    off: removeEventListeners,
    each: each,
    merge: merge,
    extend: extend,
    assign: assign,
    inherit: inherit,
    bindFn: bindFn,
    prefixed: prefixed
});

// this prevents errors when Hammer is loaded in the presence of an AMD
//  style loader but by script tag, not by the loader.
var freeGlobal = (typeof window !== 'undefined' ? window : (typeof self !== 'undefined' ? self : {})); // jshint ignore:line
freeGlobal.Hammer = Hammer;

if (typeof define === 'function' && define.amd) {
    define(function() {
        return Hammer;
    });
} else if (typeof module != 'undefined' && module.exports) {
    module.exports = Hammer;
} else {
    window[exportName] = Hammer;
}

})(window, document, 'Hammer');
;var Notes = {};;(function(Notes) {

	var elemental = {
	    addClass: function(element, className) {
	        if (element.classList && element.classList.add) {
	            element.classList.add(className);
	        } else if (element.className) {
	            element.className = element.className + ' ' + className;
	        }
	        return element;
	    },
	    removeClass: function(element, className) {
	        if (element.classList && element.classList.remove) {
	            element.classList.remove(className);
	        } else if (element.className) {
	            element.className = element.className.toString().replace(className, '');
	        }
	        return element;
	    },
	    removeClassWithPrefix: function(element, prefix) {
	        if (element.classList && element.classList.remove) {
	            element.classList.remove(prefix);
	        } else if (element.className) {
	            element.className = element.className.toString().replace(/[aA-zZ\-]*/, '');
	        }
	        return element;
        },
        hasClass: function(element, className) {
        	
        	if (element.classList && element.classList.remove) {
        		for (var property in element.classList) {
        			if (element.classList.hasOwnProperty(property)) {
        				if (element.classList[property] === className)
        					return true
        			}
        		}
	        } else if (element.className) {
	        	if (element.className.toString.indexOf(/[aA-zZ\-]*/)) {
	        		return true;
	        	}
	        }

	        return false;
        },
		toClass: function(className, fn) {
        	Array.prototype.forEach.call(document.getElementsByClassName(className), fn);
        },
        createElement: function() {

        },
        // @refactor
        removeElement: function() {
        	if (!('remove' in Element.prototype)) {
			    Element.prototype.remove = function() {
			        if (this.parentNode) {
			            this.parentNode.removeChild(this);
			        }
			    };
			} else {
				element.remove();
			}
        }
	};

	Notes.elemental = elemental;

})(Notes);;(function(Notes) {

// meant to be inherited into another objects prototype
    var events = (function() {

        var listeners = [];

        var addListener = function(name, callback) {            // @todo validate listener properties

            listeners.push({
            	name: name,
            	callback: callback
            });
        
        };

        var emit = function(name, context) {

            if (typeof name !== 'string') {

            }

            var args = Array.prototype.splice.call(arguments, 2);

            listeners.forEach(function(listener, index, array) {
                for (var prop in listener) {
                    if(listener.hasOwnProperty(prop)) {
                        if ((prop === 'name') && (listener[prop] === name)) {
                            listener.callback.apply(context || this, args);
                        }
                    }
                }
            }.bind(this));
            return this;
        };

        return {
          addListener: addListener,
          emit: emit
        }

    })();

	Notes.events = events;

})(window.Notes);;(function(Notes) {

    var uniqueId = function() {
        return parseInt(new Date().getTime() * Math.random(100), 10);
    };

    Notes.uniqueId = uniqueId;

})(Notes || {});(function(Notes, window) {

  var fetch = function(options) {

      $.ajax({
        url: options.url,
        type: options.method,
        data: options.data,
        contentType: 'application/json',
        dataType: 'json',
      }).error(function(data) {
        if (data.status === 401) {
          // if (window.location.pathname !== '/tour') {
          window.location = '/login';
          // }

        }
      }).done(function(data) {
          options.callback(data);
      });
   
    };


  Notes.fetch = fetch;


  var message = (function(options) {

    Notes.events.addListener('message', function(message) {
      var messageQueue = document.getElementById('console');
      var m = "<div class='console-message'>"+message+"</div>";
      var consoleMessage = $(m).appendTo(messageQueue).velocity("slideDown", {
        easing: "spring",
        duration: 300
      }).delay(2000).fadeOut(100, function() {
        this.remove();
      });

    });

  })();

  Notes.message = message;

})(Notes || {}, window);(function(Notes, localStorage) {

  var _ = Notes.events;

  var User = function() {
 		  this.isUserLoggedIn = false;
      this.listen.call(this);
      this.loadUser.call(this);
  };

  User.prototype.init = function(options) {
      if (typeof options === 'string') {
        options = JSON.parse(options);
      }
      if (options.token)
        localStorage.setItem('notes-token', options.token);
      this.username = options.username;
      this.login();

  };

  User.prototype.listen = function() {
      _.addListener('user-login', this.init.bind(this));
  };

  User.prototype.doesUserTokenExist = function() {
    if (localStorage.getItem('notes-token')) {
        return true;
    }
    return false;
  };

  User.prototype.loadUser = function() {
    if (this.doesUserTokenExist()) {
          Notes.api.write({
            method: "POST",
            endpoint: "user",
            name: "user-login",
            data: {
              token: localStorage.getItem('notes-token')
            }
          });
    }
  };

  User.prototype.login = function() {
  		this.isUserLoggedIn = true;
  		return this;
  };

  User.prototype.logout = function() {
  		this.isUserLoggedIn = false;
  		return this;
  };

  User.prototype.getStatus = function() {
  	return this.isUserLoggedIn;
  };

  User.prototype.getUsername = function() {
  		return this.username;
  };


  Notes.User = User;

})(Notes, localStorage);/**
 * @todo an wrapper for whatever xhr client is being used
 * @param  {[type]} Notes        [description]
 * @param  {[type]} localStorage [description]
 * @return {[type]}              [description]
 */
(function(Notes, localStorage) {

  var e = Notes.events;
  var api = (function() {

    //var rootUrl = 'http://ec2-52-36-65-67.us-west-2.compute.amazonaws.com/api/';
    var rootUrl = 'http://localhost:3000/api/';
    return {
        write: function(options){
            return Notes.fetch({
                "method": options.method,
                "url": rootUrl + options.endpoint,
                "data": JSON.stringify(options.data),
                "callback": function(response) {
                    if (options.name) {
                        e.emit(options.name, options.context || null, response);
                    } else if (options.message) {
                        e.emit(options.message.name, options.message.context, options.message.message, options.message.type);
                    }
                }
            });
        },
        get: function(options) {
            return Notes.fetch({
                "method": options.method,
                "url": rootUrl + options.endpoint,
                "data": JSON.stringify(options.data),
                "callback": function(response) {
                    e.emit(options.name, options.context || null, response);
                }
            });
        }
    }

  })();

  Notes.api = api;


})(Notes, localStorage); (function(Notes, localStorage) {

 		var $ = Notes.elemental;
 		var _ = Notes.events;

		/**
		 * [Button description]
		 * @param {[type]} options [description]
		 */
		var LoginFormButton = function(options) {
			this.button = options.button;
			this.loaderClassName = options.loaderClassName || 'btn__loader';
			this.successClassName = options.successClassName || 'btn__success';
			this.contentClassName = options.contentClassName || 'btn__content';
			this.loader = this.button.getElementsByClassName(this.loaderClassName)[0];
			this.success = this.button.getElementsByClassName(this.successClassName)[0];
			this.content = this.button.getElementsByClassName(this.contentClassName)[0];
			this.inner = [this.loader, this.success, this.content];
		};

		/**
		 * [changeButtonState description]
		 * @param  {[type]} state [description]
		 * @return {[type]}       [description]
		 */
		LoginFormButton.prototype.changeButtonState = function(state) {
			this.setDefault();
			switch (state) {
				case 'ready':
					this.show(this.content);
					break;
				case 'waiting':
					this.show(this.loader);
					break;
				case 'success':
					this.show(this.success);
					break;
			}
		};

		/**
		 * [hide description]
		 * @param  {[type]} element [description]
		 * @return {[type]}         [description]
		 */
		LoginFormButton.prototype.hide = function(element) {
			return element.style.display = 'none';
		};

		/**
		 * [show description]
		 * @param  {[type]} element [description]
		 * @return {[type]}         [description]
		 */
		LoginFormButton.prototype.show = function(element) {
			return element.style.display = 'block';
		};

		/**
		 * [setDefault description]
		 */
		LoginFormButton.prototype.setDefault = function() {
			this.inner.forEach(function(content) {
				this.hide.call(this, content);
			}.bind(this));
		};

        if (Notes.elements === undefined) {
	        Notes.elements = {};
	    }
    
	    Notes.elements.LoginFormButton = LoginFormButton;

})(Notes, localStorage);; (function(Notes, localStorage) {

 		var $ = Notes.elemental;
 		var _ = Notes.events;

		/**
		 * [Input description]
		 * @param {[type]} value [description]
		 */
		var LoginFormInput = function(input) {
			this.state = 'none';
			this.input = input;
			this.successClass = 'login-form__message success ion-android-done';
			this.errorClass = 'login-form__message error ion-android-warning';
		}
		
		/**
		 * [changeValidationState description]
		 * @param  {[type]} state [description]
		 * @return {[type]}       [description]
		 */
		LoginFormInput.prototype.changeValidationState = function(state, validated) {
			if (state !== this.state) {
				switch (state) {
					case 'none':
						this.removeIcon;
						break;
					case 'success':
						this.addIcon(this.successClass)
						break;
					case 'error':
			        	if (validated) return $.addClass(this.input, 'error');
						this.addIcon(this.errorClass);
						break;
				}
			}
			this.state = state;
		};

		/**
		 * [addIcon description]
		 * @param {[type]} className [description]
		 */
		LoginFormInput.prototype.addIcon = function(className) {
			this.removeIcon();
			this.input.insertAdjacentHTML('afterend', '<div class="'+className+'"></div>');
		};

		/**
		 * [removeIcon description]
		 * @return {[type]} [description]
		 */
		LoginFormInput.prototype.removeIcon = function() {
			this.input.nextSibling.remove();
		};

        if (Notes.elements === undefined) {
	        Notes.elements = {};
	    }
    
	    Notes.elements.LoginFormInput = LoginFormInput;

})(Notes, localStorage);; (function(Notes, localStorage) {

 		var $ = Notes.elemental;
 		var _ = Notes.events;

		/**
		 * @todo refactor
		 * @param {[type]} form [description]
		 */
		var LoginForm = function(options) {
			this.form = options.form;
			this.inputs = this.form.getElementsByTagName('input');
			this.input = {};
			this.userField = this.inputs[0];
			this.passwordField = this.inputs[1];
			this.submitButton = this.form.getElementsByTagName('button')[0];
			this.init();
			this.listen();
		};

		/**
		 * [init description]
		 * @return {[type]} [description]
		 */
		LoginForm.prototype.init = function() {
			this.button = new Notes.elements.LoginFormButton({
				button: this.submitButton
			});
		};

		/**
		 * [changeValidationState description]
		 * @param  {[type]} value [description]
		 * @param  {[type]} input [description]
		 * @return {[type]}       [description]
		 */
		LoginForm.prototype.changeValidationState = function(value, input) {
			this.validate(value) ? input.changeValidationState('success') : input.changeValidationState('error');
		};

		/**
		 * [listen description]
		 * @return {[type]} [description]
		 */
		LoginForm.prototype.listen = function() {

			Array.prototype.forEach.call(this.inputs, function(value, index, array) {
				var input = this.input[value.dataset.field] = new Notes.elements.LoginFormInput(value);
				value.addEventListener('keyup', function(event) {
					this.changeValidationState(value.value, input);
				}.bind(this));

				value.addEventListener('focus', function(event) {
					$.removeClass(value, 'error');
					this.changeValidationState(value.value, input);
				}.bind(this));

			}.bind(this));

			this.submitButton.addEventListener('click', function(event) {
				event.preventDefault();
				this.button.changeButtonState('waiting');
				this.submit.call(this, this.submitButton.dataset.function);
			}.bind(this));

			// response listener
			_.addListener('login-response', function(response) {
				response === 'success' ? this.button.changeButtonState(response) : this.button.changeButtonState('ready');
			}.bind(this));

			_.addListener(this.submitButton.dataset.function, this.response.bind(this));

		};

		/**
		 * [validate description]
		 * @param  {[type]} value [description]
		 * @return {[type]}       [description]
		 */
		LoginForm.prototype.validate = function(value) {
			
			if (value === '') {
				_.emit('message', null, 'This field can\'t be empty');
				return false;
			}

			if (value.length > 14) {
				_.emit('message', null, 'Too long!');
				return false;
			}
			return true;

		};

		LoginForm.prototype.validateOnSubmit = function() {
			
			Array.prototype.forEach.call(this.inputs, function(input) {
 				if (input.value === '') {
					this.input[input.dataset.field].changeValidationState('error');
					_.emit('login-response', null, 'error');
					_.emit('message', this, 'The form can\'t be blank!');
				}
			}.bind(this));

		};

		LoginForm.prototype.resetInputs = function() {
			Array.prototype.forEach.call(this.inputs, function(input) {
				Notes.elemental.removeClass(input, 'error');
			});
		};

		LoginForm.prototype.getFormValues = function() {
			return {
				username: this.inputs[0].value,
				password: this.inputs[1].value
			};
		};

		LoginForm.prototype.submit = function(type) {
			this.resetInputs();
	        this.validateOnSubmit();
	        var values = this.getFormValues();
	        var doc = {
	            method: "POST",
	            endpoint: type,
	            name: type,
	            data: {
	                username: values.username,
	                password: values.password
	            }
	        };

	        Notes.api.write(doc);
	    };

    	LoginForm.prototype.response = function(data) {
	        if (data.error !== undefined) {
	        }

	        if (data.hasOwnProperty('token')) {
	            // new user
	            _.emit('user-login', this, data);
	            _.emit('message', this, 'Successfully logged in!'); 
	            _.emit('login-response', null, 'success');  
	            
         	if (window.location.pathname === '/login') 
         		window.location = '/';
		       

	        } else {
	            _.emit('message', Notes, data.error);
	            _.emit('login-response', null, 'error'); 
	            if (data.error === "Invalid Password") {
	            	this.input['password'].changeValidationState('error', true);
	            } else if(data.error === "Unknown User") {
	            	this.input['login'].changeValidationState('error', true);
	            } else if(data.error === 'User Exists') {
  	            	this.input['login'].changeValidationState('error', true);
	            }
	        }
	    }

        if (Notes.elements === undefined) {
	        Notes.elements = {};
	    }
    
	    Notes.elements.LoginForm = LoginForm;

})(Notes, localStorage);;(function(Notes) {
	 var e = Notes.events;

     /**
      *
      * @param {[type]} options [description]
      */
     var LoginModal = function(options) {
        this.modal = options.modal;
        this.toggleFormButton = options.toggleFormButton || document.getElementsByClassName('switch-form');
        this.loginForm = options.loginForm || document.getElementById('login-form');
        this.signupForm = options.signupForm || document.getElementById('signup-form');
        this._isModalOpen = false;
        this._isLoginPaneActive = true;

        this.hideForm(this.signupForm);

        this.listen.call(this);
  	};

    /**
     * [listen description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.listen = function() {

      e.addListener('login-btn-clicked', this.toggleLoginModal.bind(this));
      e.addListener('user-login', this.close.bind(this));

      Array.prototype.forEach.call(this.toggleFormButton, function(button) {
          button.addEventListener('click', function() {
              this.toggleSignupForm.call(this);
          }.bind(this));
      }.bind(this));

      return this;
    };

    /**
     * [toggleLoginModal description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.toggleLoginModal = function() {

        if(!this._isModalOpen) {
          this.open.call(this);
        } else {
          this.close.call(this);
        }
        return this;
    };

    /**
     * [toggleSignupForm description]
     * @todo  refactor
     * @return {[type]} [description]
     */
    LoginModal.prototype.toggleSignupForm = function() {
        this.hideForms([this.loginForm, this.signupForm]);
        !this._isLoginPaneActive ? this.showForm(this.loginForm) : this.showForm(this.signupForm);
     		this._isLoginPaneActive = !this._isLoginPaneActive;
   
    		return this;
    };

    /**
     * [hideForms description]
     * @param  {[type]} forms [description]
     * @return {[type]}       [description]
     */
    LoginModal.prototype.hideForms = function(forms) {
        return forms.forEach(function(form){
          this.hideForm(form);
        }.bind(this));
    };

    /**
     * [showForm description]
     * @param  {[type]} form [description]
     * @return {[type]}      [description]
     */
    LoginModal.prototype.showForm = function(form) {
        return form.style.display = 'block';
    };

    /**
     * [hideForm description]
     * @param  {[type]} form [description]
     * @return {[type]}      [description]
     */
    LoginModal.prototype.hideForm = function(form) {
        return form.style.display = 'none';
    };

    /**
     * [open description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.open = function() {
      if (!this._isModalOpen) {
        $.Velocity(this.modal, "slideDown", {
          duration: 500,
          easing: 'spring'
        });
      }
      this._isModalOpen = !this._isModalOpen;
      return this;
    };

    /**
     * [close description]
     * @return {[type]} [description]
     */
    LoginModal.prototype.close = function() {
      if (this._isModalOpen) {
        $.Velocity(this.modal, "slideUp", {
          duration: 150,
          easing: "easeIn"
        });
        this._isModalOpen = !this._isModalOpen;
      }
      return this;
    };

  	if (Notes.elements === undefined) {
  		Notes.elements = {};
  	}

  	Notes.elements.LoginModal = LoginModal;

})(Notes);
;(function(Notes) {


	var UserNav = function() {

		this.isUserNavOpen = false;
		this.userBtn = document.getElementById('user-btn');
		this.listen();

	};

	UserNav.prototype.init = function() {
		
		var handle = function(event) {
			document.addEventListener('click', function(event) {
				if (event.toElement !== this.userBtn) {
					this.hide();
				}
			}.bind(this));
		}.bind(this);

		document.removeEventListener('click', handle);
		document.addEventListener('click', handle);

	};

	UserNav.prototype.listen = function() {

		this.userBtn.addEventListener('click', function() {

			!this.isUserNavOpen ? this.show.call(this) : this.hide.call(this);
			this.init(this.userBtn);
	
		}.bind(this));	
	};

	UserNav.prototype.show = function() {
		if (!this.isUserNavOpen) {
			//$('#user-menu').show();
			$.Velocity($('#user-menu'), "slideDown", {
				easing: "easeInOut",
				duration: 50
			});
			this.isUserNavOpen = !this.isUserNavOpen;
		}
	};

	UserNav.prototype.hide = function() {
		if(this.isUserNavOpen) {
			$.Velocity($('#user-menu'), "slideUp", {
				easing: "easeInOut",
				duration: 50
			});
			this.isUserNavOpen = !this.isUserNavOpen;
		}
	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.UserNav = UserNav;

})(Notes);;(function(Notes, localStorage) {
	var _ = Notes.events;

	var UserMenu = function(options) {

		this.loginButton = options.loginBtn;
		this.logoutButton = options.logoutBtn;
		this.userButton = options.userBtn;
		this.user = options.user;
		this.listen.call(this);
		this.init.call(this);
	};

	UserMenu.prototype.init = function() {

	};

	UserMenu.prototype.listen = function() {
        this.loginButton.addEventListener('click', this.login.bind(this));
        this.logoutButton.addEventListener('click', this.logout.bind(this));
        _.addListener('user-login', function(user) { this.update.call(this, user); }.bind(this));
	};


	UserMenu.prototype.logout = function() {
		this.update.call(this);
		_.emit('message', Notes, 'Logging out ...');
		this.user.logout();
        localStorage.removeItem('notes-token');
        window.location = '/';
	};

	UserMenu.prototype.login = function() {
		_.emit('login-btn-clicked');
	};

	UserMenu.prototype.toggleUserButton = function(isUserLoggedIn) {
		this.user.isUserLoggedIn ? this.showUserButton() : this.showLoginButton();
	};

	UserMenu.prototype.showUserButton = function() {
		this.loginButton.style.display = 'none';
		this.userButton.innerHTML = this.user.username;
		this.userButton.style.display = '';
	};

	UserMenu.prototype.showLoginButton = function() {
        this.userButton.style.display = 'none';
		this.loginButton.style.display = 'block';
	};

	UserMenu.prototype.update = function() {
		this.toggleUserButton.call(this, this.user.isUserLoggedIn);
	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.UserMenu = UserMenu;

})(Notes, localStorage);
;	(function(Notes, window) {

	var _ = Notes.events;

	var NoteMenu = function(options) {
		this.$newBtn = options.newBtn;
		this.$reminderBtn = options.reminderBtn;
		this.$detailsBtn = options.detailsBtn;
		this.$saveBtn = options.saveBtn;
		this.isDetailsActive = false;
		this.init.call(this);
		this.listen.call(this);
	};

	NoteMenu.prototype.init = function() {

	};

	NoteMenu.prototype.listen = function() {
		this.$newBtn.addEventListener('click', function() {
			window.location = '/';
		});

		this.$detailsBtn.addEventListener('click', function() {
			this.isDetailsActive = !this.isDetailsActive;
			if (this.isDetailsActive) {
				$(this.$detailsBtn).addClass('active');
			} else {
				$(this.$detailsBtn).removeClass('active');
			}
			_.emit('details-btn-clicked');
		}.bind(this));

		this.$saveBtn.addEventListener('click', function() {
			_.emit('save-btn-clicked');
		});

	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.NoteMenu = NoteMenu;

})(Notes, window);;(function(Notes, localStorage) {

    var Note = function(options) {

        this.note = options.note;
      
        this.listen();
    };

    Note.prototype.listen = function() {
        Notes.events.addListener('save-btn-clicked', function() {
            if (!Notes.user.isUserLoggedIn) {
                alert('Please login or create an account to save');
            } else {
                this.save.call(this);
            }
        }.bind(this));
    };

    Note.prototype.getNoteData = function() {          
        var doc = {};
        doc.fields = {};
        doc.ideas = [];
        doc.descriptions = [];
        doc.details = [];
        doc.footnotes = [];

        var serialize = function(value, index) {
            var id;
            if (value.dataset !== undefined) {
              if (value.dataset.uniqid !== undefined) {
                id = value.dataset.uniqid;
              }
            }

            if (id) {
              if(value.classList.contains('ideas-editor')) {
                  doc.ideas.push({
                    "name": value.name,
                    "value": value.value,
                    "id": id
                  });
              }
              if (value.classList.contains('description-editor')) {
                  doc.descriptions.push({
                    "name": value.name,
                    "value": value.innerHTML,
                    "id": id
                  });
              }
              
            } else {
              if (value.name === 'title') {
                if (value.value === '' || value.value === undefined) {
                  value.value = 'Untitled'
                }
              }
              doc.fields[value.name] = {
               "value": value.value
              }
            }
            
        }

        var serializeDetails = function(value) {
            var name = value.getElementsByClassName('details-name');
            var val = value.getElementsByClassName('details-value');
            doc.details.push({
                name:  name[0].value,
                value: val[0].value
            });
        };

        var serializeFootnotes = function(value) {
            var input = value.getElementsByTagName('textarea');
            doc.footnotes.push({
                name: input[0].name,
                value: input[0].value,
                id: value.dataset.id,
                noteId: value.dataset.noteId
            });
        };

        var inputs = document.getElementsByClassName('input');
        var editors = document.getElementsByClassName('editor');
        var details = document.getElementsByClassName('details-form');
        var footnotes = document.getElementsByClassName('footnote');

        Array.prototype.forEach.call(editors, serialize, this);
        Array.prototype.forEach.call(inputs, serialize, this);
        Array.prototype.forEach.call(footnotes, serializeFootnotes, this);
        Array.prototype.forEach.call(details, serializeDetails, this);
        return doc;

    };

    Note.prototype.serialize = function() {

    };

    Note.prototype.save = function() {
        var doc = {
            data: {
                note: this.getNoteData(),
                noteId: this.note.dataset.noteId,
                token: localStorage.getItem('notes-token'),
            },
            method: "POST",
            endpoint: "note/save",
            message: {
                name: "message",
                context: Notes,
                message: 'Note Saved',
                type: 'success'
            }
        };

        Notes.api.write(doc);
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.Note = Note;

})(Notes, localStorage);
;(function(Notes, setTimeout) {

	var _ = Notes.events;

	var EditorShortcuts = function(options) {
		this.isIdeaEditing = false;
		this._carriageLog = [];
		this.editor = options.editor;
		this.listen();
	};

	EditorShortcuts.prototype.init = function() {

	};

	EditorShortcuts.prototype.listen = function() {

		_.addListener('isIdeaEditing', function (bool) {
			this.isIdeaEditing = bool;
		}.bind(this));

		$(this.editor).on('keydown', function(event) {
			this.carriageListener(event);
		}.bind(this));

	};

	EditorShortcuts.prototype.carriageListener = function(event) {
		if (event.keyCode === 13) {
            event.preventDefault();
            if (this._carriageLog.length === 1) {
              this.create.call(this);
              this._carriageLog = []; 
            } else {
              this._carriageLog.push(event);
            }
        } else {
            if (this._carriageLog.length > 0) {
                this._carriageLog = [];
            }
        }
	};

	EditorShortcuts.prototype.create = function() {
		if(!this.isIdeaEditing) {
			_.emit('create-new-editor');
		}
	};

	if (Notes.utilities === undefined) {
		Notes.utilities = {};
	}

	Notes.utilities.EditorShortcuts = EditorShortcuts;

})(Notes, window.setTimeout);;(function(Notes) {

	var $ = Notes.elemental;
	var _ = Notes.events;

	var EditIdeasMenu = function() {
		this.isIdeaEditing = false;
		this.init();
		this.listen();
	};

	EditIdeasMenu.prototype.init = function() {
		this.addIdeasButton = document.getElementById('add-ideas-item');
		this.ideasEditorContainer = document.getElementById('ideas-editor-container');
		this.ideasEditors = document.getElementsByClassName('ideas-editor');
		this.closeOptionsButton = document.getElementById('close-editor-options-button');
		this.openOptionsButton = document.getElementById('open-editor-options-button');
		console.log(this.openOptionsButton);
		this.deleteIdeaButton = document.getElementsByClassName('delete-idea');
	};

	EditIdeasMenu.prototype.listen = function() {

		this.openOptionsButton.addEventListener('click', function() {
			this.toggle();
		}.bind(this));

		this.closeOptionsButton.addEventListener('click', function() {
			this.toggle();
		}.bind(this));
	};

	EditIdeasMenu.prototype.toggleDeleteButton = function() {
		Array.prototype.forEach.call(this.deleteIdeaButton, function(button) {
			this.isEditing ? button.style.display = 'block' : button.style.display = 'none';
		}.bind(this));
	};

	EditIdeasMenu.prototype.toggleIdeasEditor = function() {
		Array.prototype.forEach.call(this.ideasEditors, function(editor) {
			this.isEditing ? editor.setAttribute('disabled', 'disabled') : editor.removeAttribute('disabled');
		}.bind(this));
	};

	EditIdeasMenu.prototype.toggleIdeasEditorContainer = function() {
		this.isEditing ? $.addClass(this.ideasEditorContainer, 'editing') : $.removeClass(this.ideasEditorContainer, 'editing');
	};

	EditIdeasMenu.prototype.toggleAddIdeasButton = function() {
		this.isEditing ? $.addClass(this.addIdeasButton, 'disabled') : $.removeClass(this.addIdeasButton, 'disabled');
	};

	EditIdeasMenu.prototype.toggleOptionsButton = function() {
		if (this.isEditing) {
			this.openOptionsButton.style.display = 'none';
			this.closeOptionsButton.style.display = 'inline';
		} else {
			this.openOptionsButton.style.display = 'block';
			this.closeOptionsButton.style.display = 'none';
		}
	};

	EditIdeasMenu.prototype.toggle = function() {
		this.isEditing = !this.isEditing;
		this.toggleAddIdeasButton();
		this.toggleIdeasEditorContainer();
		this.toggleIdeasEditor();
		this.toggleDeleteButton();
		this.toggleOptionsButton();

		_.emit('isIdeaEditing', null, this.isEditing);
		
		if (this.isEditing)
			_.emit('message', null, 'Editing Ideas');

	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.EditIdeasMenu = EditIdeasMenu;

})(Notes);;(function(Notes) {

	var $ = Notes.elemental;
	var _ = Notes.events;

	var Editor = function(options) {
		this.editor = options.editor;
		this.id = this.editor.dataset.uniqid;
		this.deleteBtn = document.getElementById('delete-idea-' + this.id);
		this.isIdeaEditing = false;
		this.isDescriptionActive = false;
		this.isCommentActive = false;
		this.descriptionEditor = document.getElementById('description-editor-' + this.id);
		this.createFootnoteButton = document.getElementById('create-footnote-btn-' + this.id);
		// select all footnotes by data attribute
		this.footNotes = document.getElementById('footnote-' + this.id);
		var editMenu = new Notes.elements.EditIdeasMenu();
		var shortcuts = new Notes.utilities.EditorShortcuts({editor: this.editor});
		$.hasClass(this.editor, 'ideas-editor');
		this.listen();
	};

	Editor.prototype.init = function() {
	};

	/**
	 * Each editor is listening for focus and inputs 
	 * @return [type]} [description]
	 */
	Editor.prototype.listen = function() {

		_.addListener('isIdeaEditing', function (bool) {
			this.isIdeaEditing = bool;
		}.bind(this));

		this.editor.addEventListener('focus', function() {
			this.addActiveClass.call(this);
			_.emit('idea-focus', null, this.id, true);
		}.bind(this));

		_.addListener('description-focus', function(id, hasFocus) {
			if (id === this.id.toString()) {
				this.isDescriptionActive = hasFocus;
			}
		}.bind(this));

		_.addListener('comment-focus', function(id, hasFocus) {
			if (id === this.id.toString()) {
				this.isCommentActive = hasFocus;
			}
		}.bind(this));

		this.editor.addEventListener('blur', function() {
			this.removeActiveClass.call(this);
			_.emit('idea-focus', null, this.id, false);
		}.bind(this));

		this.deleteBtn.addEventListener('click', function() {
			this.delete();
		}.bind(this));

	};

	Editor.prototype.addActiveClass = function() {
		if ($.hasClass(this.editor, 'ideas-editor')) {

			$.toClass('ideas-editor', function(editor) {
				$.removeClass(editor.parentNode, 'active');
			});

			$.addClass(this.editor.parentNode, 'active');
		}
	};

	Editor.prototype.removeActiveClass = function() {
		$.removeClass(this.editor.parentNode, 'active');
	};

	Editor.prototype.delete = function() {
		this.editor.parentNode.remove();
		this.descriptionEditor.parentNode.remove();
		this.createFootnoteButton.remove();
		this.footNotes.remove();
		_.emit('message', null, 'Idea Removed');
	};

	Notes.Editor = Editor;

})(Notes);;(function(Notes, setTimeout) {

	//var $ = Notes.elemental;

	var Editors = function(options) {
		this.container = options.container;
		this.editors = $(this.container).find($('.editor'));
		this.descriptions = document.getElementById('description-editor-container');
		this.isEditing = false;
		this.editorsArr = [];
		this.init();
		this.listen();
	};

	Editors.prototype.init = function() {
		//$.toClass(this.editors, this.initEditor.bind(this));
	};

	Editors.prototype.listen = function() {
		Notes.events.addListener('create-new-editor', function() {
			this.create();
		}.bind(this));

		$('.add-ideas-item').on('click', function() {
			this.create();
		}.bind(this));
	};

	Editors.prototype.create = function() {
		if (!this.isEditing) {
			var editor = this.append.call(this);
			this.initEditor.call(this, editor);
			Notes.events.emit('new-idea', null);
	        Notes.events.emit('message', Notes, 'New Idea Added');
		}
	};

	Editors.prototype.getUniqid = function() {
		return parseInt(new Date().getTime() * Math.random(100), 10);
	};

	Editors.prototype.initEditor = function(editor) {
		var e = new Notes.Editor({
		   editor: editor[0]
	    });

	    this.editorsArr.push(e);
	};

	Editors.prototype.removeEditor = function(editor) {

	};

	/**
	 * @todo refactor
	 * @return {[type]} [description]
	 */
	Editors.prototype.append = function() {
	    var uniqid = this.getUniqid();
        var editor = $(this.container).append('<li class="ideas__item"><a id="delete-idea-'+uniqid+'" href="javascript:;" class="delete-idea btn--small" data-uniqid="'+uniqid+'"><span data-uniqid="'+uniqid+'" class="ion-android-close"></span></a><textarea id="ideas-editor-'+uniqid+'" class="editor ideas-editor" data-uniqid="'+uniqid+'" placeholder="Idea"></textarea></li>');
        $(this.descriptions).append('<div id="description-editor-'+uniqid+'" class="editor description-editor" data-uniqid="'+uniqid+'" contenteditable></div><a id="create-footnote-btn-'+uniqid+'" class="create-footnote-btn ion-android-chat" data-note-id="'+uniqid+'" href="javascript:;"></a>');
        
        var desc = new Notes.elements.Description({
            description: document.getElementById('description-editor-' + uniqid)
        });

        $('.ideas-editor').parent().removeClass('active');

        var ed = $('#ideas-editor-'+uniqid);
        ed.focus();
        // needs to to be refactored
        ed.parent().addClass('active');
        desc.addActive(uniqid);
        return ed;
	};

	Notes.Editors = Editors;

})(Notes, window.setTimeout);;(function(Notes) {

    var LoadFootnotes = function(options) {
        var footnotes = document.getElementsByClassName('footnote');
        for(var i = 0; i < footnotes.length; i++) {
            var comment = document.getElementById('footnote-' + footnotes[i].dataset.id);
            var footnote = new Footnote({
               marker: document.getElementById('footnote-marker-' + footnotes[i].dataset.id),
               footnote: comment,
               container: document.getElementById('comments-container'),
               id: footnotes[i].dataset.id,
               noteId: footnotes[i].dataset.noteId,
               description: document.getElementById('description-editor-' + footnotes[i].dataset.noteId),
               deleteBtn: comment.getElementsByTagName('button')[0]
           });          
        }
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.LoadComments = LoadFootnotes;

})(Notes);;(function(Notes) {

    var CommentButton = function(options) {
        this.button = options.button;
    };

    CommentButton.prototype.show = function() {
        this.button.style.display = 'block';
    };

    CommentButton.prototype.hide = function() {
        this.button.style.display = 'none';
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.CommentButton = CommentButton;

})(Notes);
;(function(Notes) {

    var _ = Notes.events;

    var Footnote = function(options, create) {
        this.marker = options.marker;
        this.container = options.container;
        this.id = options.id;
        this.noteId = options.noteId;
        this.description = options.description;
        this.isActive = false;

        if (!this.marker) {
            return false;
        }

        if(create) {
            this.create();   
        } else {
            this.load(options);
        }
        this.listen();
    }

    Footnote.prototype.create = function() {
        var node = document.createElement('div');
        node.className = "footnote";
        node.id = "footnote-" + this.id;
        node.dataset.id = this.id;
        node.dataset.noteId = this.noteId;
        var input = document.createElement('textarea');
        input.className = 'footnote-input';
        var deleteBtn = document.createElement('button');
        var deleteIcon = document.createElement('span');
        deleteIcon.className = "ion-android-cancel";
        deleteBtn.appendChild(deleteIcon);
        node.appendChild(input);
        node.appendChild(deleteBtn);
        this.container.appendChild(node);
        this.footnote = node;
        this.deleteBtn = deleteBtn;
        this.show();

    };

    Footnote.prototype.load = function(options) {
        this.footnote = options.footnote;
        this.deleteBtn = options.deleteBtn;
    };

    Footnote.prototype.listen = function() {
        
        this.marker.addEventListener('click', function() {
            this.show();
        }.bind(this));

        this.description.addEventListener('click', function(event) {
            if(event.target !== this.marker) {
              this.hide();
            }
        }.bind(this));

        _.addListener('description-focus', function(id, isActive) {
            if(!isActive && id === this.noteId) {
                // this.container.style.display = 'none';
            }
        }.bind(this));

        _.addListener('idea-focus', function(id, isActive) {
            this.hide();
        }.bind(this));

        _.addListener('new-idea', function() {
            this.hide();
        }.bind(this));

        this.deleteBtn.addEventListener('click', function() {
            this.footnote.remove();
            this.marker.remove();
        }.bind(this));
    };

    Footnote.prototype.toggle = function() {
        if (this.isActive) {
            this.hide();
        } else {
            this.show();
        }
    };

    Footnote.prototype.show = function() {
        if (!this.isActive) {
            $(this.marker).addClass('active');
            $('#ideas-editor-'+this.noteId).parent().addClass('active');
            $('.footnote').hide();
            $('#comments-container').show();
            this.footnote.style.display = 'block';
            $(this.footnote).addClass('active');
            this.isActive = true;
            $(this.footnote).find('textarea').focus();
            _.emit('comment-focus', null, this.noteId, true);
            $('#ideas-editor-' + this.noteId).parent().addClass('active');
        }
    };

    Footnote.prototype.hide = function() {
        if(this.isActive) {
            $('#comments-container').hide();
            $(this.marker).removeClass('active');
            this.footnote.style.display = 'none';
            this.footnote.className = 'footnote';
            this.isActive = false;
            _.emit('comment-focus', null, this.id, false);
        }
    }

    Footnote.prototype.save = function() {
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.Footnote = Footnote;


})(Notes);;(function(Notes) {

    var Wysiwyg = function(options) {
        this.editor = options.editor;
        this.wysiwyg = this.init(); 
    };

    Wysiwyg.prototype.init = function() {
		var index = 0;
		return $(this.editor).wysiwyg({
			'class': 'wysiwyg-description-editor',
			toolbar: "top",
			buttons: {
				 bold: {
                    title: 'Bold (Ctrl+B)',
                    image: '\uf032',
                    hotkey: 'b'
                },
                italic: {
                    title: 'Italic (Ctrl+I)',
                    image: '\uf033',
                    hotkey: 'i'
                },
                underline: {
                    title: 'Underline (Ctrl+U)',
                    image: '\uf0cd',
                    hotkey: 'u'
                },
                strikethrough: {
                    title: 'Strikethrough (Ctrl+S)',
                    image: '\uf0cc',
                    hotkey: 's'
                },
                forecolor: {
                    title: 'Text color',
                    image: '\uf1fc' // <img src="path/to/image.png" width="16" height="16" alt="" />
                },
                highlight: {
                    title: 'Background color',
                    image: '\uf043' // <img src="path/to/image.png" width="16" height="16" alt="" />
                },
                alignleft: index != 0 ? false : {
                    title: 'Left',
                    image: '\uf036',
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                aligncenter: index != 0 ? false : {
                    title: 'Center',
                    image: '\uf037',
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                alignright: index != 0 ? false : {
                    title: 'Right',
                    image: '\uf038',
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                alignjustify: index != 0 ? false : {
                    title: 'Justify',
                    image: '\uf039', // <img src="path/to/image.png" width="16" height="16" alt="" />
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                indent: index != 0 ? false : {
                    title: 'Indent',
                    image: '\uf03c',
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                outdent: index != 0 ? false : {
                    title: 'Outdent',
                    image: '\uf03b',
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                orderedList: index != 0 ? false : {
                    title: 'Ordered list',
                    image: '\uf0cb',
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                unorderedList: index != 0 ? false : {
                    title: 'Unordered list',
                    image: '\uf0ca',
                    //showstatic: true,    // wanted on the toolbar
                    showselection: false    // wanted on selection
                },
                removeformat: {
                    title: 'Remove format',
                    image: '\uf12d' // <img src="path/to/image.png" width="16" height="16" alt="" />
                }
	        }
		});

    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

	Notes.elements.Wysiwyg = Wysiwyg;

})(Notes);
;(function(Notes, getSelection) {

    var _ = Notes.events;

	var Description = function(options) {
		this.editor = options.description;
        this.id = this.editor.dataset.uniqid;
        this.active = false;
        this.commentHasFocus = false;
        this.init();
        this.listen();
	};

    Description.prototype.init = function init() {

        this.commentBtn = new Notes.elements.CommentButton({
            button: document.getElementById('create-footnote-btn-' + this.id)
        });

        this.footnoteBtn = document.getElementById('create-footnote-btn-' + this.id);

        var wysiwyg = new Notes.elements.Wysiwyg({ 
            editor: this.editor
        });

        this.wysiwyg = wysiwyg.wysiwyg;
    };

    Description.prototype.getFootnoteId = function() {
        return parseInt(new Date().getTime() * Math.random(100), 10);
    };

    Description.prototype.insertTextAtCursor = function insertTextAtCursor(text, id, noteId) {
        var sel, range, html;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                var div = document.createElement('input');
                div.className = "footnote-marker";
                div.readOnly = true;
                div.id = 'footnote-marker-' + id;
                div.dataset.id = id;
                div.dataset.noteId = noteId;
                range.insertNode( div );
                return div; 
            }
        } else if (document.selection && document.selection.createRange) {
            document.selection.createRange().text = text;
        }
    };

    Description.prototype.listen = function() {

        $(this.editor).focus(function() {
            _.emit('description-focus', null, this.editor.dataset.uniqid, true);
            this.hasFocus = true;
            this.addActive(this.id);
            $('.ideas-editor[data-uniqid="'+this.editor.dataset.uniqid+'"]').parent().addClass('active');
        }.bind(this));

        $(this.editor).focusout(function(event) {
            _.emit('description-focus', null, this.editor.dataset.uniqid, false, event.target);
            this.hasFocus = false;
            $('.ideas-editor[data-uniqid="'+this.editor.dataset.uniqid+'"]').parent().removeClass('active');
            if (!this.commentHasFocus) {
                this.removeActive(this.id, event.target);
            }
        }.bind(this));

        Notes.events.addListener('comment-focus', function(id, isFocused) {
            if (id === this.id && isFocused) {
                this.hasFocus = true;
                this.commentHasFocus = true;
                this.addActive(this.id);
            }
        }.bind(this));

        Notes.events.addListener('idea-focus', function(id, isActive) {
            isActive ? this.addActive.call(this, id) : this.removeActive.call(this, id);
        }.bind(this));

        $('#show-description-idea-' + this.id).on('click', function() {
            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

            $.Velocity($('.description__container'), {
                display: 'block',
                translateX: -w
            }, {
                duration: 200
            });

            $('.description__container').css({
                display: "none"
            });

            $.Velocity(document.getElementById('ideas-editor-container'), {
                translateX: 0
            }, {
                duration: 200,
                easing: "easeIn"
            });

            this.addActive.call(this, this.id);

        }.bind(this));

        this.footnoteBtn.addEventListener('click', function(event) {
            event.preventDefault();

            var id = this.getFootnoteId();
            var footnoteMarker = this.insertTextAtCursor('', id, this.id);
            if(!footnoteMarker) {
                return false;
            }

            Notes.events.emit('message', null, 'Added a Comment!');
            $('.footnote').hide();

            var footnote = new Notes.elements.Footnote({
               marker: footnoteMarker,
               container: document.getElementById('comments-container'),
               id: id,
               noteId: this.id,
               description: this.editor
            }, true);

        }.bind(this));
    };

    Description.prototype.addActive = function(id) {

        if (id.toString() === this.id) {
            $('.description-editor').parent().hide();
            $(this.editor).parent().show();
            $(this.editor).addClass('active');
            $('.create-footnote-btn').hide();
            this.commentBtn.show(); 
        }
        
        this.active = true;
    };

    Description.prototype.removeActive = function(id, eventTarget) {
        if (id.toString() === this.id) {
            $(this.editor).removeClass('active');
        }
        this.active = false;
    };

    Description.prototype.toggleFootnoteBtn = function() {

    };

    Description.prototype.hide = function() {
        $('.description-editor').parent().fadeOut(200);
        this.active = false;
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Description.prototype.insertHtml = function(html) {
    }

    Notes.elements.Description = Description;
    
})(Notes, window.getSelection);
;(function(Notes) {

	var _ = Notes.events;
	var api = Notes.api;

	var Title = function(options) {
		this.title = options.title || document.getElementById('title-input');
		this.listen.call(this);
	};

	Title.prototype.listen = function() {

		this.title.addEventListener('click', function() {
			_.emit('title-edit');
		});

		_.addListener('title-edit', function() {
		
		}.bind(this)); 

	};

	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.Title = Title;

})(Notes);
;(function(Notes) {
    var e = Notes.events;

    function DetailsPane(options) {
      this.$container = options.container;
      this._areDetailsShown = false;
      this.listen.call(this);
      this.count = 1;
    }

    DetailsPane.prototype.listen = function() {
        e.addListener('details-btn-clicked', this.toggleDetailsPane.bind(this));

        $('#add-details').on('click', function() {
            $('#details').prepend('<li id="details-item-'+this.count+'" class="details__item" style="opacity: 0;"><form class="details-form"><div class="details__input-container"><input class="details-name details__input input" name="values[]" placeholder="name" value=""></div><div class="details__input-container"><input class="details-value details__input input" name="values[]" placeholder="value" value=""></div></form></li>');        
            $("#details-item-" + this.count).velocity({
              "opacity": 1
            }, {
              "duration": 300,
              "easing": "easeIn"
            });
            this.count = this.count + 1;
            Notes.events.emit('message', null, 'Adding Detail');

        }.bind(this));
    };

    DetailsPane.prototype.toggleDetailsPane = function() {
        !this._areDetailsShown ? this.open.call(this) : this.close.call(this);
        this._areDetailsShown = !this._areDetailsShown;
        return this;
    };

    DetailsPane.prototype.open = function() {
        var editor = document.getElementById('editor-section');
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        if (!this._areDetailsShown) {

          $('#note-details').css({
              position: 'absolute',
              display: 'block'
          });

          $.Velocity(this.$container, {
              display: 'block',
              translateX: w
          }, {
              duration: 0
          });

          $.Velocity(editor, {
              translateX: -w
          }, {
              duration: 200,
              easing: "easeIn"
          });

          $.Velocity(this.$container, {
              translateX: 0
          }, {
              duration: 200,
              easing: "easeIn"
          });
        }

        return this;

    };

    DetailsPane.prototype.close = function() {
        var editor = document.getElementById('editor-section');
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        if (this._areDetailsShown) {

          $.Velocity(this.$container, {
              translateX: w
          }, {
              duration: 200,
              easing: "easeIn"
          });

          $.Velocity(editor, {
              translateX: 0
          }, {
              duration: 200,
              easing: "easeIn"
          });
          
          $('#note-details').css({
              display: 'none'
          });

        }
        return this;
    };

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.DetailsPane = DetailsPane;
    
  })(Notes);
;(function(Notes) {

	var SearchBar = function() {
		var search = document.getElementById('notes-search');
		search.addEventListener('focus', function(event) {
			$.Velocity(event.target, {
				width: "150px"
			}, {
				duration: 150,
				easing: [0.4, 0.2, 0.3, .1]
			});
		});
	}
	
	if (Notes.elements === undefined) {
		Notes.elements = {};
	}

	Notes.elements.SearchBar = SearchBar;

})(Notes);

;(function(Notes) {

	var _ = Notes.events;
	//var $ = Notes.elemental;

	var NotesList = function() {
		this.init();
		this.listen();
	};

	NotesList.prototype.init = function() {
		this.w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		this.h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	};

	NotesList.prototype.onNoteDeletedLoadListener = function() {
		
		window.addEventListener('resize', this.init.bind(this));
	    
	    /**
	     * [deleteButton description]
	     * @type {[type]}
	     */
	    this.deleteButton = document.getElementById('delete');
	   	
	   	/**
	   	 * [deleteNoteButtons description]
	   	 * @type {[type]}
	   	 */
		this.deleteNoteButtons = document.getElementsByClassName('delete-note')
		
		/**
		 * [modalBackground description]
		 * @type {[type]}
		 */
		this.modalBackground = document.getElementById('modal-background');
		
		/**
		 * [cancelDeleteButton description]
		 * @type {[type]}
		 */
		this.cancelDeleteButton = document.getElementById('cancel-delete');

	    /**
	     * [formButton description]
	     * @type {Notes}
	     */
	    this.formButton = new Notes.elements.LoginFormButton({
            button: this.deleteButton
        });
		
		/**
		 * [description]
		 * @param  {[type]} button) 
		 * @return {[type]}
		 */
		Array.prototype.forEach.call(this.deleteNoteButtons, function(button) {
			button.addEventListener('click', this.openDeleteModal.bind(this));
		}.bind(this));

		// when the modal background is clicked
		this.modalBackground.addEventListener('click', this.closeModal.bind(this));
		
		// cancel note deletion
		this.cancelDeleteButton.addEventListener('click', this.closeModal.bind(this));
        
        // delete note
        this.deleteButton.addEventListener('click', this.onDeleteNote.bind(this));
	};

	NotesList.prototype.listen = function() {
    	_.addListener('get-notes', this.onNoteDeletedLoadListener.bind(this));
		_.addListener('note-delete', this.onNoteDeletedLoadListener.bind(this));
        _.addListener('note-delete', this.onNoteDeleted.bind(this));
	};

	NotesList.prototype.onDeleteNote = function() {

        this.formButton.changeButtonState('waiting');

        Notes.api.write({
        	name: "note-delete",
        	method: "POST",
        	endpoint: "note/delete",
        	context: this,
        	data: {
                token: localStorage.getItem('notes-token'),
                noteId: this.deleteButton.dataset.noteId,
            }
        });
	};

	NotesList.prototype.onNoteDeleted = function(data, context) {
		 if (data.error !== undefined) {
            Notes.events.emit("message", null, data.error);
            this.formButton.changeButtonState('ready');
            return false;
        }
        this.formButton.changeButtonState('success');
        Notes.events.emit('message', null, 'Note Deleted');
        $('.modal-background').fadeOut(100);
        $('.delete-modal').fadeOut(50);
        var deleteBtn = document.getElementById('delete');
        var id = this.deleteButton.dataset.noteId;
        $('.notes-list__item[data-note-id="'+id+'"]').fadeOut(100);
	};

	NotesList.prototype.openDeleteModal = function(event, value) {
		this.loadNoteDataIntoModal(event, value).openModal();
	};

	NotesList.prototype.loadNoteDataIntoModal = function(event, value) {
        var note = $(event.target);
        var title = note.data('noteTitle');
        var id = note.data('noteId');
        $('#modal-note-name').html(title);
        var deleteBtn = document.getElementById('delete');
        deleteBtn.dataset.noteId = id;
        return this;
	};

	NotesList.prototype.openModal = function() {
        $('.modal-background').fadeIn(200);
        $('.delete-modal').velocity({
            translateY: this.h
        }, {
            duration: 0
        }).show().velocity({
            translateY: 0
        }, {
            duration: 200,
            easing: "easeIn"
        });
		return this;
	};

	NotesList.prototype.closeModal = function() {
        $('.modal-background').fadeOut(100);
        $('.delete-modal').fadeOut(50);
	};

    if (Notes.elements === undefined) {
        Notes.elements = {};
    }

    Notes.elements.Notes = NotesList;
})(Notes);
;(function(Notes, localStorage, setTimeout) {

    // var $ = Notes.elemental;
    var api = Notes.api;
	  var _ = Notes.events;

    var LoadNotes = function(options) {
      	this.listen();
      	this.init();
    };

    LoadNotes.prototype.init = function() {
    	api.write({
	      	name: 'get-notes',
	      	method: 'POST',
	      	endpoint: 'notes',
            context: this,
	      	data: {
	      		token: localStorage.getItem('notes-token')
      		}
	    });
    };

    LoadNotes.prototype.listen = function() {
    	_.addListener('get-notes', this.callback.bind(this));
    }

    /**
     * @todo refactor
     * @param  {[type]}   data [description]
     * @return {Function}      [description]
     */
    LoadNotes.prototype.callback = function(data) {
        if (!Array.isArray(data)) {
            _.emit('message', null, 'Error');
            return false;
        }

    	data.forEach(function(value, index) {
            if(value.note && value.note.fields) {
                $('#notes-list').append('<li class="notes-list__item" data-note-id="'+value.noteId+'" data-note-title="'+value.note.fields.title.value+'"><div class="notes-list__title"><a class="notes-list__link" href="/note/'+value.noteId+'">'+value.note.fields.title.value+'</a><ul><li><span>'+value.note.ideas[0].value+' &hellip;</span></li></ul></div><div class="notes-list__menu"><a id="delete-note-'+value.noteId+'" class="delete-note" data-note-id="'+value.noteId+'" data-note-title="'+value.note.fields.title.value+'" href="javascript:;"><span class="ion-android-cancel"></span>Delete</a></div></li>');
            }
        });
        
    }


    if (Notes.init === undefined) {
        Notes.init = {};
    }

    Notes.elements.LoadNotes = LoadNotes;

})(Notes, localStorage, window.setTimeout);
;(function(Notes, localStorage) {

    var LoadNote = function(options) {

      var e = Notes.events;
      var api = Notes.api;

      api.get({
          name: 'on-get-note',
          method: 'POST',
          endpoint: 'note/' + $('#note').data('noteId'),
          data: {
            token: localStorage.getItem('notes-token')
          }
      });

      e.addListener('on-get-note', function(data) {
        $('#note').append(data.html);
        $('.ideas-editor').each(function(editor) {
          new Notes.Editor({
              editor: document.getElementById(this.id)
          });
        });

        $('.description-editor').each(function(editor) {
          var ed = document.getElementById(this.id);
            var desc = new Notes.elements.Description({
                description: ed
            });
            desc.insertHtml(ed.innerHTML);
        });

        Notes.events.emit('note-loaded');
        Notes.events.emit('message', Notes, 'Note Loaded', 'info');
      });

    };

    if (Notes.init === undefined) {
        Notes.init = {};
    }

    Notes.init.LoadNote = LoadNote;

})(Notes, localStorage);
;(function(Notes) {

    var LoadMobile = function(options) {

    
	var mobileInit = function(off) {

		if (window.matchMedia("(max-width: 1024px)").matches) {
			var ideas = document.getElementsByClassName('active');
			if (ideas.length === 0) {
				return false;
			}
			var desc = document.getElementById('description-editor-container');
			if (ideas.length === 0) {
				var descIdeas = document.getElementsByClassName('wysiwyg-active');
				if (descIdeas.length === 0) {
					return false;
				}
			}
			var stage = document.getElementById('ideas-editor-container');
			var ideaEditor = stage.getElementsByTagName('textarea')[0];				
			
			// create a manager for that element
			var mc = new Hammer(stage, {domEvents: true});
			var d = new Hammer(desc, {domEvents: true});
			var turn = false;
			mc.on('swipeleft', function() {
				if(turn) {
					//stage.style.transform = 'rotate(0)';
					turn = false;
				} else {
					//stage.style.transform = 'rotate(-1024deg)';
					stage.style.transform = 'translateX(-100vw)';
					desc.style.transform = 'translateX(0vw)';
					turn = true;
				}
			});

			d.on('swiperight', function() {
				if(turn) {
					//stage.style.transform = 'rotate(0)';
					stage.style.transform = 'translateX(0vw)';
					desc.style.transform = 'translateX(100vw)';
					turn = false;
				} else {
					//stage.style.transform = 'rotate(-1024deg)';
					turn = true;
				}
			});
			return {
				mc: mc,
				d: d
			}
		}
	};
	if (window.matchMedia("(max-width: 1024px)").matches) {
		var desc = document.getElementById('description-editor-container');
		if (desc) {
			desc.style.transform = 'translateX(100vw)';
		} 
		mobileInit();
		Notes.events.addListener('idea-focus', function(id, isActive) {
			this.mobile;
				if (isActive) {
				this.mobile = mobileInit();
			} else {
			}
		}.bind(this));
	}

	Notes.events.addListener('note-loaded', function(isNew) {

		if (window.matchMedia("(max-width: 1024px)").matches) {

			$('#editor-section').on('click', function() {
				$('.description-editor').parent().hide();
				$('.comments__container').hide();
				$('.footnote').hide();
				$('.create-footnote-btn').hide();
				$('.description-empty').hide();
			}).children().click(function(e) {
				return false;
			});

			$('.note-menu').prependTo($('#note'));
		}
	});

    };

    if (Notes.init === undefined) {
        Notes.init = {};
    }

    Notes.init.LoadMobile = LoadMobile;

})(Notes);;(function(Notes) {

	document.addEventListener("DOMContentLoaded", function(event) {

		var mobile = new Notes.init.LoadMobile();

		/**
		 * [loginBtn description]
		 * @type {[type]}
		 */
		var loginBtn = document.getElementById('login-btn');

		if (loginBtn) {

			var user = Notes.user = new Notes.User();

			/**
			 * [siteNav description]
			 * @type {Notes}
			 */
			var userMenu = new Notes.elements.UserMenu({
				loginBtn: document.getElementById('login-btn'),
				logoutBtn: document.getElementById('logout-btn'),
				userBtn: document.getElementById('user-btn'),
				user: user
			});

			/**
			 * [userMenu description]
			 * @type {Notes}
			 */
			var userNav = new Notes.elements.UserNav({
				button: document.getElementById('user-btn'),
				menu: document.getElementById('user-menu')
			});

			/**
			 * [form description]
			 * @type {Notes}
			 */
			var form = new Notes.elements.LoginForm({
				form: document.getElementById('login-form')
			});

			/**
			 * [signupform description]
			 * @type {Notes}
			 */
			var signupform = new Notes.elements.LoginForm({
				form: document.getElementById('signup-form')
			});

			/**
			 * [loginModal description]
			 * @type {Notes}
			 */
			var loginModal = new Notes.elements.LoginModal({
				modal: document.getElementById('login-modal')
			});

			Notes.loginModal = loginModal;
	
		}

		/**
		 * [loginPanel description]
		 * @type {[type]}
		 */
		var loginPanel = document.getElementById('login-panel');

		/**
		 * [if description]
		 * @param  {[type]} loginPanel [description]
		 * @return {[type]}            [description]
		 */
		if(loginPanel) {
			var user = Notes.user = new Notes.User();
			/**
			 * [form description]
			 * @type {Notes}
			 */
			var form = new Notes.elements.LoginForm({
				form: document.getElementById('login-form')
			});

			/**
			 * [signupform description]
			 * @type {Notes}
			 */
			var signupform = new Notes.elements.LoginForm({
				form: document.getElementById('signup-form')
			});

			/**
			 * [loginModal description]
			 * @type {Notes}
			 */
			var loginModal = new Notes.elements.LoginModal({
				modal: document.getElementById('login-panel')
			});
		}
		
		/**
		 * [description]
		 * @param  {Notes}  isNew)       {			var                                         note          [description]
		 * @param  {[type]} reminderBtn: document.getElementById('set-reminder-btn')      [description]
		 * @param  {[type]} detailsBtn:  document.getElementById('edit-details-btn')      [description]
		 * @param  {Notes}  saveBtn:     document.getElementById('save-btn')			});			var detailsPane   [description]
		 * @return {[type]}              [description]
		 */
		Notes.events.addListener('note-loaded', function(isNew) {

			/**
			 * [note description]
			 * @type {Notes}
			 */
			var note = new Notes.elements.Note({
				note: document.getElementById('note')
			});

			/**
			 * [menu description]
			 * @type {Notes}
			 */
			var menu = new Notes.elements.NoteMenu({
				newBtn: document.getElementById('new-note-btn'),
				reminderBtn: document.getElementById('set-reminder-btn'),
				detailsBtn: document.getElementById('edit-details-btn'),
				saveBtn: document.getElementById('save-btn')
			});

			/**
			 * [detailsPane description]
			 * @type {Notes}
			 */
			
			var detailsPane = new Notes.elements.DetailsPane({
				container: document.getElementById('note-details')
			});

			if (isNew) {
				 /**
				 * [description description]
				 * @type {Notes}
				 */
				var description = new Notes.elements.Description({
					description: document.getElementById('description-editor-1')
				});		
			}

			/**
			 * [editors description]
			 * @type {Notes}
			 */
			
			var editor = new Notes.Editor({
				editor: document.getElementById('ideas-editor-1')
			});

			var editors = new Notes.Editors({
				container: document.getElementById('ideas')
			});

			/**
			 * [loadComments description]
			 * @type {Notes}
			 */
			var loadComments = new Notes.elements.LoadComments();

		});

		/**
		 * Note List View
		 * @type {[type]}
		 */
		var noteListEl = document.getElementById('notes-list');

		/**
		 * [if description]
		 * @param  {[type]} noteListEl [description]
		 * @return {[type]}            [description]
		 */
		if (noteListEl) {


			var loadNotes = new Notes.elements.LoadNotes({
				notesList: noteListEl
			});

			/**
			 * [notes description]
			 * @type {Notes}
			 */
			var notes = new Notes.elements.Notes({
				notesList: noteListEl
			});



			/**
			 * [search description]
			 * @type {Notes}
			 */
			var search = new Notes.elements.SearchBar();

		}


		/**
		 * Note View
		 * @type {[type]}
		 */
		var noteEl = document.getElementById('note');

		if (noteEl) {

			if (noteEl.dataset.noteStatus === 'retreived') {
				/**
				 * [note description]
				 * @type {Notes}
				 */
				var note = new Notes.init.LoadNote({
					note: noteEl
				});
			}

			/**
			 * [if description]
			 * @param  {[type]} noteEl.dataset.noteStatus [description]
			 * @return {[type]}                           [description]
			 */
			if (noteEl.dataset.noteStatus === 'new') {
				Notes.events.emit('note-loaded', null, true);	
			}

		}

	});


})(Notes);