module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            // @todo remove drupal
            dist: {
                src: [
                    'src/scripts/bower_components/wysiwyg.js/dist/wysiwyg.min.js',
                    'src/scripts/bower_components/wysiwyg.js/dist/wysiwyg-editor.min.js',
                    'src/scripts/bower_components/hammerjs/hammer.js',
                    'src/scripts/init/_init.js',
                    'src/scripts/utilities/_elemental.js',
                    'src/scripts/utilities/_events.js',
                    'src/scripts/utitilies/_message.js',
                    'src/scripts/utilities/_uniqueId.js',
                    'src/scripts/utilities/_fetch.js',
                    'src/scripts/utilities/_user.js',
                    'src/scripts/utilities/_api.js',
                    'src/scripts/elements/login/_loginFormButton.js',
                    'src/scripts/elements/login/_loginFormInput.js',
                    'src/scripts/elements/login/_loginForm.js',
                    'src/scripts/elements/login/_loginModal.js',
                    'src/scripts/elements/header/_userNav.js',
                    'src/scripts/elements/header/_userMenu.js',
                    'src/scripts/elements/note/_noteMenu.js',
                    'src/scripts/elements/note/_note.js',
                    'src/scripts/elements/editor/idea/_editorShortcuts.js',
                    'src/scripts/elements/editor/idea/_editIdeas.js',
                    'src/scripts/elements/editor/idea/_editor.js',
                    'src/scripts/elements/editor/idea/_editors.js',
                    'src/scripts/elements/editor/description/_loadFootnotes.js',
                    'src/scripts/elements/editor/description/_commentButton.js',
                    'src/scripts/elements/editor/description/_footnote.js',
                    'src/scripts/elements/editor/description/_wysiwyg.js',
                    'src/scripts/elements/editor/description/_description.js',
                    'src/scripts/elements/editor/_title.js',
                    'src/scripts/elements/details/_details.js',
                    'src/scripts/elements/archive/_searchBar.js',
                    'src/scripts/elements/archive/_notes.js',
                    'src/scripts/elements/archive/_loadNotes.js',
                    'src/scripts/elements/note/_loadNote.js',
                    'src/scripts/init/_mobile.js',
                    'src/scripts/app/_note.js'
                ],
                dest: 'dist/js/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jshint: {
            options: {
              curly: true,
              eqeqeq: true,
              eqnull: true,
              browser: true,
              reporter: 'jslint',
              reporterOutput: 'reports/jshint.xml',
              globals: {
                  jQuery: true
              },
            },
            all: ['src/scripts/**/*.js']
        },
        sass: {
            dist: {
               options: {
                   style: 'expanded'
               },
               files: {
                   'src/styles/css/main.css': 'src/styles/scss/main.scss'
               }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'dist/styles/<%= pkg.name %>.min.css': ['src/styles/css/main.css']
                }
            }
        },
        csslint: {
            options: {
                formatters: [
                    {id: 'junit-xml', dest: 'reports/csslint_junit.xml'},
                    {id: 'csslint-xml', dest: 'reports/csslint.xml'}
                ]
            },
            strict: {
                options: {
                  import: 2
                },
                src: ['src/styles/css/main.css'],
            }
        },
        watch: {
            scripts: {
                files: ['src/scripts/**/*.js'],
                tasks: ['build-js'],
                options: {
                    spawn: false,
                },
            },
            styles: {
                files: ['src/styles/**/*.scss'],
                tasks: ['build-css'],
                options: {
                    spawn: false,
                },
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-csslint');

    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build-js', ['concat', 'uglify']);
    grunt.registerTask('build-css', ['sass', 'cssmin']);
    grunt.registerTask('build', ['build-css', 'build-js']);

};
