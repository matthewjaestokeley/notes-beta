var router = require("express").Router();
var login = require('./controller');
var controller = require('../../configuration/middleware/controller').controller;

// Set routes
router.get("/", controller(login.get));

module.exports = router;
