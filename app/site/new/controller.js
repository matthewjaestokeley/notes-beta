/**
 * the controller-esque object
 * of functions to handle requests
 * on the annotations endpoint
 *
 * @type {Object}
 */
var controller = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
        // generate node id
        var uniqid = function() {
            return parseInt(new Date().getTime() * Math.random(100), 10);
        };
        var id = uniqid();
        res.render('page', {
            title: 'Page',
            id: id,
            fields: {
                title: {
                    value: ""
                },
                "summary-editor": {
                    value: ""
                }
            },
            descriptions: [
                {
                    name: "description__item--1",
                    id: 1,
                    value: ""       
                }
            ],
            ideas: [
                {
                    name: "idea__item--1",
                    id: 1,
                    value: ""
                }
            ],
            details: [
                {
                    name: "",
                    value: ""
                }
            ],
            footnotes: [
            ]
        });
    }
};

module.exports = controller;
