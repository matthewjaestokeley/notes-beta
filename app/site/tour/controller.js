/**
 * the controller-esque object
 * of functions to handle requests
 * on the annotations endpoint
 *
 * @type {Object}
 */
var controller = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
        // generate node id
        var uniqid = function() {
            return parseInt(new Date().getTime() * Math.random(100), 10);
        };
        var id = uniqid();
        res.render('tour', {
            title: 'Page',
            id: id,
            fields: {
                title: {
                    value: "The Note"
                }
            },
            descriptions: [
                {
                    name: "description__item--1",
                    id: 1,
                    value: "Pitch market success buzz equity rockstar sales conversion customer. User experience validation business-to-business product management accelerator rockstar creative beta. Technology niche market android return on investment business-to-business assets ecosystem twitter freemium creative analytics conversion. "    
                }, {
                    name: "description__item--2",
                    id: 2,
                    value: "Growth hacking user experience partnership low hanging fruit release stealth backing iPhone first mover advantage product management termsheet vesting period advisor. Scrum project series A financing responsive web design iPad launch party startup facebook"
                }, {
                    name: "description__item--3",
                    id: 3,
                    value: "Equity focus strategy funding crowdsource network effects. Research & development supply chain ownership user experience hypotheses sales mass market iPad equity scrum project assets business-to-consumer. Founders customer partner network burn rate freemium gamification business-to-business success. Buzz analytics prototype incubator traction. Partner network bootstrapping stealth release crowdfunding disruptive creative business-to-consumer iPad ecosystem."
                },
            ],
            ideas: [
                {
                    name: "idea__item--1",
                    id: 1,
                    value: "The first main idea"
                },
                {
                    name: "idea__item--2",
                    id: 2,
                    value: "A second main idea"
                },
                {
                    name: "idea__item--3",
                    id: 3,
                    value: "A third main idea"
                }
            ],
            details: [
                {
                    name: "course",
                    value: "Learning How to Learn"
                },
                {
                    name: "instructor",
                    value: "Dr. Barb Oakley"
                },
                {
                    name: "instructor",
                    value: "Dr. Terry Sejnowski"
                }
            ]
        });
    }
};

module.exports = controller;
