var router = require("express").Router();
var login = require('./controller');
var controller = require('../../configuration/middleware/controller').controller;
var authenticate = require('../../configuration/authentication/authenticate').token;
var parse = require("body-parser").json();
// Set routes
router.get("/note/:noteId", controller(login.get));

module.exports = router;
