/**
 * the controller-esque object
 * of functions to handle requests
 * on the annotations endpoint
 *
 * @type {Object}
 */
var controller = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
        var id = req.params.noteId
        res.render('note', {
            title: 'Page',
            id: id
        });
    }
};

module.exports = controller;
