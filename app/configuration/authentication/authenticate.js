var jwt = require('jwt-simple');
var config = require('./config');
/*
 *    Generic require login routing middleware for passport
 */

exports.requiresLogin = function (req, res, next) {
    if (!req.isAuthenticated()) {
        return res.redirect('/login');
    }
    next();
};


/*
 *    User authorizations routing middleware for passport
 */
exports.user = {
        hasAuthorization : function (req, res, next) {
            if (req.profile.id != req.user.id) {
                return res.redirect('/users/'+req.profile.id);
            }
            next();
        }
};


/**
 * jwt authentication
 */

exports.token = function(req, res, next) {
    var token = (req.body && req.body.token) || (req.query && req.query.token) || req.headers['x-access-token'];

    if (!token) {
        res.status(401).json({"error": "No Token Provided"});
    }

    try {
        var decoded = jwt.decode(token, config.jwt.key);
        if (decoded.exp <= Date.now()) {
            res.status(401).json({"error": "Token Expired"});
        }
        // @todo attach user to the request body
        next();

    } catch (err) {
        res.status(401).json({"error": "Token Not Validated"});
    }
};