var logger = require("./logger");
var express = require('express');
var exphbs = require('express-handlebars');
var helpers = require('./templates/handlebars/helpers');
var path = require('path');
var join = path.join;
//var logger = require("./../utils/logger");

var configuration = {};
configuration.init = function(app) {

    app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });

    app.set('view engine', 'ejs');

    logger.info('init');
    // // //setup view engine
    // logger.debug("Setting 'Vash' as view engine");

    // // Create `ExpressHandlebars` instance with a default layout.
    // var hbs = exphbs.create({
    //     defaultLayout: 'main',
    //     helpers: helpers,

    //     // Uses multiple partials dirs, templates in "shared/templates/" are shared
    //     // with the client-side of the app (see below).
    //     partialsDir: [
    //         'shared/templates/',
    //         'views/partials/'
    //     ]
    // });

    // // Register `hbs` as our view engine using its bound `engine()` function.
    // app.engine('handlebars', hbs.engine);
    // app.set('view engine', 'handlebars');

    // // Middleware to expose the app's shared templates to the cliet-side of the app
    // // for pages which need them.
    // function exposeTemplates(req, res, next) {
    //     // Uses the `ExpressHandlebars` instance to get the get the **precompiled**
    //     // templates which will be shared with the client-side of the app.
    //     hbs.getTemplates('shared/templates/', {
    //             cache: app.enabled('view cache'),
    //             precompiled: true

    //         }).then(function(templates) {
    //             // RegExp to remove the ".handlebars" extension from the template names.
    //             var extRegex = new RegExp(hbs.extname + '$');

    //             // Creates an array of templates which are exposed via
    //             // `res.locals.templates`.
    //             templates = Object.keys(templates).map(function(name) {
    //                 return {
    //                     name: name.replace(extRegex, ''),
    //                     template: templates[name]
    //                 };
    //             });

    //             // Exposes the templates during view rendering.
    //             if (templates.length) {
    //                 res.locals.templates = templates;
    //             }
    //             setImmediate(next);
    //         })
    //         .catch(next);
    // }
    logger.debug("Enabling GZip compression.");
    var compression = require('compression');
    app.use(compression({
        threshold: 512
    }));

    //logger.debug("Setting 'Public' folder with maxAge: 1 Day.");
    // var publicFolder = path.dirname(module.parent.filename) + "/public";
    // var oneYear = 31557600000;
    // app.use(express.static(publicFolder, {
    //     maxAge: oneYear
    // }));

    app.use(express.static(join(__dirname, "./../../public/")));

    logger.debug("Overriding 'Express' logger");
    app.use(require('morgan')({
        "stream": logger.stream
    }));
};

configuration.handleErrors = function(app) {
    process.on('uncaughtException', function(err) {
        logger.info('Caught exception: ');
        logger.info(err);
    });

    app.use(function(err, req, res, next) {
        if (err.isBoom) {
            logger.info('Joi Schema Validation Error:');
            logger.info(err.output.payload);
            return res.status(err.output.statusCode).json(err.output.payload);
        }
        next(err);
    });

    app.use(function(err, req, res, next) {
        logger.info(err.stack);
        if (req.xhr) {
            res.send(500, {
                error: 'Something blew up!'
            });
        } else {
            next(err);
        }
    });

    // // production error handler
    // // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        if (err) {
            logger.info('Error:');
            logger.info(err.message);
            res.status(500).send(JSON.stringify({
                message: err.message,
                error: err
            }));
        }

    });
};

module.exports = configuration;
