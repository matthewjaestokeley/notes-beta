var router = require("express").Router();
var validate = require('express-joi-validator');
var login = require('./controller');
var model = require('./model');
var controller = require('../../configuration/middleware/controller').controller;
var parse = require("body-parser").json();

// Set routes
router.post("/login", parse, validate(model.post), controller(login.post));

module.exports = router;
