var router = require("express").Router();
var validate = require('express-joi-validator');
var controller = require('./controller');
var model = require('./model');
var control = require('../../configuration/middleware/controller').controller;
var bodyParser = require("body-parser")
router.use( bodyParser.json() ); 
/**
 * 
 */
router.post("/signup", validate(model.post), control(controller.get));

module.exports = router;
