/**
 * @type {Object}
 */
var token = require('../../configuration/authentication/token');
var config = require('../../configuration/authentication/config');
var hash = require('../../configuration/authentication/hash');

var routeController = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
                      
            var mongo = require('mongodb').MongoClient;

            var doc = {
                "username": req.body.username,
                "password": hash(req.body.password, config.hmac.key)
            };

            // Connection URL
            var url = 'mongodb://localhost:27017/scribble';
            // Use connect method to connect to the Server
            mongo.connect(url, function(err, db) {
				//hash(password, config.hmac.key)
                var collection = db.collection('users');
                collection.findOne({
                    "username": req.body.username
                }, function(err, response) {
                    if (response) {
                        res.json({
                            "error": "User Exists"
                        });
                    } else {
                        collection.insert(doc, function(err, result) {
                        if (err) {
                            res.json({
                                "error": "no result"
                            });
                            return next(err);
                        }

                        if (!result) {
                            res.json({
                                "error": "no result"
                            });
                            return next();
                        }

                        // @todo return user web token
                        res.json({
                            "username": req.body.username,
                            "token": token(doc._id.toString())
                        });

                    });
                    }
                });
                


               // db.close();

            });

      

        }
};

module.exports = routeController;
