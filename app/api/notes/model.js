// the validation schema for the request object
var Joi = require('joi');
/**
 *
 * @type {Object}
 */
var keyword = {
    /**
     * @type {Object}
     */
    get: {
        /**
         * @type {Object}
         */
        query: {
            /**
             * @type {Int}
             */
            pageId: Joi.number().min(1),
            token: Joi.string()
        }
    }
};

// Use the Joi object to create a few schemas for your routes.
module.exports = keyword;
