var decode = require('../../configuration/authentication/user');

/**
 * @type {Object}
 */
var routeController = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
             var response = decode(req.body.token);
        var user = response.iss;

        var mongo = require('mongodb').MongoClient;
        var ObjectID = require('mongodb').ObjectID;
        // Connection URL
        var url = 'mongodb://localhost:27017/scribble';
        // Use connect method to connect to the Server
        mongo.connect(url, function(err, db) {

            var doc = {
                "userId": user,
                $text: {
                    $search: req.body.search,
                }
            }
            var collection = db.collection('notes');
            collection.find(doc).toArray(function(err, result) {
                if (err) {
                    res.json(JSON.stringify({
                        "error": "No Result"
                    }));
                    return next(err);
                }

                if (!result) {
                    res.json(JSON.stringify({
                        "error": "Unknown User"
                    }));
                    return next(err);
                }

                if (Object.keys(result).length === 0 ) {
                    res.json(JSON.stringify({
                        "error": "No User Data"
                    }));
                    return next(err);
                }
                // @todo return user web token
                res.json(result);

            });

        });

        }
};

module.exports = routeController;
