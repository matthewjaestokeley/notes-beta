// the validation schema for the request object
var Joi = require('joi');
/**
 *
 * @type {Object}
 */
var keyword = {
    /**
     * @type {Object}
     */
    get: {
        /**
         * @type {Object}
         */
        body: {
            /**
             * @type {Int}
             */
            noteId: Joi.number().min(1).required(),
            token: Joi.string().required()
        }
    }
};

// Use the Joi object to create a few schemas for your routes.
module.exports = keyword;
