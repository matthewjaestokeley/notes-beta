var decode = require('../../configuration/authentication/user');


/**
 * @type {Object}
 */
var routeController = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
         var response = decode(req.body.token);
        var user = response.iss;

        var mongo = require('mongodb').MongoClient;
        var ObjectID = require('mongodb').ObjectID;
        // Connection URL
        var url = 'mongodb://localhost:27017/scribble';
        // Use connect method to connect to the Server
        mongo.connect(url, function(err, db) {

            var doc = {
                "userId": user,
                "noteId": req.body.noteId
            }
            var collection = db.collection('notes');
            collection.deleteOne(doc, function(err, result) {
                if (err) {
                    res.json({
                        "error": "No Result"
                    });
                    return next(err);
                }

                if (!result) {
                    res.json({
                        "error": "Note not found"
                    });
                    return next(err);
                }

                console.log(result.result);

                res.status(200).send();

            });

        });

        }
};

module.exports = routeController;
