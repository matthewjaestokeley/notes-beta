var router = require("express").Router();
var validate = require('express-joi-validator');
var controller = require('./controller');
var model = require('./model');
var control = require('../../configuration/middleware/controller').controller;
var authenticate = require('../../configuration/authentication/authenticate').token;
var parse = require("body-parser").json();

/**
 * 
 */
router.post("/note/save", parse, authenticate, validate(model.post), control(controller.get));

module.exports = router;
