var decode = require('../../configuration/authentication/user');

var routeController = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
           
           var response = decode(req.body.token);
           var user = response.iss;

            var mongo = require('mongodb').MongoClient;

            // Connection URL
            var url = 'mongodb://localhost:27017/scribble';
            // Use connect method to connect to the Server
            mongo.connect(url, function(err, db) {
				//hash(password, config.hmac.key)
                var collection = db.collection('notes');
                
                collection.createIndex({
                    note: "text"
                });

                collection.update(
                {
                    "userId": user,
                    "noteId": req.body.noteId

                }, {
                    $set: {
                        "note": req.body.note
                    }
                }, {
                    "upsert": true
                }, function(err, result) {
                    if (err) {
                        res.json(JSON.stringify({
                            "error": "no result"
                        }));
                        return next(err);
                    }

                    if (!result) {
                        res.json(JSON.stringify({
                            "error": "no result"
                        }));
                        return next();
                    }
                    // @todo return user web token
                    res.json({
                        "test": "response"
                    });

                });


               // db.close();

            });

      

        }
};

module.exports = routeController;
