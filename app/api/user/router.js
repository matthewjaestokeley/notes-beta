var router = require("express").Router();
var validate = require('express-joi-validator');
var routerController = require('./controller');
var page = require('./model');
var control = require('../../configuration/middleware/controller').controller;
var authenticate = require('../../configuration/authentication/authenticate').token;
var parse = require("body-parser").json();


// Set routes
router.post("/user", parse, authenticate, control(routerController.get));

module.exports = router;
