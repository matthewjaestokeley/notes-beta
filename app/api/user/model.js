// the validation schema for the request object
var Joi = require('joi');
/**
 *
 * @type {Object}
 */
var signup = {
    /**
     * @type {Object}
     */
    get: {
        /**
         * @type {Object}
         */
        query: {
            /**
             * @type {Int}
             */
            pageId: Joi.number().min(1).required()
        }
    }
};

// Use the Joi object to create a few schemas for your routes.
module.exports = signup;
