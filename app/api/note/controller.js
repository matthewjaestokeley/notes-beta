var decode = require('../../configuration/authentication/user');
var ejs = require('ejs');
var fs = require('fs')
var title = fs.readFileSync('views/partials/title.ejs');
var details = fs.readFileSync('views/partials/details.ejs');
var editor = fs.readFileSync('views/partials/editor.ejs');

/**
 * @type {Object}
 */
var routeController = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    get: function(req, res, next) {
        var response = decode(req.body.token);
        var user = response.iss;

        var mongo = require('mongodb').MongoClient;
        var ObjectID = require('mongodb').ObjectID;
        // Connection URL
        var url = 'mongodb://localhost:27017/scribble';
        // Use connect method to connect to the Server
        mongo.connect(url, function(err, db) {

            var doc = {
                "userId": user,
                "noteId": req.params.noteId
            }
            var collection = db.collection('notes');
            collection.findOne(doc, function(err, result) {
                if (err) {
                    res.json(JSON.stringify({
                        "error": "No Result"
                    }));
                    return next(err);
                }

                if (!result) {
                    res.json(JSON.stringify({
                        "error": "Unknown User"
                    }));
                    return next(err);
                }

                if (Object.keys(result).length === 0 ) {
                    res.json(JSON.stringify({
                        "error": "No User Data"
                    }));
                    return next(err);
                }

                // render ejs 
                var html = [];
                html.push(ejs.render(title.toString(), result.note));
                html.push(ejs.render(details.toString(), result.note));
                html.push(ejs.render(editor.toString(), result.note));
                res.json({
                    html: html.join('')
                });

            });

        });

        }
};

module.exports = routeController;
